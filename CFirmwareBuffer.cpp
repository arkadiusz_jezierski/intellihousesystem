#include "CFirmwareBuffer.h"

CFirmwareBuffer::CFirmwareBuffer()
{

}

CFirmwareBuffer::CFirmwareBuffer(const CBuffer& orig) : CBuffer(orig) {

}

CFirmwareBuffer::~CFirmwareBuffer()
{

}


void CFirmwareBuffer::addData(unsigned int address, CBuffer& data) {

    unsigned int currentBufSize = this->size();
    if (not startAddress) {
        startAddress = address;
        this->clear();
    } else {
        for (size_t i = 0; i < (address - startAddress - currentBufSize); i++) {
            this->operator <<((unsigned char) NULL_DATA);
        }
    }
    this->operator <<(data);
}

void CFirmwareBuffer::addExData(unsigned int address, CBuffer& data) {

    unsigned int currentBufSize = this->size();
    if (not startAddress) {
        startAddress = address;
        this->clear();
    } else {
        for (size_t i = 0; i < (address - startAddress - currentBufSize); i++) {
            this->operator <<((unsigned char) NULL_EX_DATA);
        }
    }
    this->operator <<(data);
}

void CFirmwareBuffer::addExBufferData(unsigned int address, CBuffer &buffer) {


    for (int index : extendedBuffers.keys()){
        if ((index & 0xff0000) == (address & 0xff0000)) {
            extendedBuffers[index].addExData(address, buffer);
            return;
        }
    }

//    cout << "create new EX BUF [" << address << "]" << endl;
    extendedBuffers[address] = buffer;
    extendedBuffers[address].startAddress = address;

}

unsigned char CFirmwareBuffer::getExBufferQnty() {
    return extendedBuffers.size();
}

CFirmwareBuffer CFirmwareBuffer::getExBuffer(unsigned char id) {

    unsigned char index = 0;
    for (int key : extendedBuffers.keys()) {
        if (id == index){
            return extendedBuffers[key];
        }else{
            index++;
        }
    }
    CFirmwareBuffer dummy;
    return dummy;
}

void CFirmwareBuffer::resetOffset() {
    offset = 0;
}

CFirmwareBuffer CFirmwareBuffer::getNotNullDataBlock(unsigned char length) {
    CFirmwareBuffer buf;
    do {
        buf = this->subbuffer(offset, length);
        offset += buf.size();

    } while (buf.isNullDataBuffer() && buf.size());

    if (buf.size() > 0 && buf.size() < 8) {
        length = buf.size();
        for (size_t i = length; i < 8; i++) {
            buf << (unsigned char) NULL_DATA;
        }
    }
    buf.setOffsetAddress(startAddress + offset - length);

    return buf;
}

CFirmwareBuffer CFirmwareBuffer::getDataBlock(unsigned char length) {
    CFirmwareBuffer buf;
    do {
        buf = this->subbuffer(offset, length);
        offset += buf.size();

    } while (buf.isNullDataBuffer() && buf.size());

    if (buf.size() > 0 && buf.size() < 8) {
        length = buf.size();
    }
    buf.setOffsetAddress(startAddress + offset - length);

    return buf;
}

unsigned char CFirmwareBuffer::getReadingProgress() {
    //    cout << "offset: " << offset << ", len: " << this->size() << ", prog: " << offset * 100 / this->size() << endl;
    return (offset * 100 / this->size());
}

unsigned int CFirmwareBuffer::getDataBlockAddress() {
    return offsetAddress;
}

void CFirmwareBuffer::setOffsetAddress(unsigned int offset) {
    this->offsetAddress = offset;
}

bool CFirmwareBuffer::isNullDataBuffer() {
    for (size_t i = 0; i < this->size(); i++) {
        if (this->operator [](i) != NULL_DATA) {
            return false;
        }
    }
    return true;
}

