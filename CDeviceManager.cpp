#include "CDeviceManager.h"

CDeviceManager::CDeviceManager(QObject *parent) : QObject(parent)
{
    logger = &CSingleton<CLogger>::Instance();
    config = &CSingleton<CConfig>::Instance();
    devicesConfig = &CSingleton<CInitDevicesConfig>::Instance();
    lastPing = QDateTime::currentDateTime();
    lastDeviceAvailabilityChecking = QDateTime::currentDateTime().addSecs(-10);
    pingInterval = config->getConfigParam(PING_INTERVAL).toInt();
    maxLastSeenTime = config->getConfigParam(MAX_LAST_SEEN_TIME).toInt();
    expectedParamInterval = config->getConfigParam(EXPECTED_PARAM_INTERVAL).toInt();
}

CDeviceManager::~CDeviceManager()
{
    destroyDevices(devicesList);
    destroyDevices(specialDevicesList);
}


void CDeviceManager::assignTransceiverPointer(CTransceiver *transceiver){
    this->transceiver = transceiver;
}

void CDeviceManager::loadDevices(){
    loadDevicesFileData();
    emit sendDevicesListChangedSignal();
}

CCanDevice* CDeviceManager::createDeviceForCategory(DevCat category){

    CCanDevice *device;
    switch(category){
    case DevCat::BroadCast:
        device = new CDevBroadcast;
        break;
    case DevCat::BistableSwitch:
        device = new CDevBistableSwitch;
        break;
    case DevCat::BistableSwitchSensor:
        device = new CDevBistableSensor;
        break;
    case DevCat::PWM:
        device = new CDevPwmDriver;
        break;
    case DevCat::RGB:
        device = new CDevRgbDriver;
        break;
    case DevCat::MotionDetector:
        device = new CDevMotionDetector;
        break;
        //    case EDeviceCategory::BistableSwitch:
        //        device = new CDevBistableSwitch;
        //        break;
    default:
        device = new CCanDevice;
    }

    if (transceiver != NULL){
        device->assignTransceiver(transceiver);
    }else{
        logger->warning(DEVICE_MANAGER, "Trying to assign invalid pointer of transceiver");
    }

    return device;
}


QList<CCanDevice*> CDeviceManager::getCanDevices(){
    QMutexLocker locker(&devManagerMutex);
    return devicesList.values();
}

void CDeviceManager::startUploadFirmware (QString fileName){
    if (!devicesLocked){
        devicesLocked = true;
        firmwareFileName = fileName;
        emit sendPauseThreadsSignal();
    }
}

void CDeviceManager::uploadFirmware(){

    if (firmwareFileName.length() > 0){
        transceiver->configForCanA();
        QThread::msleep(1000);
        CCanDevice device;
        device.assignTransceiver(transceiver);
        device.uploadFirmware(firmwareFileName);
        firmwareFileName = "";
        transceiver->configForCanB();
        emit sendResumeThreadSignal();
        devicesLocked = false;
    }

}

EAddressingStatus CDeviceManager::getAddresses(const CCanBuffer &buffer, QVector<unsigned char> &addresses){

    QVector<CCanDevice*> newDevices;
    unsigned char address;
    DevCat category;

    try{
        unsigned char addressingFrameId = buffer.getAddressingModeFrameId();
        UID uid = buffer.getSourceUID();
        logger->low_info(DEVICE_MANAGER, "Getting addresses for device with UID " + QString::number(uid, 16));
        logger->debug(DEVICE_MANAGER, "Request address frame ID: " + QString::number((int)addressingFrameId, 16));
        if (addressingFrameId == 0){
            loadAddressesForUID(uid);
            currentlyAddressedDevicesUID.push_back(uid);
        }else if(addressingFrameId == CMD_ADDRESSING_COMPLETED){
            currentlyAddressedDevicesUID.removeAll(uid);
            emit sendDevicesListChangedSignal();
            return EAddressingStatus::ADDRESSING_COMPLETED;
        }else{
            if (!currentlyAddressedDevicesUID.contains(uid)){
                return EAddressingStatus::ADDRESSING_INVALID_FRAME_ID;
            }
        }

        if (tempDevicesList[uid].size() > 0){
            logger->low_info(DEVICE_MANAGER, "Addresses already exists in database");
            for (size_t i = OFFSET_CAN_REQ_ADDRESS; i < buffer.size(); i++){
                category = getRequestedCategory(buffer, i);
                address = readAddressForCategory(uid, category);
                logger->low_info(DEVICE_MANAGER, "Loading address " + QString::number((int)address) + " for category " + CCanDevice::categoryToString(category));
                addresses.push_back(address);
            }
        }else{
            logger->low_info(DEVICE_MANAGER, "Creating new addresses");
            for (size_t i = OFFSET_CAN_REQ_ADDRESS; i < buffer.size(); i++){
                category = getRequestedCategory(buffer, i);
                address = getFirstFreeAddress(category);
                logger->low_info(DEVICE_MANAGER, "Created new address " + QString::number((int)address) + " for category " + CCanDevice::categoryToString(category));
                addresses.push_back(address);
                CCanDevice* dev = createNewCanDevice(category, uid, address);
                newDevices.push_back(dev);
                devicesList.insert(dev->getDeviceId(), dev);
                updateDevicesFileData();

            }

        }

    }catch(CException ex){
        addresses.clear();
        for (CCanDevice *device : newDevices){
            devicesList.remove(device->getDeviceId());
        }
        destroyDevices(newDevices);
        updateDevicesFileData();
        logger->error(DEVICE_MANAGER, "Address request frame refused because of: " + ex.what());
        return EAddressingStatus::ADDRESSING_ERROR;
    }

    return EAddressingStatus::ADDRESSING_OK;
}

QDataStream & operator<< (QDataStream& stream, CCanDevice& device){

    stream << device.getUID();
    stream << device.name;
    stream << static_cast<unsigned char>(device.getCategory());
    stream << device.getAddress();

    return stream;
}

QDataStream & operator>> (QDataStream& stream, CCanDevice& device){

    UID uid;
    stream >> uid;
    device.setUID(uid);
    stream >> device.name;

    unsigned char category;
    stream >> category;
    
    unsigned char address;
    stream >> address;

    device.setCategoryAndAddress(static_cast<DevCat>(category), address);
    return stream;
}


void CDeviceManager::updateDevicesFileData(){
    logger->info(DEVICE_MANAGER, "Updating devices file data");
    QFile file(DEVICES_FILE_DATA);
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);

    out << devicesList.size();
    for (CCanDevice *dev : devicesList.values()){
        out << *dev;
    }

    file.close();
}

void CDeviceManager::setDeviceName(CCanDevice *device, QString name){
    device->name = name;
    updateDevicesFileData();
}

void CDeviceManager::loadDevicesFileData(){
    logger->info(DEVICE_MANAGER, "Loading devices from file data");
    QFile file(DEVICES_FILE_DATA);
    file.open(QIODevice::ReadOnly);
    QDataStream in (&file);

    int size;
    in >> size;
    for (int i = 0; i < size; i++){
        CCanDevice d;
        in >> d;
        CCanDevice *device = createDeviceForCategory(d.getCategory());
        device->setCategoryAndAddress(d.getCategory(), d.getAddress());
        device->name = d.name;
        device->setUID(d.getUID());
        devicesList.insert(device->getDeviceId(), device);
        logger->low_info(DEVICE_MANAGER, "New device added to the devices list: " + device->objectName());
    }
}

void CDeviceManager::destroyDevices(QMap<QString, CCanDevice*> devices) {
    
    for (QString key : devices.keys()) {
        delete devices[key];
        devices[key] = NULL;
    }

    devices.clear();
}

void CDeviceManager::destroyDevices(QVector<CCanDevice *> &devices) {
    CCanDevice *device;
    while(devices.size() > 0){
        device = devices.first();
        devices.remove(0);
        delete device;
        device = NULL;
    }
}

unsigned char CDeviceManager::getFirstFreeAddress(DevCat category){
    unsigned char address = 1;
    QVector<unsigned char> addresses;
    for (CCanDevice *device : devicesList.values()){
        if (device->getCategory() == category){
            addresses.push_back(device->getAddress());
        }
    }
    qSort(addresses);
    for (unsigned char adr : addresses){
        if (address < adr){
            break;
        }else{
            address++;
        }
    }
    return address;
}


CCanDevice* CDeviceManager::createNewCanDevice(DevCat category, UID uid, unsigned char address){

    CCanDevice *device = createDeviceForCategory(category);
    device->setCategoryAndAddress(category, address);
    device->setUID(uid);
    return device;
}

DevCat CDeviceManager::getRequestedCategory(const CCanBuffer &buffer, size_t index){
    int catId = buffer[index];
    DevCat cat = static_cast<DevCat>(catId);
    if (! CCanDevice::checkCategory(cat)){
        throw CException(QString("Category ID = ") + QString::number(catId)+ QString(" NOT exists"));
    }
    return cat;

}


unsigned char CDeviceManager::readAddressForCategory(const UID uid, const DevCat category){
    unsigned char adr;
    if (tempDevicesList.find(uid) != tempDevicesList.end()){
        for (CCanDevice *dev : tempDevicesList[uid]){
            if (dev->getCategory() == category){
                adr = dev->getAddress();
                tempDevicesList[uid].removeOne(dev);
                return adr;
            }else{
                throw CException(QString("Overrange address requested for category ") +
                                 QString::number((int)category) + " for device with UID: " +
                                 QString::number(uid), this);

            }
        }
    }
    return 0;
}

void CDeviceManager::loadAddressesForUID(UID uid){
    QVector<CCanDevice*> devices;
    if (tempDevicesList.find(uid) != tempDevicesList.end()){
        tempDevicesList.remove(uid);
    }
    for (CCanDevice *device : devicesList.values()){
        if (device->getUID() == uid){
            devices.push_back(device);
        }
    }
    if (devices.size() > 0){
        tempDevicesList[uid] = devices;
    }
}

CCanDevice* CDeviceManager::getCanDevice(const CCanBuffer &incomingBuffer){
    QMutexLocker locker(&devManagerMutex);
    if (devicesLocked){
        return NULL;
    }
    for (CCanDevice *device : devicesList.values()){
        if (device->getAddress() == incomingBuffer.getSourceAddress() &&
                device->getCategory() == incomingBuffer.getSourceCategory()){
            return device;
        }
    }
    logger->error(DEVICE_MANAGER, "Device has not been found [category: " +
                  CCanDevice::categoryToString(incomingBuffer.getSourceCategory()) +
                  ", address: " +
                  QString::number(static_cast<int>(incomingBuffer.getSourceAddress()))+ "]");

    return 0;
}

CCanDevice* CDeviceManager::getFirstCanDevice(const UID &uid){
    QMutexLocker locker(&devManagerMutex);
    if (devicesLocked){
        return NULL;
    }
    for (CCanDevice *device : devicesList.values()){
        if (device->getUID() == uid){
            return device;
        }
    }
    logger->warning(DEVICE_MANAGER, "Device for UID: [" + QString::number(uid, 16)+ "] has not been found");

    for (CCanDevice *device : specialDevicesList){
        if (device->getUID() == uid){
            logger->error(DEVICE_MANAGER, "Temporary device exists");
            return device;
        }
    }
    logger->error(DEVICE_MANAGER, "Creating temporary device");
    CCanDevice *device = createDeviceForCategory(DevCat::BroadCast);
    device->setCategoryAndAddress(DevCat::BroadCast, 0);
    device->setUID(uid);
    specialDevicesList.push_back(device);
    return device;
}

QList<CCanDevice*> CDeviceManager::getCanDevices(const UID &uid){
    QMutexLocker locker(&devManagerMutex);
    QList<CCanDevice*> devList;

    if (devicesLocked){
        return devList;
    }


    for (CCanDevice *device : devicesList.values()){
        if (device->getUID() == uid){
            devList.push_back(device);
        }
    }

    if (devList.isEmpty()) {
        logger->warning(DEVICE_MANAGER, "Device has not been found for UID: [" + QString::number(uid, 16)+ "]");
    }

    return devList;
}

CCanDevice* CDeviceManager::getCanDevice(QString deviceId) {
    QMutexLocker locker(&devManagerMutex);
    if (devicesList.contains(deviceId)) {
        return devicesList[deviceId];
    }

    return NULL;
}

CCanDevice* CDeviceManager::getCategoryBroadcastDevice(DevCat category, unsigned char address){
    for (CCanDevice *device : specialDevicesList){
        if (device->getCategory() == category && device->getAddress() == address){
            return device;
        }
    }
    CCanDevice *device = createDeviceForCategory(category);
    device->setCategoryAndAddress(category, 0);
    specialDevicesList.push_back(device);
    return device;
}

CCanDevice* CDeviceManager::getAddressBroadcastDevice(DevCat category, unsigned char address){
    for (CCanDevice *device : specialDevicesList){
        if (device->getCategory() == category && device->getAddress() == address){
            return device;
        }
    }
    CCanDevice *device = createDeviceForCategory(category);
    device->setCategoryAndAddress(category, 0);
    specialDevicesList.push_back(device);
    return device;
}

CCanDevice* CDeviceManager::getCanDevice(DevCat category, unsigned char address){
    QMutexLocker locker(&devManagerMutex);
    if (devicesLocked){
        return NULL;
    }

    if (category == DevCat::BroadCast){
        return getCategoryBroadcastDevice(category, address);
    }

    if (address == 0){
        return getAddressBroadcastDevice(category, address);
    }

    for (CCanDevice *device : devicesList.values()){
        if (device->getCategory() == category && device->getAddress() == address){
            return device;
        }
    }
    CLogger *logger = &CSingleton<CLogger>::Instance();
    logger->error(DEVICE_MANAGER, "Device has not been found in category " + CCanDevice::categoryToString(category) + " with address " + QString::number((int)address));

    return NULL;
}



void CDeviceManager::executeActionForCommand(const CCanBuffer &incomingBuffer){
    CCanDevice *device = getCanDevice(incomingBuffer);
    if (device != NULL){
        device->executeCommand(incomingBuffer.getCommand(), incomingBuffer);
        device->updateLastSeenTime();
    }
}

UID CDeviceManager::getUID(unsigned char address, DevCat category){
    for (CCanDevice *device : devicesList.values()){
        if (device->getAddress() == address && category == device->getCategory()){
            return device->getUID();
        }
    }
    return 0;
}

void CDeviceManager::clearDeviceStatus(const UID &uid){

    logger->low_info(DEVICE_MANAGER, "Clearing status for devices with UID: " + QString::number(uid, 16));
    for (CCanDevice *device : devicesList.values()){
        if (device->getUID() == uid){
            device->clearStatuses();
        }
    }
}

void CDeviceManager::handleStartingUpDevice(const UID &uid, SDeviceStartupInfo startupInfo) {
    QList<CCanDevice*> devList = getCanDevices(uid);
    QString devName = "";
    QList<CDeviceConfig> deviceConfig;

    if (!devList.isEmpty()) {
        devName = devList[0]->objectName();
        qint64 tmstmp = QDateTime::currentMSecsSinceEpoch();

        for (CCanDevice* device : devList) {
            deviceConfig.append(devicesConfig->getConfigForDevice(device->getCategory(), device->getAddress()));
            device->startupTimestamp = tmstmp;
        }
    }

    QString msg = "Device [" + devName + "] with UID: " + QString::number(uid, 16) + " is starting up";
    if (startupInfo.startupInfo == EStartupInfoType::RESET_BY_WDT) {
        msg += ", reseted by WDT";
    } else if (startupInfo.startupInfo == EStartupInfoType::RESET_BY_RESET_INSTRUCTION) {
        msg += ", reseted by internal RESET instruction";
    } else if (startupInfo.startupInfo == EStartupInfoType::STARTED_ON_POWER_UP) {
        msg += " after power on";
    }

    msg += ", RCON: 0x" + QString::number(startupInfo.RCONRegister, 16);

    logger->info(SYSTEM, msg);

    if (startupInfo.startupInfo != EStartupInfoType::RESET_BY_WDT && startupInfo.startupInfo != EStartupInfoType::RESET_BY_RESET_INSTRUCTION) {
        configureDevices(deviceConfig);
    }
    
}


void CDeviceManager::pingDevice(){
    QDateTime currentTime = QDateTime::currentDateTime();
    if ((currentTime.toMSecsSinceEpoch() - lastPing.toMSecsSinceEpoch()) > pingInterval){
        //qDebug()<<"========= PING DEVICE METHOD  ============";

        CCanDevice* device = getTheLatestSeenDevice();

        if (device == NULL){
            //            qDebug()<<"==PING DEVICE no device found in 1st iteration";
            devicesWithPingRequest.clear();
            device = getTheLatestSeenDevice();
        }else{
            //            qDebug()<<"==PING DEVICE device found in 1st iteration: " << device->objectName();
        }

        if (device != NULL){
            device->sendPing();
            devicesWithPingRequest.push_back(device);
        }else{
            //            qDebug()<<"==PING DEVICE no device found in 2nd iteration";
        }
        lastPing = QDateTime::currentDateTime();
    }
}

CCanDevice* CDeviceManager::getTheLatestSeenDevice(){
    CCanDevice* dev = NULL;
    QDateTime currentTime = QDateTime::currentDateTime();
    qint64 lastSeenTime = currentTime.toMSecsSinceEpoch() - maxLastSeenTime;
    for (CCanDevice* device : devicesList.values()){
        //        qDebug()<<"==PING DEVICE check device " << device->objectName();
        if (device->getLastSeenTime() < lastSeenTime){
            //            qDebug()<<"==PING DEVICE device the latest seen in "<<QString::number(device->getLastSeenTime());
            if (checkDevicesWithPingRequestList(device)){
                //                qDebug()<<"==PING DEVICE @@ device on the PING LIST";
                continue;
            }else{
                dev = device;
                lastSeenTime = device->getLastSeenTime();
            }
        }
    }

    return dev;
}

bool CDeviceManager::checkDevicesWithPingRequestList(CCanDevice *device){
    for (CCanDevice *dev : devicesWithPingRequest){
        if (device->getUID() == dev->getUID()){
            return true;
        }
    }
    return false;
}

void CDeviceManager::checksDevicesAvailability(){
    QDateTime currentTime = QDateTime::currentDateTime();
    if ((currentTime.toMSecsSinceEpoch() - lastDeviceAvailabilityChecking.toMSecsSinceEpoch()) > 5000){
        for (CCanDevice *device : devicesList.values()){
            if (device->isActive()){
                for (QString parameter : device->parameters.keys()){
                    if (device->parameters[parameter].undefined){
                        logger->info(DEVICE_MANAGER, "Device " + device->objectName() + " is active and has parameter " + parameter + " value undefined");
                        // device->requestParameter(parameter); 
                        addNewDeviceParameterRequest(device, parameter);
                    }
                }
            }else{
                device->clearStatuses();
                removeActionsFromQueueForDevice(device);
                removeExpectedParameterFromQueueForDevice(device);
                removeRequestParameterFromQueueForDevice(device);
            }
        }
        lastDeviceAvailabilityChecking = QDateTime::currentDateTime();
    }
}

void CDeviceManager::resetAddressesInDevices(){
    logger->info(DEVICE_MANAGER, "Reseting addresses in devices");
    CCanDevice *device = getCanDevice(DevCat::BroadCast, 0);
    if (device != NULL){
        device->sendInitCommand();
    }
}

void CDeviceManager::bootDevice(UID uid){
    logger->info(DEVICE_MANAGER, "Booting device with UID: " + QString::number(uid));
    CCanDevice *bootDevice = getFirstCanDevice(uid);
    if (bootDevice == NULL){
        logger->info(DEVICE_MANAGER, "Device with UID " + QString::number(uid) + " has not been found, temporary device will be created");
        bootDevice = new CCanDevice;
        bootDevice->setUID(uid);
        bootDevice->assignTransceiver(transceiver);
        bootDevice->sendBootCommand();
        delete bootDevice;
        bootDevice = NULL;
    }else{
        bootDevice->sendBootCommand();
        bootDevice->clearStatuses();
    }


}

void CDeviceManager::removeDevice(UID uid){
    logger->info(DEVICE_MANAGER, "Removing device with UID: " + QString::number(uid));
    QVector<CCanDevice*> deviceToRemove;
    for (CCanDevice*device : devicesList.values()){
        if (device->getUID() == uid){
            deviceToRemove.push_back(device);
        }
    }

    removeDevicesFromLists(deviceToRemove);
    deviceToRemove.clear();
    updateDevicesFileData();
    emit sendDevicesListChangedSignal();
}

void CDeviceManager::removeDevice(DevCat category, int addressBegin, int addressEnd){
    logger->info(DEVICE_MANAGER, "Removing devices in category: " + CCanDevice::categoryToString(category)
                 + "(" + QString::number(static_cast<int>(category)) + ") from address "
                 + QString::number(addressBegin) + " to address " + QString::number(addressEnd));
    QVector<CCanDevice*> deviceToRemove;
    for (CCanDevice*device : devicesList.values()){
        if (device->getCategory() == category){
            if (addressEnd >= addressBegin
                     && device->getAddress() >= addressBegin
                     && device->getAddress() <= addressEnd
                    ){
                deviceToRemove.push_back(device);
            }

        }
    }
//    qDebug()<<"removing "<<QString::number(deviceToRemove.size())<<" devices";
    removeDevicesFromLists(deviceToRemove);
    deviceToRemove.clear();
    updateDevicesFileData();
    emit sendDevicesListChangedSignal();
}

void CDeviceManager::removeDevicesFromLists(QVector<CCanDevice*> deviceToRemove){
    for (CCanDevice* dev : deviceToRemove){
        removeActionsFromQueueForDevice(dev);
        removeExpectedParameterFromQueueForDevice(dev);
        removeRequestParameterFromQueueForDevice(dev);

        devicesWithPingRequest.removeAll(dev);
        specialDevicesList.removeAll(dev);
        devicesList.remove(dev->getDeviceId());
        currentlyAddressedDevicesUID.removeAll(dev->getUID());
        delete dev;
        dev = NULL;
    }
}

void CDeviceManager::removeActionsFromQueueForDevice(CCanDevice *device){
    QMutexLocker locker(&actionsQueueMutex);

    for (SDeviceAction action: actionsQueue){
        if (action.device->getUID() == device->getUID()){
            logger->debug(DEVICE_MANAGER, "Action definition for device " + device->objectName() + " removed from queue");
            actionsQueue.removeOne(action);
        }
    }
}

void CDeviceManager::removeExpectedParameterFromQueueForDevice(CCanDevice *device){
    QMutexLocker locker(&expDeviceParamMutex);

    for (SDeviceExpectedStatus expStatus: expectedDeviceParameters){
        if (expStatus.expectedStatus.device->getUID() == device->getUID()
                || expStatus.action.device->getUID() == device->getUID()){
            logger->debug(DEVICE_MANAGER, "Expected parameter for device " + device->objectName() + " removed from queue");
            expectedDeviceParameters.removeOne(expStatus);
        }
    }
}

void CDeviceManager::removeRequestParameterFromQueueForDevice(CCanDevice *device){
    QMutexLocker locker(&requestQueueMutex);
    for (SDeviceRequest request: requestQueue){
        if (request.device->getUID() == device->getUID()){
            logger->debug(DEVICE_MANAGER, "Request parameter for device " + device->objectName() + " removed from queue");
            requestQueue.removeOne(request);
        }
    }
}

void CDeviceManager::pingResponse(const CCanBuffer &incomingBuffer){
    CCanDevice *device = getCanDevice(incomingBuffer);
    if (device == NULL){
        return;
    }
    for (CCanDevice *dev : devicesList.values()){
        if (dev == device){
            dev->updateLastSeenTime();
            dev->resetCounter = incomingBuffer.getResetCounter();
            dev->wdtResetCounter = incomingBuffer.getWdtResetCounter();
            logger->low_info(DEVICE_MANAGER, "Device " + dev->objectName() + " sent ping response");
        }
    }
}


void CDeviceManager::requestDeviceParameter(QString deviceId, QString parameter){
    addNewDeviceParameterRequest(getCanDevice(deviceId), parameter);
}

void CDeviceManager::requestDeviceParameter(DevCat category, unsigned char address, QString parameter){
    addNewDeviceParameterRequest(getCanDevice(category, address), parameter);
}

void CDeviceManager::addNewDeviceParameterRequest(CCanDevice *device, QString parameter){
    QMutexLocker locker(&requestQueueMutex);
    if (device == NULL) {
        logger->error(DEVICE_MANAGER, "New parameter request (" + parameter + ") cannot be created for NULL device ");
        return;
    }

    if (!device->parameters.contains(parameter)) {
        logger->error(DEVICE_MANAGER, "Device " + device->objectName() + " has NO " + parameter + " parameter, parameter request skipped");
        return;
    }

    logger->low_info(DEVICE_MANAGER, "New parameter request created (" + parameter + ") for device " + device->objectName());
    for (SDeviceRequest request : requestQueue){
        if (*request.device == *device && request.parameter == parameter){
            logger->low_info(DEVICE_MANAGER, "Parameter request already exists in the queue");
            return;
        }
    }

    SDeviceRequest request;
    request.device = device;
    request.parameter = parameter;
    request.lastParamRequestTmstp = 0;
    requestQueue.push_back(request);
    logger->low_info(DEVICE_MANAGER, "Parameter request added to the queue");
}

void CDeviceManager::checkDeviceParameterRequestQueue(){
    QMutexLocker locker(&requestQueueMutex);

    if (requestQueue.size() > 0) {
        qint64 currentTime = QDateTime::currentMSecsSinceEpoch();
        logger->low_info(DEVICE_MANAGER, "Device parameter requests queue size: " + QString::number(requestQueue.size()) + 
                    ", current time: " + QString::number(currentTime));
 
        QList<SDeviceRequest>::iterator request = requestQueue.begin();
        while (request < requestQueue.end()){
            logger->low_info(DEVICE_MANAGER, "Request info: device (" +
                                 request->device->objectName()+
                                 ") parameter (" + request->parameter + "), last tmstmp: " + QString::number(request->lastParamRequestTmstp));

            if (request->lastParamRequestTmstp == 0){
                logger->low_info(DEVICE_MANAGER, "Request is new and timestamp of the request is updated, request is executed");
                request->device->requestParameter(request->parameter);
                request->lastParamRequestTmstp = QDateTime::currentMSecsSinceEpoch();

            } else if ((currentTime - request->lastParamRequestTmstp) > 30000){
                logger->low_info(DEVICE_MANAGER, "Removing device request from queue after timeout");
                requestQueue.erase(request);

                return;
            } else if (!request->device->parameters[request->parameter].undefined) {
                logger->low_info(DEVICE_MANAGER, "Removing device request from queue - parameter already updated");
                requestQueue.erase(request);

                return;
            }

            ++request;
        }
    }
    

}

void CDeviceManager::configureDevices(QList<CDeviceConfig> configs) {
    if (configs.isEmpty()) {
        logger->info(DEVICE_MANAGER, "No device configurations");
    } else {
        logger->info(DEVICE_MANAGER, "Configuring devices, configs number: " + QString::number(configs.length()));
        foreach(CDeviceConfig config, configs) {
            addNewDeviceParameterStatus(config.getDevCategory(), config.getDevAddress(), config.getParam(), config.getValue());
        }
    }
}

void CDeviceManager::configureDevices() {
    configureDevices(devicesConfig->getDevicesConfig());
}

void CDeviceManager::addNewDeviceParameterStatus(DevCat category, unsigned char address, QString parameter, qint64 value){
    addNewActionForDevice(getCanDevice(category, address), parameter, value);
}

bool operator == (SDeviceAction act1, SDeviceAction act2){
    return (*act1.device == *act2.device &&
            act1.parameter == act2.parameter &&
            act1.value == act2.value);
}

bool operator == (SDeviceExpectedStatus stat1, SDeviceExpectedStatus stat2){
    return (stat1.expectedStatus == stat2.expectedStatus &&
            stat1.startTime == stat2.startTime);
}

bool operator == (SDeviceRequest req1, SDeviceRequest req2){
    return (*req1.device == *req2.device &&
            req1.parameter == req2.parameter &&
            req1.lastParamRequestTmstp == req2.lastParamRequestTmstp);
}

void CDeviceManager::addNewActionForDevice(CCanDevice *device, QString parameter, qint64 value){
    QMutexLocker locker(&actionsQueueMutex);

    if (device == NULL){
        logger->error(DEVICE_MANAGER, "New action for NULL device created");
        return;
    }

    logger->low_info(DEVICE_MANAGER, "Creating new action definition (" + parameter + " = " + QString::number(value) +") for device "
                            + device->objectName());

    QList<SDeviceAction>::iterator it = actionsQueue.begin();
    while (it < actionsQueue.end()){
        if (*it->device == *device){
            if (it->parameter == parameter){
                if (it->value == value){
                    logger->debug(DEVICE_MANAGER, "Action definition already exists in the queue");
                    return;
                }else{
                    logger->debug(DEVICE_MANAGER, "Action definition already exists in the queue but with the different parameters value, value is updated");
                    it->value = value;
                    return;
                }
            }
        }
        it++;
    }

    //if no element exists in the actionsQueue list
    SDeviceAction action;
    action.device = device;
    action.parameter = parameter;
    action.value = value;
    actionsQueue.push_back(action);
    logger->low_info(DEVICE_MANAGER, "Action definition (" + parameter + " = " + QString::number(value) +") for device "
                            + device->objectName() + " added to the queue");
}

bool CDeviceManager::isBroadCastDevice(CCanDevice *device){
    return (device->getCategory() == DevCat::BroadCast ||
            device->getAddress() == 0);
}

void CDeviceManager::addNewExpectedDeviceParameter(CCanDevice *device, QString expectedParameter, qint64 expectedValue,
                                                   QString actionParameter, qint64 actionValue){
    QMutexLocker locker(&expDeviceParamMutex);
    if (expectedParameter == ""){
        return;
    }

    if (device == NULL){
        logger->error(DEVICE_MANAGER, "New expected parameter value request created for NULL device");
        return;
    }

    if (!device->parameters.contains(expectedParameter)){
        logger->error(DEVICE_MANAGER, "Device " + device->objectName() + " has NO " +  expectedParameter + " parameter, expected parameter request skipped");
        return;
    }

    logger->low_info(DEVICE_MANAGER, "New expected parameter value ("
                                     + expectedParameter + " = " + QString::number(expectedValue)
                                     + ") request created for device " + device->objectName() + ", action details: "
                                     + actionParameter + " = " + QString::number(actionValue));
    QList<SDeviceExpectedStatus>::iterator it = expectedDeviceParameters.begin();
    while (it < expectedDeviceParameters.end()){
        if (*it->expectedStatus.device == *device){
            if (it->expectedStatus.parameter == expectedParameter){
                if (it->expectedStatus.value == expectedValue){
                    logger->low_info(DEVICE_MANAGER, "Expected parameter value already exists in the queue");
                }else{
                    it->expectedStatus.value = expectedValue;
                    it->action.device = device;
                    it->action.parameter = actionParameter;
                    it->action.value = actionValue;
                    logger->low_info(DEVICE_MANAGER, "Expected parameter value already exists in the queue but with different value, value is updated");
                }
                it->startTime = QDateTime::currentMSecsSinceEpoch();
                return;
            }
        }
        it++;
    }


    //if no element exists in the actionsQueue list
    SDeviceExpectedStatus status;
    status.expectedStatus.device = device;
    status.expectedStatus.parameter = expectedParameter;
    status.expectedStatus.value = expectedValue;
    status.action.device = device;
    status.action.parameter = actionParameter;
    status.action.value = actionValue;
    status.startTime = QDateTime::currentMSecsSinceEpoch();

    expectedDeviceParameters.push_back(status);
    logger->low_info(DEVICE_MANAGER, "Expected parameter value added to the queue");
}

void CDeviceManager::checkExpectedDeviceParameterStatus(){
    QMutexLocker locker(&expDeviceParamMutex);

    qint64 timeoutTime = QDateTime::currentMSecsSinceEpoch() - expectedParamInterval;

    QList<SDeviceExpectedStatus>::iterator expectedQueue = expectedDeviceParameters.begin();
    while (expectedQueue < expectedDeviceParameters.end()){

        if (expectedQueue->expectedStatus.device->parameters[expectedQueue->expectedStatus.parameter].undefined ||
                expectedQueue->expectedStatus.device->parameters[expectedQueue->expectedStatus.parameter].value != expectedQueue->expectedStatus.value){

            if (expectedQueue->startTime < timeoutTime){

                logger->low_info(DEVICE_MANAGER, "Expected parameter <"
                                 + expectedQueue->expectedStatus.parameter
                                 + " = " + QString::number(expectedQueue->expectedStatus.value)
                                 + "> is " + 

                                ((expectedQueue->expectedStatus.device->parameters[expectedQueue->expectedStatus.parameter].undefined)?"undefined":(
                                    "different than actual ("
                                    + QString::number(expectedQueue->expectedStatus.device->parameters[expectedQueue->expectedStatus.parameter].value)
                                    + ")"
                                ))
                                 

                                 + " for device " + expectedQueue->action.device->objectName()
                                 + " and new action is created");
                expectedQueue->startTime = QDateTime::currentMSecsSinceEpoch();
                addNewActionForDevice(expectedQueue->action.device, expectedQueue->action.parameter, expectedQueue->action.value);
                addNewDeviceParameterRequest(expectedQueue->action.device, expectedQueue->expectedStatus.parameter);
            } else {
                logger->low_info(DEVICE_MANAGER, "Expected parameter <"
                                 + expectedQueue->expectedStatus.parameter
                                 + " = " + QString::number(expectedQueue->expectedStatus.value)
                                 + "> is " + 

                                ((expectedQueue->expectedStatus.device->parameters[expectedQueue->expectedStatus.parameter].undefined)?"undefined":(
                                    "different than actual ("
                                    + QString::number(expectedQueue->expectedStatus.device->parameters[expectedQueue->expectedStatus.parameter].value)
                                    + ")"
                                ))
                                 

                                 + " for device " + expectedQueue->action.device->objectName());
            }

            ++expectedQueue;
        }else{
            logger->low_info(DEVICE_MANAGER, "Removing expected parameter request ("
                                             + expectedQueue->action.parameter + " = " +    QString::number(expectedQueue->action.value)
                                             + ") for device " + expectedQueue->action.device->objectName() + " from the queue");
            expectedDeviceParameters.erase(expectedQueue);

            return;
        }
    }


}

void CDeviceManager::checkActionsQueue(){
    QMutexLocker locker(&actionsQueueMutex);

    SCommandSet commSet;

    QList<SDeviceAction>::iterator it = actionsQueue.begin();
    while (it < actionsQueue.end()){
        logger->low_info(DEVICE_MANAGER, "Command to execute in queue found");
        bool removeCurrentAction = false;
        if (isBroadCastDevice(it->device)){
            logger->low_info(DEVICE_MANAGER, "Command to execute is for broadcast device");
            commSet = it->device->executeAction(SCommandSet(it->parameter, it->value));
            actionsQueue.erase(it);
            it++;
            continue;
        }

        if (it->device->parameters.contains(it->parameter) &&
                (it->device->parameters[it->parameter].undefined ||
                 it->device->parameters[it->parameter].value != it->value)){

            if (it->device->parameters[it->parameter].undefined){
                logger->low_info(DEVICE_MANAGER, "Device parameter is undefined");
                addNewDeviceParameterRequest(it->device, it->parameter);
            } else {
                logger->low_info(DEVICE_MANAGER, "Device parameters ("
                    + it->parameter + ") value is different ("
                    + QString::number(it->device->parameters[it->parameter].value)
                    + ") than requested (" + QString::number(it->value) + ")");

                commSet = it->device->executeAction(SCommandSet(it->parameter, it->value));
                removeCurrentAction = true;

                if (commSet.expected) {
                    addNewExpectedDeviceParameter(it->device, commSet.parameter, commSet.value, it->parameter, it->value);
                } else {
                    logger->low_info(DEVICE_MANAGER, "Expected parameter is not set");
                }
            }

            


        } else if (it->device->commands.contains(it->parameter)){
            logger->low_info(DEVICE_MANAGER, "Command ("
                    + it->parameter + ") with value (" + QString::number(it->value)
                    + ") will be executed on the device");

            commSet = it->device->executeAction(SCommandSet(it->parameter, it->value));
            removeCurrentAction = true;
            if (commSet.expected) {
                addNewExpectedDeviceParameter(it->device, commSet.parameter, commSet.value, it->parameter, it->value);
            } else {
                logger->low_info(DEVICE_MANAGER, "Expected parameter is not set");
            }
        }

        if (removeCurrentAction) {
            logger->low_info(DEVICE_MANAGER, "Removing command to execute ("
                    + it->parameter + " = " + QString::number(it->value) +") for device "
                    + it->device->objectName() + " from the queue");
            actionsQueue.erase(it);
        }
        
        it++;
    }
}

void CDeviceManager::assignAddresses(const CCanBuffer& buffer){
    QVector<unsigned char> addresses;
    CCanBuffer response;
    EAddressingStatus status = getAddresses(buffer, addresses);
    if (status == EAddressingStatus::ADDRESSING_ERROR){
        return;
    }else if (status == EAddressingStatus::ADDRESSING_INVALID_FRAME_ID){
        logger->warning(DEVICE_MANAGER, "Invalid addressing frame ID, send INIT command to device with UID: " +
                        QString::number(buffer.getSourceUID(), 16));
        response.setTransmitGlobalId(CMD_CAN_GLOB_INIT, buffer.getSourceUID());
    }else if(status == EAddressingStatus::ADDRESSING_OK){
        response << static_cast<unsigned char>(buffer.getAddressingModeFrameId());
        response << addresses;
        response.setCanID(ID_RES_ADDRESS | buffer.getSourceUID());
    }else if (status == EAddressingStatus::ADDRESSING_COMPLETED){
        response << static_cast<unsigned char>(CMD_ADDRESSING_COMPLETED);
        response.setCanID(ID_RES_ADDRESS | buffer.getSourceUID());
        logger->low_info(DEVICE_MANAGER, "Addressing complete for device with UID " + QString::number(buffer.getSourceUID(), 16));
    }
    transceiver->sendExCanFrame(response);
}
