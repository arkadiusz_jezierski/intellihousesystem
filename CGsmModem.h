#ifndef CGSMMODEM_H
#define CGSMMODEM_H

#include <CSerialPort.h>
#include <CLogger.h>
#include <CConfig.h>
#include <CSingleton.h>
#include <CModemBuffer.h>
#include <QThread>
#include <CSerialPortManager.h>
#include <HTypes.h>

#define REQUEST_MODEM_TOUT 1000
#define RECEIVE_MSG_TOUT 100
#define SEND_MESSAGE_RESPONSE_TOUT 10000
#define MAX_MESSAGE_LENGTH      160

#define MESSAGE_OK                          0x00
#define MESSAGE_INVALID_PHONE_NUMBER        0x01
#define MESSAGE_TOO_LONG                    0x02
#define MESSAGE_CONTAINS_FORBIDDEN_SIGNS    0x04


class CGsmModem  : public CSerialPort
{
    Q_OBJECT

    struct SShortMessage{
        QString phoneNumber;
        QString message;
    };

    enum class EOpenPortResult{
        OK,
        CONFIG_ERROR,
        PORT_NOT_FOUND
    };

    CLogger *logger;
    CConfig *config = NULL;

    QList<SShortMessage> messagesList;

    bool runThread = true;
    bool enabled = false;

    void openPort(QString portName);
    void closeDevicePort();
    QString findPort();
    bool handshake();

    void configForTextMessageSending();
    void configForOnlineMode();
    void configForPrintFullErrorMessages();
    void configForEchoDisabled();
    void configForCharsetGSM();
    void resetModem();

    CModemBuffer getFrame();
    void sendHandshakeCommand();
    void setParameter(QString cmd, QString value);
    void getParameter(QString cmd);
    void sendCommand(QString cmd);
    void checkSendingMessageResponse();
    void getIncomingData();
    void checkNetworkStatus();
    void getMessagesList();

    void sendMessageFromQueue();

    void sendMessage(QString number, QString msg);


public:
    CGsmModem();
    ~CGsmModem();

    EOpenPortResult open();

    void runGsmModuleService();

    static MessageValidationResult validateMessage(QString phoneNumber, QString message);

public slots:
    void addNewMessage(QString phoneNumber, QString message);
    void killThread();

};

#endif // CGSMMODEM_H
