#include "CAction.h"

CAction::CAction()
{
	this->actionType = EActionType::OTHER;
    this->actionId = "EVT" + QString::number(QDateTime::currentMSecsSinceEpoch()) + QString::number(qrand());
}

CAction::~CAction()
{

}


EActionType CAction::getActionType() {
    return actionType;
}
