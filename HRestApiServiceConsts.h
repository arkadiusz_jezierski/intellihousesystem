#ifndef CRESTAPISERVICECONSTS
#define CRESTAPISERVICECONSTS

#define JSON_ID_OPERATIONS  "operation_list"
#define JSON_ID_LOG_FILTERS "log_filters"

#define REST_REQUEST_NAME  "requestName"

#define REST_RES_ERR_CODE       "errorCode"
#define REST_RES_RESULT         "result"
#define REST_RES_JSON_BODY      "json"
#define REST_RES_VALUE          "value"
#define REST_RES_DEVICES        "devices"
#define REST_RES_FILES          "files"
#define REST_RES_LAST_STARTUP   "lastStartup"
#define REST_RES_LAST_SEEN      "lastSeen"
#define REST_RES_RESET_CNT      "resetCounter"
#define REST_RES_WDT_CNT        "wdtCounter"
#define REST_RES_DEV_PARAM_NAME "parameter"
#define REST_RES_DEV_PARAM_DEFINED  "defined"
#define REST_RES_DEV_PARAM_VALUE "value"
#define REST_RES_DEV_PARAMS     "parameters"
#define REST_RES_CATEGORIES     "categories"
#define REST_RES_CAT_PARAM_NAME "name"
#define REST_RES_CAT_PARAM_ID   "id"
#define REST_RES_CAT_COMMANDS   "commands"
#define REST_RES_CAT_PARAMS     "parameters"



#define REST_DEVICE_CAT         "category"
#define REST_DEVICE_ADDR        "address"
#define REST_DEVICE_NAME        "name"
#define REST_DEVICE_ACTIVE      "active"
#define REST_DEVICE_UID         "uid"

#define REST_REQ_PARAM_DEVICE       "device"
#define REST_REQ_PARAM_PARAMETER    "parameter"
#define REST_REQ_PARAM_VALUE        "value"
#define REST_REQ_PARAM_JSON_ID      "id"
#define REST_REQ_PARAM_NAME         "name"
#define REST_REQ_PARAM_JSON_BODY    "json"
#define REST_REQ_PARAM_FILENAME     "fileName"
#define REST_REQ_PARAM_UID          "uid"
#define REST_REQ_PARAM_CAT          "category"
#define REST_REQ_PARAM_ADR_START    "adrStart"
#define REST_REQ_PARAM_ADR_STOP     "adrStop"
#define REST_REQ_PARAM_NUMBER       "number"
#define REST_REQ_PARAM_MSG          "message"
#define REST_REQ_PARAM_ARM          "armDisarm"
#define REST_REQ_PARAM_HASH         "hash"

#define REST_REQ_GET_JSON           "getJson"
#define REST_REQ_SET_DEV_NAME       "setDeviceName"
#define REST_REQ_SET_JSON           "saveJson"
#define REST_REQ_SET_DEV_PARAM      "setDeviceParameter"
#define REST_REQ_GET_DEV_PARAM      "getDeviceParameter"
#define REST_REQ_GET_DEVS_LIST      "getDevicesList"
#define REST_REQ_GET_FILES_LIST     "getFilesList"
#define REST_REQ_UPLOAD_FIRMWARE    "uploadFirmware"
#define REST_REQ_BOOT_DEV           "bootDevice"
#define REST_REQ_INIT_DEVS          "initDevices"
#define REST_REQ_REMOVE_DEV         "removeDevice"
#define REST_REQ_REMOVE_DEV_ADV     "removeDeviceAdvance"
#define REST_REQ_GET_DEV_DETAILS    "getDeviceDetails"
#define REST_REQ_GET_DEV_PARAMS     "getDeviceParameters"
#define REST_REQ_GET_CATEGORIES     "getCategories"
#define REST_REQ_SEND_MESSAGE       "sendMessage"
#define REST_REQ_ARM_SYSTEM         "armSystem"
#define REST_REQ_GET_ARM_STATUS     "getArmSystemStatus"

#endif // CRESTAPISERVICECONSTS

