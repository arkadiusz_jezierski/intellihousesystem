#ifndef CDEVBISTABLESWITCH_H
#define CDEVBISTABLESWITCH_H

#include <QObject>
#include <CCanDevice.h>

#define PARAM_UNBLOCK       "unblock"
#define PARAM_SWITCH        "switch"
#define PARAM_STATE         "state"
#define PARAM_OFF           "off"

class CDevBistableSwitch : public CCanDevice
{
    Q_OBJECT
public:
    explicit CDevBistableSwitch();
    ~CDevBistableSwitch();

    void initParameters();
    void initCommandMethods();
    void initRequestMethods();
    void initExecuteMethods();

    typedef void (CDevBistableSwitch::*method)(CBuffer &data);
    QMap<COMMAND, method> methodsMap;

    typedef void (CDevBistableSwitch::*requestMethod)();
    QMap<QString, requestMethod> requestMethodsMap;

    typedef SCommandSet (CDevBistableSwitch::*executeDeviceCommand)(qint64);
    QMap<QString, executeDeviceCommand> executeDeviceCommandMap;

    void getOutputState(CBuffer &data);
    void setOutputState(CBuffer &data);

    void requestOutputState();

    SCommandSet executeSetOutputState(qint64 value);
    SCommandSet executeSetOutputsOff(qint64 value);
    SCommandSet executeSetOutputStateSwitched(qint64 value);
    SCommandSet executeSetOutputUnblocked(qint64 value);


public slots:

    void commandMethodOfBistableSwitch(COMMAND cmd, CBuffer &data);
    void requestParameterForBistableSwitch(QString param);
    SCommandSet executeCommandForBistableSwitch(SCommandSet commSet);
};

#endif // CDEVBISTABLESWITCH_H
