#ifndef CDEVICEWATCHER_H
#define CDEVICEWATCHER_H

#include <QMap>
//#include <HTypes.h>
#include <QDateTime>

class CDeviceWatcher
{
    QMap<unsigned int, qint64> lastSeenTimeMap;
public:
    CDeviceWatcher();
    ~CDeviceWatcher();

    void updateLastSeenTime(const unsigned int &uid);
    qint64 getLastSeenTime(const unsigned int &uid) const;
};

#endif // CDEVICEWATCHER_H
