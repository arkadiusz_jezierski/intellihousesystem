#ifndef CDEVPWMDRIVER_H
#define CDEVPWMDRIVER_H

#include <QObject>
#include <CCanDevice.h>

#define PARAM_DUTY_CYCLE    "dutyCycle"
#define PARAM_OFF           "off"

class CDevPwmDriver : public CCanDevice
{
    Q_OBJECT
public:
    explicit CDevPwmDriver();
    ~CDevPwmDriver();

    void initParameters();
    void initCommandMethods();
    void initRequestMethods();
    void initExecuteMethods();

    void initSetDeviceParamsMap();

    typedef void (CDevPwmDriver::*method)(CBuffer &data);
    QMap<COMMAND, method> methodsMap;

    typedef void (CDevPwmDriver::*requestMethod)();
    QMap<QString, requestMethod> requestMethodsMap;

    typedef SCommandSet (CDevPwmDriver::*executeDeviceCommand)(qint64);
    QMap<QString, executeDeviceCommand> executeDeviceCommandMap;

    void getDutyCycle(CBuffer &data);
    void setDutyCycle(CBuffer &data);

    void requestDutyCycle();

    SCommandSet executeSetDutyCycle(qint64 value);
    SCommandSet executeSetDutyCycleZero(qint64 value);
    SCommandSet executeSetDutyCycleForCoupleDevices(qint64 value);
    SCommandSet executeSetSameDutyCycleForCoupleDevices(qint64 value);
    SCommandSet executeIncreaseDutyCycleForCoupleDevices(qint64 value);
    SCommandSet executeIncreaseDutyCycle(qint64 value);
    SCommandSet executeDecreaseDutyCycleForCoupleDevices(qint64 value);
    SCommandSet executeDecreaseDutyCycle(qint64 value);

public slots:
    void commandMethodOfPwmDriver(COMMAND cmd, CBuffer &data);
    void requestParameterForPwmDriver(QString param);
    SCommandSet executeCommandForPwmDriver(SCommandSet commSet);

};

#endif // CDEVPWMDRIVER_H
