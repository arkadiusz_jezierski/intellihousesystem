#ifndef CDEVICECONFIG_H
#define CDEVICECONFIG_H

#include <QCoreApplication>
#include <QDebug>
#include "HTypes.h"

class CDeviceConfig
{

	DevCat	category;
	unsigned char address;
    QString parameter;
    qint64 value;

    QString sCategory;
    QString sAddress;
    QString sParam;
    QString sValue;
    
public:
    CDeviceConfig(QString cat, QString addr, QString param, QString val);
    DevCat getDevCategory();
    unsigned char getDevAddress();
    QString getParam();
    qint64 getValue();

    QString toString();
};

#endif // CDEVICECONFIG_H