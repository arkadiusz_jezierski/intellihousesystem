#-------------------------------------------------
#
# Project created by QtCreator 2014-12-28T16:11:57
#
#-------------------------------------------------

QT       += core serialport concurrent network

QT       -= gui

TARGET = IntelliHouseSystem
CONFIG   += console
CONFIG   += c++11
CONFIG   -= app_bundle

QMAKE_CXXFLAGS+=-g
QMAKE_CXXFLAGS+=-Wall
QMAKE_CXXFLAGS_WARN_ON += -Wno-reorder

TEMPLATE = app

SOURCES += main.cpp \
    CTimeOut.cpp \
    CConfig.cpp \
    CLogger.cpp \
    CMainApp.cpp \
    CSerialPort.cpp \
    CException.cpp \
    CSerialBuffer.cpp \
    CCan232.cpp \
    CBuffer.cpp \
    CCanBuffer.cpp \
    CCanDevice.cpp \
    CDeviceManager.cpp \
    CEvent.cpp \
    CDelayAction.cpp \
    CAction.cpp \
    CTransceiver.cpp \
    CDevBistableSwitch.cpp \
    CDevBistableSensor.cpp \
    CEventManager.cpp \
    COperationListLoader.cpp \
    COperation.cpp \
    CFirmwareLoader.cpp \
    CFirmwareBuffer.cpp \
    CDevPwmDriver.cpp \
    CDevBroadcast.cpp \
    CDevRgbDriver.cpp \
    CDeviceWatcher.cpp \
    CDevMotionDetector.cpp \
    CGsmModem.cpp \
    CModemBuffer.cpp \
    CSerialPortManager.cpp \
    CAlarmModule.cpp \
    CErrorException.cpp \
    pistache/cookie.cc \
    pistache/description.cc \
    pistache/endpoint.cc \
    pistache/http_defs.cc \
    pistache/http_header.cc \
    pistache/http_headers.cc \
    pistache/http.cc \
    pistache/listener.cc \
    pistache/mime.cc \
    pistache/net.cc \
    pistache/os.cc \
    pistache/peer.cc \
    pistache/reactor.cc \
    pistache/router.cc \
    pistache/stream.cc \
    pistache/tcp.cc \
    pistache/timer_pool.cc \
    pistache/transport.cc \
    libbool/LibBoolEE.cpp \
    CRestApiService.cpp \
    CRestHandler.cpp \
    COperationContainer.cpp \
    CDevCondition.cpp \
    CTimeCondition.cpp \
    CAlarmCondition.cpp \
    CCondition.cpp \
    CGsmModemAction.cpp \
    CDeviceAction.cpp \
    CTools.cpp \
    CInitDevicesConfig.cpp \
    CDeviceConfig.cpp \
    CLoggerPoster.cpp \
    CRestAction.cpp \
    CRestClientService.cpp

DISTFILES += \
    IntelliHouseSystem.pro.user \
    ToDo.txt \
    ../build-IntelliHouseSystem-Desktop_Qt_5_4_0_GCC_64bit-Debug/operations.json \
    ../build-IntelliHouseSystem-Desktop_Qt_5_4_0_GCC_64bit-Debug/configuration.json \
    operations.json

HEADERS += \
    CTimeOut.h \
    CConfig.h \
    CLogger.h \
    CMainApp.h \
    CSerialPort.h \
    CSingleton.h \
    CException.h \
    CSerialBuffer.h \
    CCan232.h \
    CBuffer.h \
    HCommConstans.h \
    CCanBuffer.h \
    CCanDevice.h \
    HTypes.h \
    CDeviceManager.h \
    CEvent.h \
    CDelayAction.h \
    CAction.h \
    CTransceiver.h \
    CDevBistableSwitch.h \
    CDevBistableSensor.h \
    CEventManager.h \
    COperationListLoader.h \
    COperation.h \
    CFirmwareLoader.h \
    CFirmwareBuffer.h \
    CDevPwmDriver.h \
    CDevBroadcast.h \
    CDevRgbDriver.h \
    CDeviceWatcher.h \
    CDevMotionDetector.h \
    CGsmModem.h \
    CModemBuffer.h \
    CSerialPortManager.h \
    CAlarmModule.h \
    CErrorException.h \
    pistache/async.h \
    pistache/serializer/rapidjson.h \
    pistache/client.h \
    pistache/common.h \
    pistache/cookie.h \
    pistache/description.h \
    pistache/endpoint.h \
    pistache/flags.h \
    pistache/http_defs.h \
    pistache/http_header.h \
    pistache/http_headers.h \
    pistache/http.h \
    pistache/iterator_adapter.h \
    pistache/listener.h \
    pistache/mailbox.h \
    pistache/mime.h \
    pistache/net.h \
    pistache/optional.h \
    pistache/os.h \
    pistache/peer.h \
    pistache/prototype.h \
    pistache/reactor.h \
    pistache/router.h \
    pistache/stream.h \
    pistache/tcp.h \
    pistache/timer_pool.h \
    pistache/transport.h \
    pistache/typeid.h \
    pistache/view.h \
    libbool/LibBoolEE.h \
    CRestApiService.h \
    HRestApiServiceConsts.h \
    CRestHandler.h \
    COperationContainer.h \
    CDevCondition.h \
    CTimeCondition.h \
    CAlarmCondition.h \
    CCondition.h \
    CGsmModemAction.h \
    CDeviceAction.h \
    CTools.h \
    CInitDevicesConfig.h \
    CDeviceConfig.h \
    CLoggerPoster.h \
    CRestAction.h \
    CRestClientService.h



INCLUDEPATH += /usr/include/x86_64-linux-gnu/qt5/QtCore
INCLUDEPATH += /usr/include/x86_64-linux-gnu/qt5/
INCLUDEPATH += /usr/include/x86_64-linux-gnu/

OTHER_FILES +=
