#include "CDeviceWatcher.h"

CDeviceWatcher::CDeviceWatcher()
{

}

CDeviceWatcher::~CDeviceWatcher()
{

}

void CDeviceWatcher::updateLastSeenTime(const unsigned int &uid){
    lastSeenTimeMap[uid] = QDateTime::currentMSecsSinceEpoch();
}

qint64 CDeviceWatcher::getLastSeenTime(const unsigned int &uid) const{
    if (lastSeenTimeMap.contains(uid)){
        return lastSeenTimeMap[uid];
    }else{
        return 0;
    }
}
