#include "CConfig.h"

CConfig::CConfig(QObject *parent) : QObject(parent)
{
    logger = &CSingleton<CLogger>::Instance();
    loadConfig();

}

CConfig::~CConfig()
{
    qDebug("Config destructor");
}

QString CConfig::loadFromFile(){
    QFile file("settings/configuration.json");
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QString val = file.readAll();
    file.close();
    return val;
}

void CConfig::loadConfig(){
    CLogger *logger = &CSingleton<CLogger>::Instance();

    try{
        QJsonDocument json = QJsonDocument::fromJson(loadFromFile().toUtf8());
        QJsonObject mainObject = json.object();
        QJsonArray configs = mainObject["Configuration"].toArray();

        foreach (const QJsonValue & value, configs) {
            QJsonObject obj = value.toObject();
            configuration[obj["parameter"].toString()] = obj["value"].toString();
        }

        logger->debug(SYSTEM, "Configuration:");
        for (QString key : configuration.keys()){
            logger->debug(key + ": " + configuration[key]);
        }

        logger->success(SYSTEM, "Configuration loading success");


    }catch(CException ex){
        logger->error(SYSTEM, "Error during loading configuration file: " + ex.what());

    }catch(std::exception ex){
        logger->error(SYSTEM, "Error during loading configuration file: " + QString(ex.what()));

    }




}

QString CConfig::getConfigParam(QString name){
    if (configuration.contains(name)){
        return configuration[name];
    }
    logger->error(SYSTEM, "No configuration parameter '" + name + "'");
    return "";
}

bool CConfig::isConfigComplete(){
    QList<QString> parameters;
    parameters.push_back(FIRMWARE_PATH);
    parameters.push_back(OPERATIONS_PATH);
    parameters.push_back(TRANS_RESTART_TIME);
    parameters.push_back(DEVICE_ACTIVE_TIME);
    parameters.push_back(PING_INTERVAL);
    parameters.push_back(MAX_LAST_SEEN_TIME);
    parameters.push_back(EXPECTED_PARAM_INTERVAL);
    parameters.push_back(WEBSERVICE_PORT);
    parameters.push_back(GSM_MODULE);
    parameters.push_back(ALARM_HASH);
    parameters.push_back(ALARM_CODE);
    parameters.push_back(ALARM_DELAY_TIME);

    for (QString param : parameters){
        if (!configuration.contains(param)){
            logger->error(SYSTEM, "No configuration parameter '" + param + "'");
            return false;
        }
    }
    return true;
}
