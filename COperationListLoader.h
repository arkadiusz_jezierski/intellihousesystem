#ifndef COPERATIONLISTLOADER_H
#define COPERATIONLISTLOADER_H

#include <QObject>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <COperation.h>
#include <CAction.h>
#include <CGsmModemAction.h>
#include <CDeviceAction.h>
#include <CRestAction.h>
#include <CException.h>
#include <CLogger.h>
#include <CSingleton.h>
#include <QFile>
#include <QList>
#include <CCanDevice.h>
#include <CDeviceManager.h>
#include <CDevCondition.h>
#include <CTimeCondition.h>
#include <CAlarmCondition.h>
#include <COperationContainer.h>

class COperationListLoader : public QObject
{
    Q_OBJECT

    static bool validateOperation(COperation *operation, QMap<QString, CCondition*> conditions);
    static bool validateOperationWithExtendedConditionsLogic(COperation *operation, QMap<QString, CCondition*> conditions);

public:
    explicit COperationListLoader(QObject *parent = 0);
    ~COperationListLoader();

    static QString loadFromFile();
    static bool saveJsonToFile(QString json);
    static void loadOperations();

};

#endif // COPERATIONLISTLOADER_H
