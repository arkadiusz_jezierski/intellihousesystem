#ifndef HTYPES
#define HTYPES

#include <HCommConstans.h>
#include <QVector>
#include <QMetaEnum>

enum class EDeviceType{
    Sensor = 0,
    Actuator = 1,
};

enum class EOperationType{
    Normal,
    Delayed
};

enum class EReceivedEventType{
    Normal,
    StartDelayed,
    StopDelayed,
    Unknown
};

enum class EEventType{
    DEVICE_EVENT,
    TIME_EVENT,
    ALARM_EVENT,
    OTHER
};

enum class EActionType{
    DEVICE_ACTION,
    GSM_MODULE_ACTION,
    REST_ACTION,
    OTHER
};

enum class EParamChanges{
    None,
    Rising,
    Falling
};

enum class EConditionResult{
    True_Val,
    False_Val,
    DeviceParamUndefined,
    DeviceInactive,
    NotReady
};



class EDevice : public QObject{

    Q_OBJECT
    Q_ENUMS(Category)
public:
    enum  Category{
        Unknown = -1,
        BroadCast = 0,
        BistableSwitch = CATEGORY_ACTUATOR_BISTABLE_SWITCH,
        RGB = CATEGORY_ACTUATOR_RGB,
        PWM = CATEGORY_ACTUATOR_PWM,

        BistableSwitchSensor = CATEGORY_SENSOR_BISTABLE_SWITCH,
        MotionDetector = CATEGORY_SENSOR_MOTION_DETECTOR,
    };

    QList<QString> static getCategoryList(){
        QMetaObject mo = EDevice::staticMetaObject;
        int index = mo.indexOfEnumerator("Category");
        QMetaEnum metaEnum = mo.enumerator(index);

        QList<QString> lst;
        for (int i = 0; i < metaEnum.keyCount(); i++){
            if (QString(metaEnum.key(i)) == "Unknown"){// || QString(metaEnum.key(i)) == "BroadCast"){
                continue;
            }
            lst.push_back(metaEnum.key(i));
        }
        return lst;
    }

    int static getCategoryId(QString name){
        QMetaObject mo = EDevice::staticMetaObject;
        int index = mo.indexOfEnumerator("Category"); // watch out during refactorings
        QMetaEnum metaEnum = mo.enumerator(index);

        for (int i = 0; i < metaEnum.keyCount(); i++){
            if (QString(metaEnum.key(i)) == name){
                int value = metaEnum.keyToValue(name.toStdString().c_str());
                return value;
            }
        }
        return -1;
    }

    QList<QString> static getListAllCategories(){
        QMetaObject mo = EDevice::staticMetaObject;
        int index = mo.indexOfEnumerator("Category"); // watch out during refactorings
        QMetaEnum metaEnum = mo.enumerator(index);

        QList<QString> lst;
        for (int i = 0; i < metaEnum.keyCount(); i++){
            lst.push_back(metaEnum.key(i));
        }
        return lst;
    }

    Category static getCategory(QString categoryName){
        QMetaObject mo = EDevice::staticMetaObject;
        int index = mo.indexOfEnumerator("Category"); // watch out during refactorings
        QMetaEnum metaEnum = mo.enumerator(index);

        int value = metaEnum.keyToValue(categoryName.toStdString().c_str());
        EDevice::Category key = static_cast<EDevice::Category>(value);

        return key;
    }
};

enum class EAddressingStatus{
    ADDRESSING_ERROR = 0,
    ADDRESSING_OK = 1,
    ADDRESSING_INVALID_FRAME_ID = 2,
    ADDRESSING_COMPLETED = 3,
};

enum class ELogicalConditionType{
    UNDEFINED = -1,
    EQUAL = 0,
    GREATER_THAN = 1,
    GREATER_OR_EQUAL,
    LESS_THAN,
    LESS_OR_EQUAL,
    ANY,
    RISING,
    FALLING,
    NOT_EQUAL
};

enum class EConditionType{
    DEVICE = 0,
    TIME,
    ALARM
};

enum class EStartupInfoType{
    UNDEFINED = 0,
    RESET_BY_WDT,
    RESET_BY_RESET_INSTRUCTION,
    STARTED_ON_POWER_UP
};

struct SExpectedParametersValues{
    unsigned char address;
    QString parameter;
    qint64 value;
};

struct SDeviceStartupInfo{
    EStartupInfoType startupInfo;
    unsigned char RCONRegister;
};

typedef unsigned int UID;
typedef unsigned char COMMAND;

typedef QVector<unsigned char> RawData;
typedef EDevice::Category DevCat;

typedef int MessageValidationResult;


#endif // HTYPES

