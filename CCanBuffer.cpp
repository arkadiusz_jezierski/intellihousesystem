#include "CCanBuffer.h"

CCanBuffer::CCanBuffer()
{
    canID = 0;
    command = 0;
}

CCanBuffer::~CCanBuffer()
{

}

void CCanBuffer::setCanID(const unsigned int id){
    if (id > 0x1fffffff){
        logger->warning("Invalid CAN ID: " + QString::number(id, 16));
        canID = 0x1fffffff;
        logger->warning("CAN ID changed to: " + QString::number(canID, 16));
    }else{
        canID = id;
    }
}

void CCanBuffer::setTransmitGlobalId(const COMMAND globalCommand, const UID uid) {

    canID = ((unsigned int)globalCommand) << OFFSET_CAN_GLOBAL_COMMAND;
    canID |= ID_GLOBAL_BROADCAST;
    canID |= uid;

}

void CCanBuffer::setTransmitDeviceId(DevCat category, unsigned char address) {

    canID = ID_ADDRESS_MASTER_SOURCE;
    canID |= (static_cast<unsigned int>(category) & 0x0f) << 11;
    canID |= (static_cast<unsigned int>(address) & 0x7f) << 15;

}

unsigned int CCanBuffer::getCanID() const{
    return canID;
}

void CCanBuffer::printCanBuffer(QString msg) const{

    if (this->size() > 0){
        logger->low_info(msg + (msg.length() > 0 ? " " : "") + "CAN BUFFER ID: 0x" + QString::number(getCanID(), 16) + " DATA: [ "+this->getPrintBuffer(true)+"]");
    }else{
        logger->low_info(msg + (msg.length() > 0 ? " " : "") + "CAN BUFFER ID: 0x" + QString::number(getCanID(), 16) + " NO DATA");
    }
}

QString CCanBuffer::shortDescriptionForDestinationID(const unsigned int &canID){
    QString ret = "<DST: ";
    ret += QString::number(getDstCat(canID));
    ret += ":";
    ret += QString::number(getDstAddress(canID));
    ret += ">";
    return ret;
}

QString CCanBuffer::shortDescriptionForSourceID(const unsigned int &canID){
    QString ret = "<SRC: ";
    ret += QString::number(getSrcCat(canID));
    ret += ":";
    ret += QString::number(getSrcAddress(canID));
    ret += ">";
    return ret;
}

unsigned int CCanBuffer::getDstId(const unsigned int& canId){
    return (canId & ID_ADDRESS_MASTER_DESTINATION) >> 11;
}
unsigned int CCanBuffer::getDstAddress(const unsigned int& canId){
    return (getDstId(canId) >> 4) & 0x7f;
}
unsigned int CCanBuffer::getDstCat(const unsigned int& canId){
    return getDstId(canId) & 0x0f;
}
unsigned int CCanBuffer::getSrcId(const unsigned int& canId){
    return canId & ID_ADDRESS_MASTER_SOURCE;
}
unsigned int CCanBuffer::getSrcAddress(const unsigned int& canId){
    return (getSrcId(canId) >> 4) & 0x7f;
}
unsigned int CCanBuffer::getSrcCat(const unsigned int& canId){
    return getSrcId(canId) & 0x0f;
}


unsigned int CCanBuffer::getSourceID() const{
    return getCanID() & ID_ADDRESS_MASTER_SOURCE;
}

unsigned char CCanBuffer::getSourceAddress() const{
    return static_cast<unsigned char>((getSourceID() >> 4) & 0x7f);
}

unsigned char CCanBuffer::getSourceCategoryID() const{
    return static_cast<unsigned char>(getSourceID() & 0x0f);
}

unsigned int CCanBuffer::getDestinationID() const{
    return (getCanID() & ID_ADDRESS_MASTER_DESTINATION) >> 11;
}

unsigned char CCanBuffer::getDestinationAddress() const{
    return static_cast<unsigned char>((getDestinationID() >> 4) & 0x7f);
}

unsigned char CCanBuffer::getDestinationCategoryID() const{
    return static_cast<unsigned char>(getDestinationID() & 0x0f);
}

DevCat CCanBuffer::getSourceCategory() const{
    return static_cast<DevCat>(getSourceID() & 0x0f);
}

unsigned char CCanBuffer::getSourceCategoryTypeId() const{
    return static_cast<unsigned char>(getSourceID() & 0x01);
}

EDeviceType CCanBuffer::getSourceCategoryType() const{
    return static_cast<EDeviceType>(getSourceID() & 0x01);
}

unsigned int CCanBuffer::getSourceUID() const{
    return getCanID() & MASK_FOR_UID_IN_ID;
}

CCanBuffer::EFrameType CCanBuffer::getFrameType() const{

    if (getCanID() & ID_REQ_ADDRESS){
        return EFrameType::ADDRESS_REQUEST;
    }else if (getCanID() & ID_RES_ADDRESS){
        return EFrameType::ADDRESS_RESPONSE;
    }else if (getCanID() & ID_REQ_ERROR_INFO){
        return EFrameType::ERROR_INFO;
    }else if (getCanID() & ID_GLOBAL_BROADCAST){
        return EFrameType::GLOBAL_COMMAND;
    }else if (getCanID() & ID_REQ_STARTING_UP_INFO){
        return EFrameType::DEV_STARTING_UP;
    }else if (getCanID() != 0){
        return EFrameType::REGULAR;
    }else{
        return EFrameType::EMPTY;
    }
}

COMMAND  CCanBuffer::getCommand() const{
    if (this->size() > OFFSET_CAN_COMMAND){
        return this->operator [](OFFSET_CAN_COMMAND);
    }else{
        return 0;
    }
}

//CBuffer CCanBuffer::getData() const{
//    CCanBuffer data;
//    for (size_t i = OFFSET_CAN_DATA; i < this->size(); i++){
//        data << static_cast<unsigned char>(this->operator [](i));
//    }
//    return data;
//}

unsigned char CCanBuffer::getAddressingModeFrameId() const{
    if (this->size() == 0){
        throw CException(QString("Empty addressing request buffer"));
    }
    return this->operator [](OFFSET_CAN_REQ_ADR_FRAME_ID);

}

void CCanBuffer::clear(){
    command = 0;
    canID = 0;
    buffer.clear();
}


int CCanBuffer::getResetCounter() const{
    int counter = this->operator [](OFFSET_CAN_RESET_COUNTERS);
    counter |= this->operator [](OFFSET_CAN_RESET_COUNTERS + 1) << 8;
    return counter;
}

int CCanBuffer::getWdtResetCounter() const{
    int counter = this->operator [](OFFSET_CAN_RESET_COUNTERS + 2);
    counter |= this->operator [](OFFSET_CAN_RESET_COUNTERS + 3) << 8;
    return counter;
}

unsigned char CCanBuffer::getCANRxErrorCounter() const {
    return this->operator [](OFFSET_CAN_RX_ERR_COUNTER);
}


unsigned char CCanBuffer::getCANTxErrorCounter() const {
    return this->operator [](OFFSET_CAN_TX_ERR_COUNTER);
}

unsigned char CCanBuffer::getStartupInfo() const {
    return this->operator [](OFFSET_STARTUP_INFO);
}

unsigned char CCanBuffer::getRCONRegisterValue() const {
    return this->operator [](OFFSET_RCON_REGISTER);
}


void CCanBuffer::insertFlashAddress(unsigned int address) {
    this->flashAddress = address & 0xffffff;
}

void CCanBuffer::insertBootCommand(unsigned char command) {
    this->bootCommand = command & 0x0f;
}

void CCanBuffer::insertBootControlBits(unsigned char bits) {
    this->bootControlBits = bits & 0x1f;
}

unsigned int CCanBuffer::convertToFlashAddress(unsigned int address){
    unsigned int retAddress = 0;
    retAddress |= ((address & 0xff) << 24);
    retAddress |= (((address >> 8) & 0xff) << 16);
    retAddress |= (((address >> 16) & 0xff) << 8);
    return retAddress;
}

unsigned int CCanBuffer::getBootCRC(){
    //    this->printBuffer();
    unsigned int crc = this->operator[](0);
    crc |= (this->operator[](1) << 8);
            return crc;
}

void CCanBuffer::addPwmValuesToBuffer(const unsigned long long &values){
    buffer.push_back((values >> 0) & 0xff);
    buffer.push_back((values >> 8) & 0xff);
    buffer.push_back((values >> 16) & 0xff);
    buffer.push_back((values >> 24) & 0xff);
    buffer.push_back((values >> 32) & 0xff);
    buffer.push_back((values >> 40) & 0xff);
    buffer.push_back((values >> 48) & 0xff);

}

void CCanBuffer::addRgbValuesToBuffer(const unsigned long long &values){
    buffer.push_back((values >> 32) & 0xff);
    buffer.push_back((values >> 24) & 0xff);
    buffer.push_back((values >> 16) & 0xff);
    buffer.push_back((values >> 8) & 0xff);
    buffer.push_back((values >> 0) & 0xff);

}
