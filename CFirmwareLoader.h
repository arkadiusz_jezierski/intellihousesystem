#ifndef CFIRMWARELOADER_H
#define CFIRMWARELOADER_H

#include <iostream>
#include <fstream>
#include <QVector>
#include "CLogger.h"
#include "CSingleton.h"
#include "CFirmwareBuffer.h"
#include "HTypes.h"
#include "CConfig.h"
#include <QDir>

#define OFFSET_FIRMWARE_LENGTH   0
#define OFFSET_FIRMWARE_ADDR   1
#define OFFSET_FIRMWARE_DATA     4
#define OFFSET_FIRMWARE_TYPE     3

#define TYPE_DATA       0
#define TYPE_EOF        1
#define TYPE_EXADR      4



class CFirmwareLoader : public QObject
{
public:
    CFirmwareLoader();
    ~CFirmwareLoader();

    void printFileList();
    static QStringList getFileList();
    CFirmwareBuffer readFile(QString filename);

private:
    CLogger *logger;
    CConfig *config;

    bool checkCRC(RawData &input);
    bool checkInputData(RawData &input);
    unsigned int getAddress(RawData &input);
    CBuffer getData(RawData &input);
    RawData parseData (QString &input);
    unsigned int getHighExtendedAddress(RawData &input);
};

#endif // CFIRMWARELOADER_H
