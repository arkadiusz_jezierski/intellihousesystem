#ifndef CRESTCLIENTSERVICE_H
#define CRESTCLIENTSERVICE_H

#include <QObject>
#include <QDebug>
#include <QDateTime>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QUrl>
#include <CLogger.h>
#include <CSingleton.h>
#include <QThread>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>

class CRestClientService : public QObject
{

	Q_OBJECT

    QNetworkAccessManager *manager = NULL;

    CLogger *logger = NULL;

    bool runThread = true;

public:
    CRestClientService(QObject *parent = 0);
    ~CRestClientService();

    void runRestClientService();

public slots:
	void sendPostRequest(QString endpoint, QString data);
    void onPostAnswer(QNetworkReply* reply);
    void killThread();
};

#endif // CRESTCLIENTSERVICE_H