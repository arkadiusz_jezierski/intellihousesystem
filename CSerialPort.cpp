#include "CSerialPort.h"

CSerialPort::CSerialPort(QObject *parent) : QObject(parent)
{
    m_PortFd = -1;
    portName = "";
}

CSerialPort::~CSerialPort()
{
    closePort();
}

bool CSerialPort::isOpen() {
    return m_PortFd != -1 ? true : false;
}



int CSerialPort::openTermio() {
    m_PortFd = open(m_PortName, O_RDWR | O_NOCTTY | O_NONBLOCK); //set the user console port up
    if (m_PortFd >= 0) {
        tcgetattr(m_PortFd, &m_OldTio); // save current port settings   //so commands are interpreted right for this program
        memset(&m_ActTio, 0, sizeof (struct termios));
        m_ActTio.c_cflag = m_Baud | m_DBits | m_SBits | m_ParityOn | m_ParityMode | CLOCAL | CREAD;
        m_ActTio.c_iflag = IGNPAR;
        m_ActTio.c_oflag = 0;
        m_ActTio.c_lflag = 0; //ICANON;
        m_ActTio.c_cc[VMIN] = 1;
        m_ActTio.c_cc[VTIME] = 0;
        tcflush(m_PortFd, TCIOFLUSH);
        tcsetattr(m_PortFd, TCSANOW, &m_ActTio);

    } else {
        throw CException(QString("Cannot open serial port"), this);
    }
    return 0;

}


int CSerialPort::openSerial(QString PortName, long Baud, int DBits, int SBits, int Parity) {
    int error = 0;
    if (isOpen())
        closePort();
    memset(m_PortName, 0, sizeof (m_PortName));

    strncpy(m_PortName, PortName.toStdString().c_str(), PORT_NAME_LEN);
    // Convert Parameter to termio
    switch (Baud) {
    case 230400:
        m_Baud = B230400;
        break;
    case 115200:
        m_Baud = B115200;
        break;
    case 57600:
        m_Baud = B57600;
        break;
    case 38400:
        m_Baud = B38400;
        break;
    case 19200:
        m_Baud = B19200;
        break;
    case 9600:
        m_Baud = B9600;
        break;
    case 4800:
        m_Baud = B4800;
        break;
    case 2400:
        m_Baud = B2400;
        break;
    case 1800:
        m_Baud = B1800;
        break;
    case 1200:
        m_Baud = B1200;
        break;
    case 600:
        m_Baud = B600;
        break;
    case 300:
        m_Baud = B300;
        break;
    case 200:
        m_Baud = B200;
        break;
    case 150:
        m_Baud = B150;
        break;
    case 134:
        m_Baud = B134;
        break;
    case 110:
        m_Baud = B110;
        break;
    case 75:
        m_Baud = B75;
        break;
    case 50:
        m_Baud = B50;
        break;
    default:
        m_Baud = B9600;
        break;
    }

    switch (DBits) {
    case 8:
    default:
        m_DBits = CS8;
        break;
    case 7:
        m_DBits = CS7;
        break;
    case 6:
        m_DBits = CS6;
        break;
    case 5:
        m_DBits = CS5;
        break;
    }

    switch (SBits) {
    case 1:
        m_SBits = 0;
        break;
    default:
    case 2:
        m_SBits = CSTOPB;
        break;
    }

    switch (Parity) {
    case 0:
    default: //none
        m_ParityOn = 0;
        m_ParityMode = 0;
        break;
    case 1: //odd
        m_ParityOn = PARENB;
        m_ParityMode = PARODD;
        break;
    case 2: //even
        m_ParityOn = PARENB;
        m_ParityMode = 0;
        break;
    } //end of switch parity
    error = openTermio();
    return error;
}



void CSerialPort::closePort() {
    if (m_PortFd != -1) {
        tcsetattr(m_PortFd, TCSANOW, &m_OldTio);
        //close(m_PortFd);
        m_PortFd = -1;
    }
    CSerialPortManager *portManager = &CSingleton<CSerialPortManager>::Instance();
    portManager->releasePort(portName);
    portName = "";
}


unsigned char CSerialPort::receiveByte(int timeOut) {
    int rec = 0;
    int ret = 0;
    CTimeOut tout;

    if (isOpen()) {
        tout.startTimeOut(timeOut);
        ret = read(m_PortFd, &rec, 1);
        while (ret != 1 && !tout.isTimeOut()) {
            ret = read(m_PortFd, &rec, 1);
            usleep(1000);
        }
        if (ret != 1) {
            throw CException(QString("Error during receive byte "), this);
        } else
            rec &= 0xff;
    } else {
        throw CException(QString("Port closed."), this);
    }

    return rec;
}





int CSerialPort::sendBuffer(int bufferSize) {
    int err = NO_GLOBAL_ERROR;
    int written = 0;
    int rep = 10000;
    int rest = bufferSize;

    if (!isOpen()) {
        throw CException(QString("Port closed."), this);
    }
    if (bufferSize <= 0) {
        throw CException(QString("Error with sending buffer"), this);
    }

    do {
        written = write(m_PortFd, buf, rest);
        if (written >= 0){
            rest = bufferSize - written;
        }

    } while (!err && rest > 0 && rep--);

    tcflush(m_PortFd, TCOFLUSH);

    if (rest > 0) {
        throw CException(QString("Error during sending buffer, data sending incomplete"), this);
    }
    return err;
}


void CSerialPort::sendBuffer(const CBuffer &buffer){

    for (size_t i = 0; i < buffer.size(); i++){
        buf[i] = buffer[i];
    }

    int result = sendBuffer(buffer.size());

    if (result){
        throw CException (QString("Sending buffer with result code: ") + QString::number(result), this);
    }
}


int CSerialPort::flushReceiveBuffer(void) {
    if (!isOpen()) {
        throw CException(QString("Port closed."), this);
    }

    tcflush(m_PortFd, TCIFLUSH);

    return 0;
}
