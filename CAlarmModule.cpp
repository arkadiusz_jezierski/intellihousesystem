#include "CAlarmModule.h"

CAlarmModule::CAlarmModule(QObject *parent) : QObject(parent)
{
    CConfig *config = &CSingleton<CConfig>::Instance();
    salt = config->getConfigParam(ALARM_HASH);
    code = config->getConfigParam(ALARM_CODE);
    alarmDelayTime = config->getConfigParam(ALARM_DELAY_TIME).toInt();

    logger = &CSingleton<CLogger>::Instance();
}

CAlarmModule::~CAlarmModule()
{

}

void CAlarmModule::armSystem() {
    QMutexLocker locker(&alarmFlagMutex);
    logger->success(ALARM_MODULE, "System will be ARMED in " + QString::number(alarmDelayTime) + " seconds");
    alarmStartTime = QDateTime::currentMSecsSinceEpoch() + 1000 * alarmDelayTime;
}

bool CAlarmModule::disarmSystem(QString code) {
    QMutexLocker locker(&alarmFlagMutex);
    if (checkUnlockCode(code)) {
        logger->success(ALARM_MODULE, "System DISARMED");
        alarmStartTime = 0;
        return true;
    } else {
        logger->error(ALARM_MODULE, "Invalid password, system remains armed");
        return false;
    }
}

bool CAlarmModule::systemIsArmed() {
    QMutexLocker locker(&alarmFlagMutex);
    bool systemArmed = ((alarmStartTime > 0) && alarmStartTime < QDateTime::currentMSecsSinceEpoch());
    if (systemArmed) {
        logger->debug(ALARM_MODULE, "System is ARMED");
    }
    return systemArmed;
}

QString CAlarmModule::getMd5(QString input) {

    QByteArray byteArray = QByteArray(input.toStdString().c_str());
    return QCryptographicHash::hash(byteArray, QCryptographicHash::Md5).toHex();

}

bool CAlarmModule::checkUnlockCode(QString unlockCode) {
    unsigned long millis = QDateTime::currentMSecsSinceEpoch() / 1000 - 300;

    for (int i = 0; i < 600; i++) {
        QString generatedCode = this->salt + QString::number(millis) + this->code;
        generatedCode = getMd5(generatedCode);

        if (generatedCode.compare(unlockCode, Qt::CaseInsensitive) == 0) {
            return true;
        }

        millis++;
    }

    return false;
}

