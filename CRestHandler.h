#ifndef CRESTHANDLER_H
#define CRESTHANDLER_H

#include <CSingleton.h>
#include <CRestApiService.h>
#include "pistache/endpoint.h"
using namespace Net;

class CRestHandler : public Http::Handler
{
public:
    CRestHandler();
public:

    HTTP_PROTOTYPE(CRestHandler)

    void onRequest(const Http::Request& request, Http::ResponseWriter response);
};

#endif // CRESTHANDLER_H
