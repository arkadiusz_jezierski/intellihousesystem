#ifndef CEXCEPTION_H
#define CEXCEPTION_H

#include <QObject>
#include <QException>

class CException : public QException
{

//    QString message = "";
protected:
    QString objName = "";
    QString strMessage = "";

public:
    QString what();
    void raise() const { throw *this; }
    CException *clone() const { return new CException(*this); }
//    CException(const char * msg);
    CException(const QString msg);
//    CException(const char * msg, QObject* obj);
    CException(const QString msg, const QObject* obj);
    ~CException() throw() {}

};

#endif // CEXCEPTION_H
