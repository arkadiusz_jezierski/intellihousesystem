#ifndef CTOOLS_H
#define CTOOLS_H
#include <QtGlobal>
#include "CCanDevice.h"
#include "HTypes.h"

#define KEY_DEVICE		"DEV"
#define KEY_TIME		"TIME"
#define KEY_ALARM		"ALARM"



class CTools
{
public:
    CTools();

    static QString generateEventKey(EConditionType condType, CCanDevice* device, QString param, qint64 val);
    static QString generateEventKey(EEventType eventType, CCanDevice* dev, QString param, qint64 value);
    static QString secondsSinceMidnightToHourFormat(qint64 secs);
};

#endif // CTOOLS_H
