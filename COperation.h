#ifndef COPERATION_H
#define COPERATION_H

#include <CAction.h>
#include <QVector>
#include <QTime>
#include <HTypes.h>
#include <QDateTime>
#include <QtGlobal>

class COperation
{

public:
    explicit COperation();
    virtual ~COperation();


    EOperationType type;
    QString name;

    QSet<QString> deviceConditions;
    QSet<QString> stopConditions;
    QSet<QString> timeConditions;
    QSet<QString> timeStopConditions;
    QSet<QString> alarmConditions;
    QSet<QString> alarmStopConditions;
    QString conditionsExtLogic;
    QString stopConditionsExtLogic;

    QDateTime executionTime;
    int delayForActions;
    QList<CAction*> actions;

    QString operationId;

    QSet<QString> getRegularConditions();
    QSet<QString> getDelayedStopConditions();
    QSet<QString> getDelayedStartConditions();


};

#endif // COPERATION_H
