#include "CRestClientService.h"


CRestClientService::CRestClientService(QObject *parent) : QObject(parent)
{
	logger= &CSingleton<CLogger>::Instance();

	manager = new QNetworkAccessManager(this);

	QObject::connect(manager, SIGNAL(finished(QNetworkReply*)),
        this, SLOT(onPostAnswer(QNetworkReply*)));

}

CRestClientService::~CRestClientService()
{
	if (manager != NULL) {
		delete manager;
		manager = NULL;
	}
}

void CRestClientService::sendPostRequest(QString endpoint, QString data) {

    QNetworkRequest request;
	request.setHeader(QNetworkRequest::ContentTypeHeader,"text/plain");
    request.setUrl(QUrl(endpoint));

	QByteArray postData = data.toUtf8();
    logger->info(REST_CLIENT, "Sending POST request, msg: '" + QString(data) + "' to '" + QString(endpoint)+ "'");
    manager->post(request, postData);
}

void  CRestClientService::onPostAnswer(QNetworkReply* reply)
{

    if (reply->error() != QNetworkReply::NoError) {
        logger->error(REST_CLIENT, "Error during sending POST request: " + reply->errorString());
    } else {
    	//QByteArray response = reply->readAll();
     
        logger->success(REST_CLIENT, "POST request successfully sent");
    	
    } 

    delete reply;
 
}

void CRestClientService::killThread() {
    runThread = false;
}

void CRestClientService::runRestClientService() {
    logger->info(EVENT_MANAGER, "Starting REST Client Service");
    runThread = true;
    while(runThread) {
       
        QThread::msleep(1);
    }
    logger->debug(EVENT_MANAGER, "REST Client Service thread finished");
}