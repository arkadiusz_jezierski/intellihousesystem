#include "CEvent.h"

// CEvent::CEvent()
// {
    // this->device = NULL;
    // this->parameter = "";
    // this->value = 0;
    // this->eventType = EEventType::OTHER;
    // this->eventId = "EVT" + QString::number(QDateTime::currentMSecsSinceEpoch()) + QString::number(qrand());
// }

CEvent::CEvent(CCanDevice *device, QString param, qint64 val){
    this->device = device;
    this->parameter = param;
    this->value = val;
    this->eventType = EEventType::DEVICE_EVENT;
}

CEvent::CEvent(qint64 val, EEventType type) {
	this->value = val;
    this->eventType = type;
}

CEvent::CEvent(EEventType type) {
    this->eventType = type;
}

//CEvent::CEvent(CCanDevice *device, QString param, ELogicalConditionType condition, qint64 val){
//    this->device = device;
//    this->parameter = param;
//    this->value = val;
//    this->condition = condition;
//}

//CEvent::CEvent(const CEvent &event){
//    this->device = event.device;
//    this->parameter = event.parameter;
//    this->value = event.value;
//}

CEvent::~CEvent()
{

}


EEventType CEvent::getEventType() {

    return this->eventType;
}

QString CEvent::toString(){
    QString output = "";

    if (eventType == EEventType::DEVICE_EVENT) {
        output = "Device event: " + device->objectName() + " has parameter " + parameter + " value " + QString::number(value);
    } else if (eventType == EEventType::TIME_EVENT) {
        // output = "Time event, value: " + CTools::secondsSinceMidnightToHourFormat(value);
        output = "Time event, value: " +QString::number(value);

    } else if (eventType == EEventType::TIME_EVENT) {
        output = "Alarm event";
    }

    return output;

}

