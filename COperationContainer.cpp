#include "COperationContainer.h"

COperationContainer::COperationContainer(QObject *parent) : QObject(parent)
{
	logger = &CSingleton<CLogger>::Instance();
}


COperationContainer::~COperationContainer()
{
}

void COperationContainer::addConditionWithOperation(CCondition* condition, COperation* operation) {
	this->operationByIdMap[operation->operationId] = operation;
	this->conditionByIdMap[condition->conditionId] = condition;
	this->cond2operationMap[condition->conditionId] = operation->operationId;
	
	QString eventKey = CTools::generateEventKey(condition->type, condition->device, condition->param, condition->value);
	this->conditionsByEventKeyMap[eventKey].insert(condition->conditionId);
	if (condition->type == EConditionType::TIME) {
		if (!this->timeConditionValues.contains(condition->value)) {
			this->timeConditionValues.push_back(condition->value);
		}
	}
}

void COperationContainer::addConditionsWithOperation(QList<CCondition*> conditions, COperation* operation) {
	for (CCondition* cond : conditions) {
		addConditionWithOperation(cond, operation);
	}
}

void COperationContainer::clearContainer() {
	for (QString key : conditionByIdMap.keys()) {
		delete conditionByIdMap[key];
		conditionByIdMap[key] = NULL;
	}
	conditionByIdMap.clear();
	conditionsByEventKeyMap.clear();

	for (QString key : operationByIdMap.keys()) {
		for (CAction* action : operationByIdMap[key]->actions) {
			delete action;
			action = NULL;
		}
		delete operationByIdMap[key];
		operationByIdMap[key] = NULL;
	}
	operationByIdMap.clear();
	cond2operationMap.clear();
	timeConditionValues.clear();
}



void COperationContainer::printOperationList() {
	logger->debug(EVENT_MANAGER, ">>>> OPERATIONS LIST: <<<<");
	for (QString key : operationByIdMap.keys()) {
		this->printOperation(operationByIdMap[key]);
	}

	logger->debug(EVENT_MANAGER, ">>>> CONDITIONS KEYS: <<<<");
	for (QString key : conditionsByEventKeyMap.keys()) {
		logger->debug(EVENT_MANAGER, "  Condition event key: " + key + ":");
		for (QString condId : conditionsByEventKeyMap[key]) {
			logger->debug(EVENT_MANAGER, "    " + conditionByIdMap[condId]->toString());
		}
	}

}

void COperationContainer::printOperation(COperation* operation) {

	if (operation->type == EOperationType::Normal) {
		QString extCondLogic = "";
		if (operation->conditionsExtLogic == "") {
			extCondLogic = "no extended conditions logic";
		} else {
			extCondLogic = "extended conditions logic: " + operation->conditionsExtLogic;
		}
		logger->debug(EVENT_MANAGER, ">>> Operation " + operation->name + ", type: Regular, " + extCondLogic);
		if (operation->deviceConditions.size() > 0) {
			logger->debug(EVENT_MANAGER, "  Device conditions:");
			for (QString condId : operation->deviceConditions) {
				logger->debug(EVENT_MANAGER, "    " + this->conditionByIdMap[condId]->toString());
			}
		}

		if (operation->alarmConditions.size() > 0) {
			logger->debug(EVENT_MANAGER, "  Alarm conditions:");
			for (QString condId : operation->alarmConditions) {
				logger->debug(EVENT_MANAGER, "    " + this->conditionByIdMap[condId]->toString());
			}
		}

		if (operation->timeConditions.size() > 0) {
			logger->debug(EVENT_MANAGER, "  Time conditions:");
			for (QString condId : operation->timeConditions) {
				logger->debug(EVENT_MANAGER, "    " + this->conditionByIdMap[condId]->toString());
			}
		}

		logger->debug(EVENT_MANAGER, "  Actions:");
		for (CAction* event : operation->actions) {
			logger->debug(EVENT_MANAGER, "    " + event->toString());
		}
		
	} else if (operation->type == EOperationType::Delayed) {
		QString extCondLogic = "";
		if (operation->conditionsExtLogic == "") {
			extCondLogic = "no extended conditions logic";
		} else {
			extCondLogic = "extended start conditions logic: " + operation->conditionsExtLogic + ", stop conditions logic: " + operation->stopConditionsExtLogic;
		}
		logger->debug(EVENT_MANAGER, ">>> Operation " + operation->name + ", type: Delayed, delay time: " + QString::number(operation->delayForActions) + "sec, " + extCondLogic);
		if (operation->deviceConditions.size() > 0) {
			logger->debug(EVENT_MANAGER, "  Device start conditions:");
			for (QString condId : operation->deviceConditions) {
				logger->debug(EVENT_MANAGER, "    " + this->conditionByIdMap[condId]->toString());
			}
		}

		if (operation->stopConditions.size() > 0) {
			logger->debug(EVENT_MANAGER, "  Device stop conditions:");
			for (QString condId : operation->stopConditions) {
				logger->debug(EVENT_MANAGER, "    " + this->conditionByIdMap[condId]->toString());
			}
		}

		if (operation->alarmConditions.size() > 0) {
			logger->debug(EVENT_MANAGER, "  Alarm start conditions:");
			for (QString condId : operation->alarmConditions) {
				logger->debug(EVENT_MANAGER, "    " + this->conditionByIdMap[condId]->toString());
			}
		}

		if (operation->alarmStopConditions.size() > 0) {
			logger->debug(EVENT_MANAGER, "  Alarm stop conditions:");
			for (QString condId : operation->alarmStopConditions) {
				logger->debug(EVENT_MANAGER, "    " + this->conditionByIdMap[condId]->toString());
			}
		}

		if (operation->timeConditions.size() > 0) {
			logger->debug(EVENT_MANAGER, "  Time start conditions:");
			for (QString condId : operation->timeConditions) {
				logger->debug(EVENT_MANAGER, "    " + this->conditionByIdMap[condId]->toString());
			}
		}

		if (operation->timeStopConditions.size() > 0) {
			logger->debug(EVENT_MANAGER, "  Time stop conditions:");
			for (QString condId : operation->timeStopConditions) {
				logger->debug(EVENT_MANAGER, "    " + this->conditionByIdMap[condId]->toString());
			}
		}

		logger->debug(EVENT_MANAGER, "  Actions:");
		for (CAction* event : operation->actions) {
			logger->debug(EVENT_MANAGER, "    " + event->toString());
		}
	}

}