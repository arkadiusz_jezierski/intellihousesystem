#ifndef CDevCondition_H
#define CDevCondition_H

#include "CCondition.h"
#include "CCanDevice.h"
#include "HTypes.h"

class CDevCondition : public CCondition
{

	bool compareParameterValues(qint64 value1, qint64 value2, ELogicalConditionType condition);
	void calculateCondition();
	
public:
    CDevCondition();
    ~CDevCondition();
    CDevCondition(CCanDevice *device, QString param, ELogicalConditionType condition, qint64 val);

    // bool operator == (const CDevCondition &event);

    QString toString();
    

};

#endif // CDevCondition_H
