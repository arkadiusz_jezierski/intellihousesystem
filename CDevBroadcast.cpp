#include "CDevBroadcast.h"

CDevBroadcast::CDevBroadcast()
{
    this->setCategory(DevCat::BroadCast);

    initParameters();
    initExecuteMethods();
}

CDevBroadcast::~CDevBroadcast()
{

}

void CDevBroadcast::initParameters(){
    Value justForCommand;

    justForCommand.undefined = false;

    parameters["off"] = justForCommand;
    commands["off"] = justForCommand;
}

void CDevBroadcast::initExecuteMethods(){
    executeMethodsMap["off"] = &CDevBroadcast::executeCommandOff;
}


void CDevBroadcast::executeCommandOff(qint64 value){
    logger->info(DEVICE_MANAGER, "BroadCast->executeCommandOff(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_ALL_OFF);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
}

void CDevBroadcast::executeActionForBroadCast(QString param, qint64 value){

    if (executeMethodsMap.contains(param)){
        try{
            (this->*executeMethodsMap[param])(value);
        }catch(CException ex){
            logger->error(DEVICE_MANAGER, "Error in BroadCast: " + ex.what());
        }
    }else{
        logger->warning(DEVICE_MANAGER, "BroadCast: No execution to invoke for parameter " + param);
    }
}

