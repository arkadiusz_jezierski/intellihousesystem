#ifndef CDEVMOTIONDETECTOR
#define CDEVMOTIONDETECTOR

#include <QObject>
#include <CCanDevice.h>

#define PARAM_SENSITIVITY       "sensitivity"
#define PARAM_LED_POWER         "ledPower"
#define PARAM_MOTION_STATUS     "motionStatus"
#define PARAM_TEMPER_STATUS     "temperStatus"

class CDevMotionDetector : public CCanDevice
{
    Q_OBJECT

public:
    explicit CDevMotionDetector();
    ~CDevMotionDetector();

    void initParameters();
    void initCommandMethods();
    void initRequestMethods();
    void initExecuteMethods();

    typedef void (CDevMotionDetector::*method)(CBuffer &data);
    QMap<COMMAND, method> methodsMap;

    typedef void (CDevMotionDetector::*requestMethod)();
    QMap<QString, requestMethod> requestMethodsMap;

    typedef SCommandSet (CDevMotionDetector::*executeDeviceCommand)(qint64);
    QMap<QString, executeDeviceCommand> executeDeviceCommandMap;

    void getSensitivity(CBuffer &data);
    void setSensitivity(CBuffer &data);
    void getLedPower(CBuffer &data);
    void setLedPower(CBuffer &data);
    void getMotionStatus(CBuffer &data);
    void getTemperStatus(CBuffer &data);

    void requestLedPower();
    void requestSensitivity();
    void requestMotionStatus();
    void requestTemperStatus();

    SCommandSet executeSetLedPower(qint64 value);
    SCommandSet executeSetSensitivity(qint64 value);


public slots:

    void commandMethodOfMotionDetector(COMMAND cmd, CBuffer &data);
    void requestParameterForMotionDetector(QString param);
    SCommandSet executeCommandForMotionDetector(SCommandSet commSet);
};

#endif // CDEVMOTIONDETECTOR

