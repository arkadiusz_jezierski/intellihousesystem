
#include "CTimeOut.h"


CTimeOut::CTimeOut()
{
}

CTimeOut::~CTimeOut()
{

}


void CTimeOut::startTimeOut(int Tout)
{
    getTimer();
    m_TimeOut= m_msTimer + Tout;
    return ;
}


bool CTimeOut::isTimeOut(void)
{
    bool elapse;

    getTimer();

    elapse = (m_msTimer >= m_TimeOut );

    return elapse;
}

void CTimeOut::getTimer(void)
{
    gettimeofday(&time, NULL);
    m_msTimer= ((time.tv_sec) * 1000 + time.tv_usec / 1000.0) + 0.5;
}
