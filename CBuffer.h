#ifndef CBuffer_H
#define CBuffer_H

#include <QObject>
#include <QVector>
#include <CException.h>
#include <CLogger.h>
#include <CSingleton.h>

/**
 * @brief The CBuffer class
 * Base buffer class.
 */
class CBuffer
{

protected:
    QVector<unsigned char> buffer;


public:
    explicit CBuffer();

    virtual ~CBuffer();

    void operator <<(const unsigned char &data);
    void operator <<(const unsigned short &data);
    void operator <<(const unsigned int &data);
    void operator <<(const int &data);
    void operator <<(const CBuffer &data);
    void operator <<(const QVector<unsigned char> &data);
    void operator <<(const QString &data);
    unsigned char operator[] (const unsigned int &index) const;
    bool operator ==(const CBuffer &buffer);
    void subbuffer(const CBuffer &input, const unsigned int &index, const unsigned int &length);
    CBuffer subbuffer(unsigned int start, unsigned int length);

    QString getPrintBuffer(bool hex = false) const;
    void printBuffer(bool hex = true) const;
    size_t size() const;
    unsigned char getCommand() const;
    void setCommand(unsigned char value);
    virtual void clear();

    virtual unsigned char getCRC () const;
    virtual void setCanID(const unsigned int);
    virtual unsigned int getCanID() const;

protected:
    CLogger *logger;
    unsigned char command = 0;

};

#endif // CBuffer_H
