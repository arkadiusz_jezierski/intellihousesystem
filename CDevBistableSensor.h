#ifndef CDEVBISTABLESENSOR_H
#define CDEVBISTABLESENSOR_H

#include <QObject>
#include <CCanDevice.h>

#define PARAM_STATE         "state"

class CDevBistableSensor : public CCanDevice
{
    Q_OBJECT

public:
    explicit CDevBistableSensor();
    ~CDevBistableSensor();

    void initParameters();
    void initCommandMethods();
    void initRequestMethods();

    typedef void (CDevBistableSensor::*method)(CBuffer &data);
    QMap<COMMAND, method> methodsMap;

    typedef void (CDevBistableSensor::*requestMethod)();
    QMap<QString, requestMethod> requestMethodsMap;

    void getSensorStatus(CBuffer &data);

    void requestSensorState();


public slots:

    void commandMethodOfBistableSwitchSensor(COMMAND cmd, CBuffer &data);
    void requestParameterForBistableSwitchSensor(QString param);
};

#endif // CDEVBISTABLESENSOR_H
