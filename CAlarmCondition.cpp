#include "CAlarmCondition.h"

CAlarmCondition::CAlarmCondition(bool armed) : CCondition()
{
	this->type = EConditionType::ALARM;
    this->systemArmed = armed;
}

bool CAlarmCondition::operator ==(const CAlarmCondition &event){
    return this->systemArmed == event.systemArmed;
}


QString CAlarmCondition::toString() {
    QString str = "";

    if (this->extLogicConditionId != "") {
        str = this->extLogicConditionId + ": ";
    }

    if (this->systemArmed) {
        str += "Alarm Condition [IF alarm IS armed]";
    } else {
        str += "Alarm Condition [IF alarm IS disarmed]";
    }

    return str;
}



void CAlarmCondition::calculateCondition() {

	if (alarmModule->systemIsArmed() == systemArmed){
        logger->debug(EVENT_MANAGER, "Alarm condition and alarm expected state are consistent, comparison result: TRUE");
        this->result = EConditionResult::True_Val;
    } else {
    	logger->debug(EVENT_MANAGER, "Alarm condition and alarm expected state are NOT consistent, comparison result: FALSE");
        this->result = EConditionResult::False_Val;
    }

}