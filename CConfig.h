#ifndef CCONFIG_H
#define CCONFIG_H

#include <QObject>
#include <QCoreApplication>
#include <CSingleton.h>
#include <CLogger.h>
#include <QDebug>
#include <QFile>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <CException.h>

#define FIRMWARE_PATH               "firmwarePath"
#define OPERATIONS_PATH             "operationsPath"
#define TRANS_RESTART_TIME          "transceiverRestartTime"
#define DEVICE_ACTIVE_TIME          "deviceActiveTime"
#define PING_INTERVAL               "pingDeviceInterval"
#define MAX_LAST_SEEN_TIME          "maxLastSeenTimeOfDevice"
#define EXPECTED_PARAM_INTERVAL     "expectedParameterReadingInterval"
#define WEBSERVICE_PORT             "webservicePort"
#define GSM_MODULE                  "gsmModule"
#define ALARM_HASH                  "alarmHash"
#define ALARM_CODE                  "alarmCode"
#define ALARM_DELAY_TIME            "alarmDelayTime"
#define LOG_SERVER_URL	            "logServerUrl"
#define LOG_FILTERS_PATH			"logFiltersPath"



class CConfig : public QObject
{
    Q_OBJECT

    CLogger *logger;
    QMap<QString, QString> configuration;
    void loadConfig();
    QString loadFromFile();

public:
    explicit CConfig(QObject *parent = 0);
    ~CConfig();

    bool isConfigComplete();

    QString getConfigParam(QString name);
};

#endif // CCONFIG_H
