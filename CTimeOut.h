#ifndef _CTIMEOUT_H
#define	_CTIMEOUT_H

#include <ctime>
#include <sys/time.h>

class CTimeOut
{
public:
    CTimeOut();
    virtual ~CTimeOut();

    void startTimeOut(int Tout);
    bool isTimeOut();



private:
    void getTimer(void);
    clock_t m_TimeOut;
    clock_t m_TimerTicks;
    clock_t m_msTimer;
    clock_t m_msDiv;
    struct timeval time;

};

#endif	/* _CTIMEOUT_H */
