#ifndef CCANDEVICE_H
#define CCANDEVICE_H

#include <QObject>
#include <QMap>
#include <HTypes.h>
#include <QDataStream>
#include <CLogger.h>
#include <CSingleton.h>
#include <HCommConstans.h>
#include <CTransceiver.h>
#include "CFirmwareLoader.h"
#include "CFirmwareBuffer.h"
#include "CDeviceWatcher.h"
#include <CConfig.h>



/**
 * @brief The CCanDevice class
 * Base class for all devices.
 */


struct SCommandSet{
    QString parameter;
    qint64 value;
    bool expected;

    SCommandSet(QString param, qint64 val){
        parameter = param;
        value = val;
        expected = true;
    }

    SCommandSet(){
        expected = true;
    }
};


class CCanDevice : public QObject
{
    Q_OBJECT

    UID uid;
    CDeviceWatcher *deviceWatcher = NULL;

    //firmware upload methods
    void resetDevice();
    bool checkReady();
    void getDummyFrames();
    void clearFlash();
    void exitBootMode();
    void initBootWrite(unsigned int address);
    void initBootRead(unsigned int address);
    void initSelfCRC();
    unsigned int getSelfCRC();
    void writeProgramData(CBuffer data);
    CCanBuffer readProgramData();
    void uploadExFirmware(CFirmwareBuffer &buffer);
    unsigned int uploadFirmware(CFirmwareBuffer &buffer);
    unsigned int uploadExFirmwarePart(CFirmwareBuffer &buffer);
    void verifyFirmware(unsigned int crc);
    void calcCRC(unsigned int &crc, CFirmwareBuffer buf);

    DevCat category;
    unsigned char address;
    QString deviceId;

protected:
    CLogger *logger = NULL;
    CTransceiver *transceiver = NULL;
    EParamChanges checkValueChanged(QString param, qint64 val);
    CConfig *config;

public:


    explicit CCanDevice(QObject *parent = 0);
    CCanDevice(DevCat category, unsigned char address, QObject *parent = 0);
    CCanDevice(unsigned char category, unsigned char address, QObject *parent = 0);
    CCanDevice(QString category, QString address, QObject *parent = 0);
    ~CCanDevice();

    void assignTransceiver(CTransceiver *transceiver);

    struct Value{
        bool undefined = true;
        qint64 value = 0;
        EParamChanges changes = EParamChanges::None;
    };

    bool operator < (const CCanDevice &device);
    bool operator == (const CCanDevice &device) const;
    bool operator == (const CCanDevice *device) const;

    QString objectName() const;
    QString toStringFull() const;

    void setCategoryAndAddress(DevCat category, unsigned char address);
    void setCategory(DevCat category);
    DevCat getCategory();
    unsigned char getAddress();
    QString getDeviceId();

    QString name;
    QMap<QString, Value> parameters;
    QMap<QString, Value> commands;

    // static QString generateDeviceId(DevCat cat, unsigned char address);
    static QString generateDeviceId(DevCat cat, unsigned char address);

    static bool checkCategory(DevCat cat);
    static QString categoryToString(DevCat cat);
    //    static EDeviceCategory stringToCategory(QString cat);
    EDeviceType getDeviceType() const;
    static void printDeviceErrorInfo(const CCanBuffer& buffer);

    void executeCommand(COMMAND cmd, CBuffer data);
    void requestParameter(QString param);
    SCommandSet executeAction(SCommandSet commSet);
    void sendACKForSensor(const CCanBuffer &message);
    void sendErrorFrameACK();
    void sendPing();
    void sendBootCommand();
    void sendInitCommand();

    void uploadFirmware(QString fileName);

    void clearStatuses();
    void clearChangingStatus(QString param);




    int resetCounter = 0;
    int wdtResetCounter = 0;

    bool isActive();

    void setUID(const UID&uid);
    UID getUID()const;

    qint64 getLastSeenTime();
    void updateLastSeenTime();

    qint64 startupTimestamp = 0;

    bool assignedToEventManager = false;

signals:
    void sendEvent(DevCat category, unsigned char address, QString parameter, qint64 value);
public slots:

};






#endif // CCANDEVICE_H
