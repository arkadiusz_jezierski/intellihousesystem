

#include "CRestApiService.h"

CRestApiService::CRestApiService() {

    logger = &CSingleton<CLogger>::Instance();
    deviceManager = &CSingleton<CDeviceManager>::Instance();
    alarm = &CSingleton<CAlarmModule>::Instance();

    initRequestMethods();

    
}

void CRestApiService::run() {
    logger->info(REST_SERVICE, "Starting REST service...");
    Net::Address addr(Net::Ipv4::any(), Net::Port(9080));
    auto opts = Net::Http::Endpoint::options()
       .threads(1)
       .flags(Tcp::Options::InstallSignalHandler | Tcp::Options::ReuseAddr);

    server = new Http::Endpoint(addr);
    server->init(opts);
    server->setHandler(Http::make_handler<CRestHandler>());

    runThread = true;
    threadPaused = false;

    server->serveThreaded();

    while(runThread) {
        sleep(1);
    }
}

void CRestApiService::killThread() {
    logger->info(REST_SERVICE, "Stopping REST service...");
    runThread = false;
    server->shutdown();
    delete server;
    server = NULL;
}

void CRestApiService::pauseThread() {
    logger->info(REST_SERVICE, "Pausing REST service...");
    threadPaused = true;
}

void CRestApiService::unpauseThread() {
    logger->info(REST_SERVICE, "Unpausing REST service...");
    threadPaused = false;
}

void CRestApiService::initRequestMethods() {
    //      devObject: {"category":devCat, "address":adrDev, "active":bool, "name":"devName", "uid":"uid"}

    requestMethodsMap[REST_REQ_GET_JSON] = &CRestApiService::getJson;
    requestMethodsMap[REST_REQ_SET_DEV_NAME] = &CRestApiService::setDeviceName;
    requestMethodsMap[REST_REQ_SET_JSON] = &CRestApiService::saveJson;
    requestMethodsMap[REST_REQ_SET_DEV_PARAM] = &CRestApiService::setDeviceParameter;
    requestMethodsMap[REST_REQ_GET_DEV_PARAM] = &CRestApiService::getDeviceParameter;
    requestMethodsMap[REST_REQ_GET_DEVS_LIST] = &CRestApiService::getDevicesList;
    requestMethodsMap[REST_REQ_GET_FILES_LIST] = &CRestApiService::getFilesList;
    requestMethodsMap[REST_REQ_UPLOAD_FIRMWARE] = &CRestApiService::uploadFirmware;
    requestMethodsMap[REST_REQ_BOOT_DEV] = &CRestApiService::bootDevice;
    requestMethodsMap[REST_REQ_INIT_DEVS] = &CRestApiService::initDevices;
    requestMethodsMap[REST_REQ_REMOVE_DEV] = &CRestApiService::removeDevice;
    requestMethodsMap[REST_REQ_REMOVE_DEV_ADV] = &CRestApiService::removeDeviceAdvance;
    requestMethodsMap[REST_REQ_GET_DEV_DETAILS] = &CRestApiService::getDeviceDetails;
    requestMethodsMap[REST_REQ_GET_DEV_PARAMS] = &CRestApiService::getDeviceParameters;
    requestMethodsMap[REST_REQ_GET_CATEGORIES] = &CRestApiService::getCategories;
    requestMethodsMap[REST_REQ_SEND_MESSAGE] = &CRestApiService::sendMessage;
    requestMethodsMap[REST_REQ_ARM_SYSTEM] = &CRestApiService::armSystem;
    requestMethodsMap[REST_REQ_GET_ARM_STATUS] = &CRestApiService::getArmSystemStatus;
}

void CRestApiService::requestDispatcher(QString request, Http::ResponseWriter &response) {
    QJsonObject requestJson = QJsonDocument::fromJson(request.toUtf8()).object();
    QJsonDocument doc(requestJson);

    QString requestName = requestJson[REST_REQUEST_NAME].toString();

    logger->info(REST_SERVICE, "Received request: " + requestName);

    if (threadPaused) {
        sendPausedThreadResponse(response);
        return;
    } 

    if (requestMethodsMap.contains(requestName)){
        try{
            (this->*requestMethodsMap[requestName])(requestJson, response);
        }catch(CException ex){
            logger->error(REST_SERVICE, "Exception in Rest Service: " + ex.what());
        }
    }else{
        logger->warning(REST_SERVICE, "No request method found: " + requestName);
    }
}

// response: {"errorCode":err, "result":"res"}
void CRestApiService::sendPausedThreadResponse(Http::ResponseWriter &response) {
    logger->info(REST_SERVICE, "Sending info about paused Rest service");
    QJsonObject jsonResp;
   
    jsonResp[REST_RES_ERR_CODE] = 1;
    jsonResp[REST_RES_RESULT] = "Rest Service temporary unavailable";
             
    sendResponse(response, jsonResp);
}

CCanDevice* CRestApiService::getCanDevice(QJsonObject device) {
    return deviceManager->getCanDevice(
                static_cast<DevCat>(device[REST_DEVICE_CAT].toInt()),
                static_cast<unsigned char>(device[REST_DEVICE_ADDR].toInt())
            );
}

CCanDevice* CRestApiService::getCanDevice(const UID &uid){
    return deviceManager->getFirstCanDevice(uid);
}


void CRestApiService::sendResponse(Http::ResponseWriter &response, QJsonObject jsonResponse) {
    QJsonDocument docjson(jsonResponse);
    QByteArray bytes = docjson.toJson();
    response.send(Http::Code::Ok, bytes.toStdString());
}

// request:  {"requestName":"setDeviceParameter", "device":{devObject}, "parameter":"paramName", "value":val}
// response: {"errorCode":err, "result":"res"}
void CRestApiService::setDeviceParameter(QJsonObject request, Http::ResponseWriter &response) {
    QJsonObject device = request[REST_REQ_PARAM_DEVICE].toObject();
    QString param = request[REST_REQ_PARAM_PARAMETER].toString();
    qint64 value = (qint64)request[REST_REQ_PARAM_VALUE].toDouble();
    qint64 devCategory = device[REST_DEVICE_CAT].toInt();
    qint64 devAddress = device[REST_DEVICE_ADDR].toInt();

    logger->info(REST_SERVICE, "setDeviceParameter(device: ["
                 + QString::number(devCategory)
                 + ":"
                 + QString::number(devAddress)
                 + "], param: "
                 + param
                 + ", val: "
                 + QString::number(value)
                 + ")");

    QJsonObject jsonResp;
    CCanDevice * canDevice = getCanDevice(device);
    if (canDevice == NULL){
        jsonResp[REST_RES_ERR_CODE] = 1;
        jsonResp[REST_RES_RESULT] = "No device found in category "
                           + QString::number(devCategory)
                           + " with address "
                           + QString::number(devAddress);

    }else{
        if (canDevice->commands.contains(param)){
            //if (canDevice->parameters[parameter].undefined || canDevice->parameters[parameter].value != value){
                emit sendDeviceAction(canDevice->getCategory(), canDevice->getAddress(), param, value);
            //}

            if (canDevice->getCategory() == DevCat::BroadCast
                    || canDevice->getAddress() == 0){
                jsonResp[REST_RES_ERR_CODE] = 0;
                jsonResp[REST_RES_RESULT] = "OK";
            }else if (canDevice->isActive()){
                jsonResp[REST_RES_ERR_CODE] = 0;
                jsonResp[REST_RES_RESULT] = "OK";
            }else{
                jsonResp[REST_RES_ERR_CODE] = 2;
                jsonResp[REST_RES_RESULT] = "Device is inactive, command sent";
            }
        }else{
            jsonResp[REST_RES_ERR_CODE] = 3;
            jsonResp[REST_RES_RESULT] = "Device parameter '" + param + "' not found";
        }


    }

    sendResponse(response, jsonResp);

}

// request:  {"requestName":"getJson", "id":"jsonId"}
// response: {"errorCode":err, "result":"res", "json":"jsonBody"}
void CRestApiService::getJson(QJsonObject request, Http::ResponseWriter &response) {
    QString jsonId = request[REST_REQ_PARAM_JSON_ID].toString();
    logger->info(REST_SERVICE, "getJson(id: "+ jsonId +")");

    QString result;
    QJsonObject jsonResp;
    QString responseString = "";

    if (jsonId != JSON_ID_OPERATIONS && jsonId != JSON_ID_LOG_FILTERS) {
        result = "Json with ID[" + jsonId + "] not found";
        jsonResp[REST_RES_ERR_CODE] = 1;
        jsonResp[REST_RES_RESULT] = result;
    } else {
        if (jsonId == JSON_ID_OPERATIONS){
            responseString = COperationListLoader::loadFromFile();
        } else if (jsonId == JSON_ID_LOG_FILTERS) {
            responseString = CLoggerPoster::loadFromFile();
        }

        if (responseString.length() > 0) {
            jsonResp[REST_RES_ERR_CODE] = 0;
            jsonResp[REST_RES_RESULT] = "OK";
            if (responseString.length() > 30) {
                result = "json: [ " + responseString.mid(0, 30) + "... (len:" + QString::number(responseString.length())+ "))]";
            } else {
                result = "json: [ " + responseString + " ]";
            }
        } else {
            result = "Json file not found";
            jsonResp[REST_RES_ERR_CODE] = 1;
            jsonResp[REST_RES_RESULT] = result;
        }
    }

    jsonResp[REST_RES_JSON_BODY] = responseString;

    logger->info(REST_SERVICE, "getJson result: " + result);
    sendResponse(response, jsonResp);

}

// request:  {"requestName":"setDeviceName", "device":{devObject}, "name":"devName"}
// response: {"errorCode":err, "result":"res"}
void CRestApiService::setDeviceName(QJsonObject request, Http::ResponseWriter &response) {
    QJsonObject device = request[REST_REQ_PARAM_DEVICE].toObject();
    QString name = request[REST_REQ_PARAM_NAME].toString();
    qint64 devCategory = device[REST_DEVICE_CAT].toInt();
    qint64 devAddress = device[REST_DEVICE_ADDR].toInt();

    logger->info(REST_SERVICE, "setDeviceName(device: ["
                 + QString::number(devCategory)
                 + ":"
                 + QString::number(devAddress)
                 + "], name: "
                 + name
                 + ")");

    QJsonObject jsonResp;
    CCanDevice * canDevice = getCanDevice(device);
    if (canDevice == NULL){
        jsonResp[REST_RES_ERR_CODE] = 1;
        jsonResp[REST_RES_RESULT] = "No device found in category "
                + QString::number(devCategory)
                + " with address "
                + QString::number(devAddress);
    }else{
        deviceManager->setDeviceName(canDevice, name);
        jsonResp[REST_RES_ERR_CODE] = 0;
        jsonResp[REST_RES_RESULT] = "OK";
    }

    sendResponse(response, jsonResp);

}

// request:  {"requestName":"saveJson", "id":"jsonId", "json":"jsonBody"}
// response: {"errorCode":err, "result":"res"}
void CRestApiService::saveJson(QJsonObject request, Http::ResponseWriter &response) {

    QString jsonId = request[REST_REQ_PARAM_JSON_ID].toString();
    QJsonObject json = request[REST_REQ_PARAM_JSON_BODY].toObject();
    QJsonDocument doc(json);
    QString jsonBody(doc.toJson(QJsonDocument::Compact));

    logger->info(REST_SERVICE, "saveJson(id: "
                 + jsonId
                 + ", body: "
                 + ((jsonBody.length() > 0) ? "...(len:" + QString::number(jsonBody.length()) + "))" : "EMPTY)")
                 + ")");

    QJsonObject jsonResp;
    jsonResp[REST_RES_ERR_CODE] = 0;
    jsonResp[REST_RES_RESULT] = "OK";

    if (jsonId == JSON_ID_OPERATIONS) {
        if (COperationListLoader::saveJsonToFile(jsonBody)) {
            emit sendReloadOperationsSignal();
        } else {
            jsonResp[REST_RES_ERR_CODE] = 1;
            jsonResp[REST_RES_RESULT] = "Cannot save Json file '" + jsonId + "'";
        }
    } else if (jsonId == JSON_ID_LOG_FILTERS) { 
        if (CLoggerPoster::saveJsonToFile(jsonBody)) {
            emit sendReloadLogFiltersSignal();
        } else {
            jsonResp[REST_RES_ERR_CODE] = 1;
            jsonResp[REST_RES_RESULT] = "Cannot save Json file '" + jsonId + "'";
        }
    } else {
        jsonResp[REST_RES_ERR_CODE] = 2;
        jsonResp[REST_RES_RESULT] = "Json with ID[" + jsonId + "] not found";
    }

    sendResponse(response, jsonResp);

}

// request:  {"requestName":"getDeviceParameter", "device":{devObject}, "parameter":"paramName"}
// response: {"errorCode":err, "result":"res", "value":val}
void CRestApiService::getDeviceParameter(QJsonObject request, Http::ResponseWriter &response) {
    QJsonObject device = request[REST_REQ_PARAM_DEVICE].toObject();
    QString param = request[REST_REQ_PARAM_PARAMETER].toString();
    qint64 devCategory = device[REST_DEVICE_CAT].toInt();
    qint64 devAddress = device[REST_DEVICE_ADDR].toInt();

    logger->info(REST_SERVICE, "getDeviceParameter(device: ["
                 + QString::number(devCategory)
                 + ":"
                 + QString::number(devAddress)
                 + "], param: "
                 + param
                 + ")");

    QString result;
    QJsonObject jsonResp;
    jsonResp[REST_RES_ERR_CODE] = 0;
    jsonResp[REST_RES_RESULT] = "OK";
    jsonResp[REST_RES_VALUE] = 0;

    CCanDevice * canDevice = getCanDevice(device);
    if (canDevice == NULL){
        result = "No device found in category "
                           + QString::number(devCategory)
                           + " with address "
                           + QString::number(devAddress);
        jsonResp[REST_RES_ERR_CODE] = 1;
        jsonResp[REST_RES_RESULT] = result;
    }else{
        if (canDevice->parameters.contains(param)){
            if (canDevice->parameters[param].undefined){
                result = "Device parameter '" + param + "' undefined";
                jsonResp[REST_RES_ERR_CODE] = 2;
                jsonResp[REST_RES_RESULT] = result;
                emit sendDeviceParameterRequest(canDevice->getCategory(), canDevice->getAddress(), param);
            }else{
                result = "Parameter value: " + QString::number(canDevice->parameters[param].value);
                jsonResp[REST_RES_VALUE] = canDevice->parameters[param].value;
            }
        }else{
            result = "Device parameter '" + param + "' not found";
            jsonResp[REST_RES_ERR_CODE] = 3;
            jsonResp[REST_RES_RESULT] = result;
        }


    }

    logger->info(REST_SERVICE, "getDeviceParameter result: " + result);
    sendResponse(response, jsonResp);

}

// request:  {"requestName":"getDevicesList"}
// response: {"errorCode":err, "result":"res", "devices":[{devObject},...]}
void CRestApiService::getDevicesList(QJsonObject request, Http::ResponseWriter &response) {
    (void) request;
    logger->info(REST_SERVICE, "getDevicesList()");

    QJsonObject jsonResp;
    jsonResp[REST_RES_ERR_CODE] = 0;
    jsonResp[REST_RES_RESULT] = "OK";

    QList<CCanDevice*> devicesList = deviceManager->getCanDevices();
    if (devicesList.size() == 0){
        jsonResp[REST_RES_RESULT] = "No devices found";
        jsonResp[REST_RES_ERR_CODE] = 1;
    }else{
        QJsonArray array;

        for(CCanDevice *dev : devicesList){
            QJsonObject device;
            device[REST_DEVICE_CAT] = static_cast<qint64>(dev->getCategory());
            device[REST_DEVICE_ADDR] = dev->getAddress();
            device[REST_DEVICE_NAME] = dev->name;
            device[REST_DEVICE_ACTIVE] = (dev->isActive() ? true : false);
            device[REST_DEVICE_UID] = static_cast<qint64>(dev->getUID());
            array.append(device);
        }

        for (QString cat : EDevice::getCategoryList()){
            QJsonObject device;
            device[REST_DEVICE_CAT] = static_cast<qint64>(EDevice::getCategoryId(cat));
            device[REST_DEVICE_ADDR] = 0;
            device[REST_DEVICE_NAME] = "All Devices";
            device[REST_DEVICE_ACTIVE] = true;
            device[REST_DEVICE_UID] = 0;
            array.append(device);
        }

        jsonResp[REST_RES_DEVICES] = array;
    }

    sendResponse(response, jsonResp);

}

// request:  {"requestName":"getFilesList"}
// response: {"errorCode":err, "result":"res", "files":["name",...]}
void CRestApiService::getFilesList(QJsonObject request, Http::ResponseWriter &response) {
    (void) request;
    logger->info(REST_SERVICE, "getFilesList()");

    QJsonObject jsonResp;
    jsonResp[REST_RES_ERR_CODE] = 0;
    jsonResp[REST_RES_RESULT] = "OK";

    QStringList fileNameList = CFirmwareLoader::getFileList();
    QJsonArray array;
    if (fileNameList.size() > 0){
        for (QString name : fileNameList) {
            array.append(name);
        }
        jsonResp[REST_RES_FILES] = array;
    }else{
        jsonResp[REST_RES_ERR_CODE] = 1;
        jsonResp[REST_RES_RESULT] = "Reading firmware path failed or directory not found";
    }

    sendResponse(response, jsonResp);

}

// request:  {"requestName":"uploadFirmware", "fileName":"name"}
// response: {"errorCode":err, "result":"res"}
void CRestApiService::uploadFirmware(QJsonObject request, Http::ResponseWriter &response) {
    QString name = request[REST_REQ_PARAM_FILENAME].toString();
    logger->info(REST_SERVICE, "uploadFirmware(fileName: " + name + ")");
    QJsonObject jsonResp;
    jsonResp[REST_RES_ERR_CODE] = 0;
    jsonResp[REST_RES_RESULT] = "OK";
    emit sendUploadFirmwareSignal(name);

    sendResponse(response, jsonResp);

}

// request:  {"requestName":"bootDevice", "uid":uid}
// response: {"errorCode":err, "result":"res"}
void CRestApiService::bootDevice(QJsonObject request, Http::ResponseWriter &response) {
    qint64 uid = request[REST_REQ_PARAM_UID].toInt();
    logger->info(REST_SERVICE, "bootDevice(uid: " + QString::number(uid, 16) + ")");
    QJsonObject jsonResp;
    jsonResp[REST_RES_ERR_CODE] = 0;
    jsonResp[REST_RES_RESULT] = "OK";
    emit sendBootDeviceSignal(uid);

    sendResponse(response, jsonResp);

}

// request:  {"requestName":"initDevices"}
// response: {"errorCode":err, "result":"res"}
void CRestApiService::initDevices(QJsonObject request, Http::ResponseWriter &response) {
    (void) request;
    logger->info(REST_SERVICE, "initDevices()");
    QJsonObject jsonResp;
    jsonResp[REST_RES_ERR_CODE] = 0;
    jsonResp[REST_RES_RESULT] = "OK";
    emit sendInitDeviceSignal();

    sendResponse(response, jsonResp);

}

// request:  {"requestName":"removeDevice", "uid":uid}
// response: {"errorCode":err, "result":"res"}
void CRestApiService::removeDevice(QJsonObject request, Http::ResponseWriter &response) {
    qint64 uid = request[REST_REQ_PARAM_UID].toInt();
    logger->info(REST_SERVICE, "removeDevice(uid: " + QString::number(uid, 16) + ")");
    QJsonObject jsonResp;
    jsonResp[REST_RES_ERR_CODE] = 0;
    jsonResp[REST_RES_RESULT] = "OK";
    if (getCanDevice(uid) == NULL){
        jsonResp[REST_RES_ERR_CODE] = 1;
        jsonResp[REST_RES_RESULT] = "No device with UID " + QString::number(uid, 16) + " found";
    }else{
        emit sendRemoveDeviceSignal(uid);
    }

    sendResponse(response, jsonResp);

}

// request:  {"requestName":"removeDeviceAdvance", "category":cat, "adrStart":adr, "adrStop":adr}
// response: {"errorCode":err, "result":"res"}
void CRestApiService::removeDeviceAdvance(QJsonObject request, Http::ResponseWriter &response) {
    qint64 category = request[REST_REQ_PARAM_CAT].toInt();
    qint64 adrStart = request[REST_REQ_PARAM_ADR_START].toInt();
    qint64 adrStop = request[REST_REQ_PARAM_ADR_STOP].toInt();
    logger->info(REST_SERVICE, "removeDeviceAdvance(cat: "
                 + QString::number(category)
                 + ", adrStart: "
                 + QString::number(adrStart)
                 + ", adrStop: "
                 + QString::number(adrStop)
                 + ")");
    QJsonObject jsonResp;
    jsonResp[REST_RES_ERR_CODE] = 0;
    jsonResp[REST_RES_RESULT] = "OK";
    emit sendRemoveDeviceAdvanceSignal(static_cast<DevCat>(category), adrStart, adrStop);

    sendResponse(response, jsonResp);

}


// request:  {"requestName":"getDeviceDetails", "device":{devObject}}
// response: {"errorCode":err, "result":"res", "lastSeen":val, "resetCounter":val, "wdtCounter":val, "lastStartup":val}
void CRestApiService::getDeviceDetails(QJsonObject request, Http::ResponseWriter &response) {
    QJsonObject device = request[REST_REQ_PARAM_DEVICE].toObject();
    qint64 devCategory = device[REST_DEVICE_CAT].toInt();
    qint64 devAddress = device[REST_DEVICE_ADDR].toInt();

    logger->info(REST_SERVICE, "getDeviceDetails(device: ["
                 + QString::number(devCategory)
                 + ":"
                 + QString::number(devAddress)
                 + "])");

    QJsonObject jsonResp;
    jsonResp[REST_RES_ERR_CODE] = 0;
    jsonResp[REST_RES_RESULT] = "OK";

    CCanDevice * canDevice = getCanDevice(device);
    if (canDevice == NULL){
        jsonResp[REST_RES_ERR_CODE] = 1;
        jsonResp[REST_RES_RESULT] = "No device found in category "
                           + QString::number(devCategory)
                           + " with address "
                           + QString::number(devAddress);
    }else{
        jsonResp[REST_RES_LAST_SEEN] = canDevice->getLastSeenTime();
        jsonResp[REST_RES_RESET_CNT] = canDevice->resetCounter;
        jsonResp[REST_RES_WDT_CNT] = canDevice->wdtResetCounter;
        jsonResp[REST_RES_LAST_STARTUP] = canDevice->startupTimestamp;
    }

    sendResponse(response, jsonResp);

}

// request:  {"requestName":"getDeviceParameters", "device":{devObject}}
// response: {"errorCode":err, "result":"res", "parameters":[{"parameter":"name", "defined":bool, "value":val},...]}
void CRestApiService::getDeviceParameters(QJsonObject request, Http::ResponseWriter &response) {
    QJsonObject device = request[REST_REQ_PARAM_DEVICE].toObject();
    qint64 devCategory = device[REST_DEVICE_CAT].toInt();
    qint64 devAddress = device[REST_DEVICE_ADDR].toInt();

    logger->info(REST_SERVICE, "getDeviceParameters(device: ["
                 + QString::number(devCategory)
                 + ":"
                 + QString::number(devAddress)
                 + "])");

    QJsonObject jsonResp;
    jsonResp[REST_RES_ERR_CODE] = 0;
    jsonResp[REST_RES_RESULT] = "OK";

    CCanDevice * canDevice = getCanDevice(device);
    if (canDevice == NULL){
        jsonResp[REST_RES_ERR_CODE] = 1;
        jsonResp[REST_RES_RESULT] = "No device found in category "
                           + QString::number(devCategory)
                           + " with address "
                           + QString::number(devAddress);
    }else{
        QJsonArray params;
        for (QString param : canDevice->parameters.keys()){
            QJsonObject parameter;
            parameter[REST_RES_DEV_PARAM_NAME] = param;
            if (!canDevice->parameters[param].undefined){
                parameter[REST_RES_DEV_PARAM_DEFINED] = true;
                parameter[REST_RES_DEV_PARAM_VALUE] = canDevice->parameters[param].value;
            } else {
                parameter[REST_RES_DEV_PARAM_DEFINED] = false;
                parameter[REST_RES_DEV_PARAM_VALUE] = 0;
            }
            params.append(parameter);
        }
        jsonResp[REST_RES_DEV_PARAMS] = params;
    }

    sendResponse(response, jsonResp);

}

// request:  {"requestName":"getCategories"}
// response: {"errorCode":err,
//            "result":"res",
//            "categories":[
//                {"name":"catName",
//                 "id":"catId",
//                 "commands":["com1",...],
//                 "parameters":["param1",...]
//                }
//            ,...]
//            }
void CRestApiService::getCategories(QJsonObject request, Http::ResponseWriter &response) {
    (void) request;
    logger->info(REST_SERVICE, "getCategories()");

    QJsonObject jsonResp;
    QList<QString> categories = EDevice::getCategoryList();
    if (categories.size() > 0){
        jsonResp[REST_RES_ERR_CODE] = 0;
        jsonResp[REST_RES_RESULT] = "OK";
        QJsonArray array;

        for (QString cat : categories){
            DevCat devCategory = EDevice::getCategory(cat);
            CCanDevice *device = deviceManager->createDeviceForCategory(devCategory);

            QJsonObject catDescription;
            catDescription[REST_RES_CAT_PARAM_NAME] = cat;
            catDescription[REST_RES_CAT_PARAM_ID] = EDevice::getCategoryId(cat);

            QJsonArray commandsList;
            for (QString comm : device->commands.keys()){
                commandsList.append(comm);
            }
            catDescription[REST_RES_CAT_COMMANDS] = commandsList;

            QJsonArray paramList;
            for (QString param : device->parameters.keys()){
                paramList.append(param);
            }
            catDescription[REST_RES_CAT_PARAMS] = paramList;

            array.append(catDescription);
            delete device;
            device = NULL;
        }

        jsonResp[REST_RES_CATEGORIES] = array;
    }else{
        jsonResp[REST_RES_ERR_CODE] = 1;
        jsonResp[REST_RES_RESULT] = "No categories found";
    }

    sendResponse(response, jsonResp);

}

// request:  {"requestName":"sendMessage", "number":"phoneNumber", "message":"msg"}
// response: {"errorCode":err, "result":"res"}
void CRestApiService::sendMessage(QJsonObject request, Http::ResponseWriter &response) {
    QString number = request[REST_REQ_PARAM_NUMBER].toString();
    QString shortMessage = request[REST_REQ_PARAM_MSG].toString();
    logger->info(REST_SERVICE, "sendMessage(number: " + number + ", message: " + shortMessage + ")");

    QJsonObject jsonResp;

    MessageValidationResult result = CGsmModem::validateMessage(number, shortMessage);
    jsonResp[REST_RES_ERR_CODE] = result;

    QString responseMsg = "";

    if (result == MESSAGE_OK) {
        responseMsg = "OK";
    } else {
        if (result & MESSAGE_CONTAINS_FORBIDDEN_SIGNS) {
            responseMsg += "Message contains forbidden signs";
        }

        if (result & MESSAGE_TOO_LONG) {
            if (responseMsg.length() > 0) {
                responseMsg += ", ";
            }
            responseMsg += "Message is too long";
        }

        if (result & MESSAGE_INVALID_PHONE_NUMBER) {
            if (responseMsg.length() > 0) {
                responseMsg += ", ";
            }
            responseMsg += "Invalid phone number";
        }
    }

    jsonResp[REST_RES_RESULT] = responseMsg;

    emit sendMessageSignal(number, shortMessage);

    sendResponse(response, jsonResp);

}

// request:  {"requestName":"armSystem", "armDisarm":1, "hash":"hashCode"}
// response: {"errorCode":err, "result":"res"}
void CRestApiService::armSystem(QJsonObject request, Http::ResponseWriter &response) {
    qint64 armDisarm = request[REST_REQ_PARAM_ARM].toInt();
    QString hash = request[REST_REQ_PARAM_HASH].toString();
    logger->info(REST_SERVICE, "armSystem(armDisarm: " + QString::number(armDisarm) + ", hash: ######)");

    QJsonObject jsonResp;
    jsonResp[REST_RES_ERR_CODE] = 0;
    jsonResp[REST_RES_RESULT] = "OK";
    if (armDisarm != 0) {
        alarm->armSystem();
    } else {
        if (!alarm->disarmSystem(hash)) {
            jsonResp[REST_RES_ERR_CODE] = 1;
            jsonResp[REST_RES_RESULT] = "Invalid Password";
        }
    }

    sendResponse(response, jsonResp);

}

// request:  {"requestName":"getArmSystemStatus"}
// response: {"errorCode":err, "result":"res", "value":0}
void CRestApiService::getArmSystemStatus(QJsonObject request, Http::ResponseWriter &response) {
    (void) request;
    logger->info(REST_SERVICE, "getArmSystemStatus()");

    QJsonObject jsonResp;
    jsonResp[REST_RES_ERR_CODE] = 0;
    jsonResp[REST_RES_RESULT] = "OK";
    jsonResp[REST_RES_VALUE] = alarm->systemIsArmed() ? 1 : 0;

    sendResponse(response, jsonResp);

}
