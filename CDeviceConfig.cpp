#include "CDeviceConfig.h"

CDeviceConfig::CDeviceConfig(QString cat, QString addr, QString param, QString val) {
	category = static_cast<DevCat>(cat.toInt());
    address = addr.toInt();
    parameter = param;
    value = val.toInt();

    sCategory = cat;
    sAddress = addr;
    sParam = param;
    sValue = val;

}


QString CDeviceConfig::toString() {
	return "[" + sCategory + ":" + sAddress + "]" + sParam + "=" + sValue;
}

DevCat CDeviceConfig::getDevCategory() {
	return category;
}

unsigned char CDeviceConfig::getDevAddress() {
	return address;
}

QString CDeviceConfig::getParam() {
	return parameter;
}

qint64 CDeviceConfig::getValue() {
	return value;
}