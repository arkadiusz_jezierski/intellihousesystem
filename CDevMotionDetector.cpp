#include "CDevMotionDetector.h"

CDevMotionDetector::CDevMotionDetector()
{
    this->setCategory(DevCat::MotionDetector);

    initParameters();
    initCommandMethods();
    initRequestMethods();
    initExecuteMethods();
}

CDevMotionDetector::~CDevMotionDetector()
{

}


void CDevMotionDetector::commandMethodOfMotionDetector(COMMAND cmd, CBuffer &data){
    if (methodsMap.contains(cmd)){
        try{
            (this->*methodsMap[cmd])(data);
        }catch(CException ex){
            logger->error(DEVICE_MANAGER, "Error in MotionDetector: " + ex.what());
        }
    }else{
        logger->warning(DEVICE_MANAGER, "No method to invoke for command " + QString::number((int)cmd));
    }
}

void CDevMotionDetector::requestParameterForMotionDetector(QString param){

    if (requestMethodsMap.contains(param)){
        try{
            (this->*requestMethodsMap[param])();
        }catch(CException ex){
            logger->error(DEVICE_MANAGER, "Error in MotionDetector: " + ex.what());
        }
    }else{
        logger->warning(DEVICE_MANAGER, "MotionDetector: No request to invoke for parameter " + param);
    }
}

SCommandSet CDevMotionDetector::executeCommandForMotionDetector(SCommandSet commSet){
    SCommandSet retVal;
    if (executeDeviceCommandMap.contains(commSet.parameter)){
        try{
            retVal = (this->*executeDeviceCommandMap[commSet.parameter])(commSet.value);
        }catch(CException ex){
            logger->error(DEVICE_MANAGER, "Error in MotionDetector: " + ex.what());
        }
    }else{
        logger->warning(DEVICE_MANAGER, "MotionDetector: No execution to invoke for parameter " + commSet.parameter);
    }
    return retVal;
}

void CDevMotionDetector::initParameters(){
    Value val;
    val.undefined = true;
    val.value = 0;
    val.changes = EParamChanges::None;

    parameters[PARAM_LED_POWER] = val;
    parameters[PARAM_SENSITIVITY] = val;
    parameters[PARAM_TEMPER_STATUS] = val;
    parameters[PARAM_MOTION_STATUS] = val;
    commands[PARAM_LED_POWER] = val;
    commands[PARAM_SENSITIVITY] = val;
    commands[PARAM_TEMPER_STATUS] = val;

}

void CDevMotionDetector::initCommandMethods(){
    methodsMap[CMD_CAN_GET_SENSITIVITY] = &CDevMotionDetector::getSensitivity;
    methodsMap[CMD_CAN_GET_LED_POWER] = &CDevMotionDetector::getLedPower;
    methodsMap[CMD_CAN_GET_MOTION_DETECTED_STATUS] = &CDevMotionDetector::getMotionStatus;
    methodsMap[CMD_CAN_GET_TEMPER_CONTACT_STATUS] = &CDevMotionDetector::getTemperStatus;
    methodsMap[CMD_CAN_SET_SENSITIVITY] = &CDevMotionDetector::setSensitivity;
    methodsMap[CMD_CAN_SET_LED_POWER] = &CDevMotionDetector::setLedPower;
}

void CDevMotionDetector::initRequestMethods(){
    requestMethodsMap[PARAM_LED_POWER] = &CDevMotionDetector::requestLedPower;
    requestMethodsMap[PARAM_SENSITIVITY] = &CDevMotionDetector::requestSensitivity;
    requestMethodsMap[PARAM_MOTION_STATUS] = &CDevMotionDetector::requestMotionStatus;
    requestMethodsMap[PARAM_TEMPER_STATUS] = &CDevMotionDetector::requestTemperStatus;
}

void CDevMotionDetector::initExecuteMethods(){
    executeDeviceCommandMap[PARAM_LED_POWER] = &CDevMotionDetector::executeSetLedPower;
    executeDeviceCommandMap[PARAM_SENSITIVITY] = &CDevMotionDetector::executeSetSensitivity;
}

void CDevMotionDetector::getSensitivity(CBuffer &data){
    logger->info(DEVICE_MANAGER, this->objectName() + "->getSensitivity(" + QString::number(data[OFFSET_CAN_DATA]) + ")");

    Value val;
    val.undefined = false;
    val.value = data[OFFSET_CAN_DATA];
    val.changes = checkValueChanged(PARAM_SENSITIVITY, val.value);

    parameters[PARAM_SENSITIVITY] = val;

    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_SENSITIVITY, val.value);
}

void CDevMotionDetector::setSensitivity(CBuffer &data){
    logger->info(DEVICE_MANAGER, this->objectName() + "->setSensitivity(" + QString::number(data[OFFSET_CAN_DATA]) + ")");

    Value val;
    val.undefined = false;
    val.value = data[OFFSET_CAN_DATA];
    val.changes = checkValueChanged(PARAM_SENSITIVITY, val.value);

    parameters[PARAM_SENSITIVITY] = val;

    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_SENSITIVITY, val.value);
}

void CDevMotionDetector::getLedPower(CBuffer &data){
    logger->info(DEVICE_MANAGER, this->objectName() + "->getLedPower(" + QString::number(data[OFFSET_CAN_DATA]) + ")");

    Value val;
    val.undefined = false;
    val.value = data[OFFSET_CAN_DATA];
    val.changes = checkValueChanged(PARAM_LED_POWER, val.value);

    parameters[PARAM_LED_POWER] = val;

    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_LED_POWER, val.value);
}

void CDevMotionDetector::setLedPower(CBuffer &data){
    logger->info(DEVICE_MANAGER, this->objectName() + "->setLedPower(" + QString::number(data[OFFSET_CAN_DATA]) + ")");

    Value val;
    val.undefined = false;
    val.value = data[OFFSET_CAN_DATA];
    val.changes = checkValueChanged(PARAM_LED_POWER, val.value);

    parameters[PARAM_LED_POWER] = val;

    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_LED_POWER, val.value);
}

void CDevMotionDetector::getMotionStatus(CBuffer &data){
    logger->info(DEVICE_MANAGER, this->objectName() + "->getMotionStatus(" + QString::number(data[OFFSET_CAN_DATA]) + ")");

    Value val;
    val.undefined = false;
    val.value = data[OFFSET_CAN_DATA];
    val.changes = checkValueChanged(PARAM_MOTION_STATUS, val.value);

    parameters[PARAM_MOTION_STATUS] = val;

    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_MOTION_STATUS, val.value);
}

void CDevMotionDetector::getTemperStatus(CBuffer &data){
    logger->info(DEVICE_MANAGER, this->objectName() + "->getTemperStatus(" + QString::number(data[OFFSET_CAN_DATA]) + ")");

    Value val;
    val.undefined = false;
    val.value = data[OFFSET_CAN_DATA];
    val.changes = checkValueChanged(PARAM_TEMPER_STATUS, val.value);

    parameters[PARAM_TEMPER_STATUS] = val;


    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_TEMPER_STATUS, val.value);
}

void CDevMotionDetector::requestLedPower(){
    logger->info(DEVICE_MANAGER, this->objectName() + "->requestLedPower()");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_GET_LED_POWER);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
}

void CDevMotionDetector::requestSensitivity(){
    logger->info(DEVICE_MANAGER, this->objectName() + "->requestSensitivity()");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_GET_SENSITIVITY);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
}

void CDevMotionDetector::requestMotionStatus(){
    logger->info(DEVICE_MANAGER, this->objectName() + "->requestMotionStatus()");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_GET_MOTION_DETECTED_STATUS);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
}

void CDevMotionDetector::requestTemperStatus(){
    logger->info(DEVICE_MANAGER, this->objectName() + "->requestTemperStatus()");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_GET_TEMPER_CONTACT_STATUS);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
}

SCommandSet CDevMotionDetector::executeSetLedPower(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeSetLedPower(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_SET_LED_POWER);
    buffer << static_cast<unsigned char>(value);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;
    commSet.parameter = PARAM_LED_POWER;
    commSet.value = value;

    return commSet;
}

SCommandSet CDevMotionDetector::executeSetSensitivity(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeSetSensitivity(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_SET_SENSITIVITY);
    buffer << static_cast<unsigned char>(value);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;
    commSet.parameter = PARAM_SENSITIVITY;
    commSet.value = value;

    return commSet;
}
