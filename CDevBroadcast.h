#ifndef CDEVBROADCAST_H
#define CDEVBROADCAST_H

#include <QObject>
#include <CCanDevice.h>

class CDevBroadcast : public CCanDevice
{
    Q_OBJECT

    void executeCommandOff(qint64 value);


public:
    explicit CDevBroadcast();
    ~CDevBroadcast();

    void initParameters();
    void initExecuteMethods();

    typedef void (CDevBroadcast::*executeMethod)(qint64);
    QMap<QString, executeMethod> executeMethodsMap;

public slots:
    void executeActionForBroadCast(QString param, qint64 value);
};

#endif // CDEVBROADCAST_H
