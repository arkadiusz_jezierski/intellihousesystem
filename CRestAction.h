#ifndef CRESTACTION_H
#define CRESTACTION_H

#include <CAction.h>
#include "HTypes.h"
#include <QDateTime>
#include <QtGlobal>

class CRestAction : public CAction
{
public:
    CRestAction(QString endpoint, QString message);
    ~CRestAction();

    QString toString();

    QString endpoint;
    QString message;
};

#endif // CRESTACTION_H
