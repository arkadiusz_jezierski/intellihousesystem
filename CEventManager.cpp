#include "CEventManager.h"

CEventManager::CEventManager(QObject *parent) : QObject(parent)
{
    //qDebug()<<"CEvent constructor";
    logger = &CSingleton<CLogger>::Instance();
    alarmModule = &CSingleton<CAlarmModule>::Instance();
    opContainer = &CSingleton<COperationContainer>::Instance();
    lastAlarmState = alarmModule->systemIsArmed();
}

CEventManager::~CEventManager()
{
    //qDebug()<<"CEvent desstructor";
    opContainer->clearContainer();
}

void CEventManager::addEventToQueue(CEvent* event) {
    QMutexLocker locker(&eventQueueMutex);

    eventsList.push_back(event);
    logger->info(EVENT_MANAGER, "New event: <" + event->toString() + "> created, events queue size: " + QString::number(eventsList.size()));

}

void CEventManager::addNewDeviceEvent(DevCat category, unsigned char address, QString parameter, qint64 value) {
    CDeviceManager *deviceManager = &CSingleton<CDeviceManager>::Instance();
    CCanDevice *device = deviceManager->getCanDevice(category, address);
    if (device == NULL) {
        return;
    }

    CEvent *event = new CEvent(device, parameter, value);
    addEventToQueue(event);
}

void CEventManager::addNewTimeEvent(qint64 value) {
    CEvent *event = new CEvent(value, EEventType::TIME_EVENT);
    addEventToQueue(event);
}

void CEventManager::addNewAlarmEvent() {
    CEvent *event = new CEvent(EEventType::ALARM_EVENT);
    addEventToQueue(event);
}


CEvent* CEventManager::getEvent() {
    QMutexLocker locker(&eventQueueMutex);
    CEvent* firstElement = NULL;
    if (eventsList.size() > 0) {
        firstElement = eventsList.front();
        logger->low_info(EVENT_MANAGER, "Taking event <" + firstElement->toString() + "> from events queue");

    }
    return firstElement;
}

void CEventManager::removeFirstEvent() {
    QMutexLocker locker(&eventQueueMutex);

    if (eventsList.size() > 0) {
        CEvent * event = eventsList.first();
        logger->low_info(EVENT_MANAGER, "Removing event [" + event->toString() +"]");
        eventsList.removeFirst();
        delete event;
        event = NULL;
    }

}



void CEventManager::moveFirstEventToEndOfTheQueue() {
    QMutexLocker locker(&eventQueueMutex);
    CEvent* firstElement = NULL;
    if (eventsList.size() > 0) {
        firstElement = eventsList.front();
        logger->low_info(EVENT_MANAGER, "Taking first event <" + firstElement->toString() + "> from events queue");
    }

    if (firstElement != NULL && eventsList.size() > 1) {
        logger->low_info(EVENT_MANAGER, "Moving first event to the end of the queue");
        eventsList.removeFirst();
        eventsList.push_back(firstElement);
    }
}


void CEventManager::reloadOperationList() {
    logger->info(EVENT_MANAGER, "Reload operation list request received");
    realoadOperationsRequest = true;
}

void CEventManager::executeAction(COperation *operation) {

    logger->info(EVENT_MANAGER, "Executing actions for current operation");
    for (CAction *action : operation->actions) {
        logger->low_info(EVENT_MANAGER, "Action name: " + action->toString());
        if (action->getActionType() == EActionType::DEVICE_ACTION) {
            CDeviceAction* deviceAction = static_cast<CDeviceAction*>(action);
            if (deviceAction->device != NULL) {

                if (deviceAction->device->parameters.contains(deviceAction->parameter)) {

                    if(deviceAction->device->parameters[deviceAction->parameter].undefined ||
                            deviceAction->device->parameters[deviceAction->parameter].value != deviceAction->value) {

                        logger->low_info(EVENT_MANAGER, "Parameter " + deviceAction->parameter + " of device "
                                         + deviceAction->device->objectName() + " is undefined ("
                                         + QString::number(deviceAction->device->parameters[deviceAction->parameter].undefined)
                                + ") or devices parameter value ("
                                + QString::number(deviceAction->device->parameters[deviceAction->parameter].value)
                                + ") is not equal to requested value ("
                                + QString::number(deviceAction->value) + "), sending action request");

                        emit sendDeviceAction(deviceAction->device->getCategory(), deviceAction->device->getAddress(), deviceAction->parameter, deviceAction->value);
                    } else {
                        logger->low_info(EVENT_MANAGER, "Parameter " + deviceAction->parameter + " of device "
                                         + deviceAction->device->objectName() + " is equal to requested value, skipping action");
                    }
                } else if (deviceAction->device->commands.contains(deviceAction->parameter)) {
                    logger->low_info(EVENT_MANAGER, "Command " + deviceAction->parameter + " with value ("
                                     + QString::number(deviceAction->value) + ") for device "
                                     + deviceAction->device->objectName() + " will be executed");

                    emit sendDeviceAction(deviceAction->device->getCategory(), deviceAction->device->getAddress(), deviceAction->parameter, deviceAction->value);
                }
            } else {
                logger->error(EVENT_MANAGER, "NULL pointer of device");
            }
        } else if (action->getActionType() == EActionType::GSM_MODULE_ACTION) {
            CGsmModemAction* gsmAction = static_cast<CGsmModemAction*>(action);
            emit sendMessageSignal(gsmAction->phoneNumber, gsmAction->message);
        } else if (action->getActionType() == EActionType::REST_ACTION) {
            CRestAction* restAction = static_cast<CRestAction*>(action);
            emit sendPostRequest(restAction->endpoint, restAction->message);
        }

    }

}

void CEventManager::checkDelayedActions() {

    qint64 currentTime = QDateTime::currentMSecsSinceEpoch() / 1000;

    for (QString opName : delayedOperationsList.keys()) {
        if (delayedOperationsList[opName].executionTime < currentTime) {
            logger->low_info(EVENT_MANAGER, "Delayed operation exceed execution time: " + opName);
            executeAction(delayedOperationsList[opName].operation);
            delayedOperationsList.remove(opName);
        }
    }

}


void CEventManager::addDelayedAction(COperation *operation) {

    if (delayedOperationsList.contains(operation->name)) {
        qint64 executionTime = QDateTime::currentMSecsSinceEpoch() / 1000 + operation->delayForActions;
        logger->low_info(EVENT_MANAGER, "Delayed action already exists in the queue, execution time will be updated at " + QString(QDateTime::fromTime_t(executionTime).toString("hh:mm:ss")));
        delayedOperationsList[operation->name].executionTime = executionTime;
        return;
    }
  
    SDelayedOperation delayedOperation;
    delayedOperation.operation = operation;
    delayedOperation.executionTime = QDateTime::currentMSecsSinceEpoch() / 1000 + operation->delayForActions;
    delayedOperationsList[operation->name] = delayedOperation;

    logger->info(EVENT_MANAGER, QString("New delayed action has been started: ") + operation->name
        + QString(", execution time: " + QDateTime::fromTime_t(delayedOperation.executionTime).toString("hh:mm:ss")));

}

void CEventManager::removeDelayedAction(COperation *operation) {
    logger->info(EVENT_MANAGER, QString("Delayed action has been cancelled: ") + operation->name);

    delayedOperationsList.remove(operation->name);
}

void CEventManager::setEventToDoNotRemove() {
    eventToRemove = false;
    logger->low_info(EVENT_MANAGER, "Current event will not be removed");
}

void CEventManager::pauseThread() {
    pausedThread = true;
}

void CEventManager::resumeThread() {
    pausedThread = false;
}

void CEventManager::runEventManager() {
    logger->info(EVENT_MANAGER, "Start EventManager");

    runThread = true;
    while(runThread) {
        while(pausedThread) {
            QThread::msleep(10);
        }

        try {

            currentEvent = getEvent();
            if (currentEvent != NULL) {
                handleEvent(currentEvent);
                accomplishEvent();
                currentEvent = NULL;
            }
            
            checkDelayedActions();
            checkOtherConditions();

            if (realoadOperationsRequest) {
                logger->info(EVENT_MANAGER, "New request for reloading operations");
                delayedOperationsList.clear();
                COperationListLoader::loadOperations();
                realoadOperationsRequest = false;
            }

            QThread::msleep(1);
        }catch(CException ex) {
            logger->error(EVENT_MANAGER, "Error in EventManager thread: " + ex.what());
        }
    }
    logger->debug(EVENT_MANAGER, "Events thread finished");
}

void CEventManager::killThread() {
    runThread = false;
}

void CEventManager::checkOtherConditions() {
    qint64 currentTime = QDateTime::currentMSecsSinceEpoch();
    if (currentTime - lastTimeValuesAndAlarmCheck > 1000) {
        lastTimeValuesAndAlarmCheck = currentTime;

        currentTime = QTime::currentTime().msecsSinceStartOfDay() / 1000;

        if (opContainer->timeConditionValues.contains(currentTime)) {
            logger->info(EVENT_MANAGER, "Time depended operation exists for time value " + QString::number(currentTime) +"(" + CTools::secondsSinceMidnightToHourFormat(currentTime) + ")");
            addNewTimeEvent(currentTime);
        }

        bool currentAlarmStatus = alarmModule->systemIsArmed();
        if (currentAlarmStatus != lastAlarmState) {
            logger->info(EVENT_MANAGER, "Alarm state has changed");
            lastAlarmState = currentAlarmStatus;
            addNewAlarmEvent();
        }
    }
}

void CEventManager::printEventKey(QString key) {
    QStringList lst = key.split('-');
    CDeviceManager *deviceManager = &CSingleton<CDeviceManager>::Instance();

    QString out = "Event key [" + key + "] => ";

    if (lst[0] == KEY_DEVICE) {
        out += "Device [" + deviceManager->getCanDevice(lst[1])->name + "]<" + lst[2] + ">";
    } else if (lst[0] == KEY_TIME) {
        out += "Time <" + lst[1] + ">";
    } else if (lst[0] == KEY_ALARM) {
        out += "Alarm";
    } 

    logger->low_info(EVENT_MANAGER, out);

}

QList<QString> CEventManager::validateConditions(QSet<QString> conditions) {
    logger->debug(EVENT_MANAGER, "Checking conditions");
    QList<QString> validConditions;
    for (QString condId : conditions) {
        if (!opContainer->conditionByIdMap.contains(condId)) {
            logger->warning(EVENT_MANAGER, "ConditionByIdMap doesn\'t contain ID " + condId);
            continue;
        }

        CCondition* cond = opContainer->conditionByIdMap[condId];
        logger->debug(EVENT_MANAGER, "Checking condition: " + cond->toString());

        EConditionResult result = cond->getResult();

        switch(result) {
            case EConditionResult::True_Val:
                logger->debug(EVENT_MANAGER, "Comparison result: TRUE, add condition to list of valid conditions");
                validConditions.push_back(condId);
                break;
            case EConditionResult::False_Val:
                logger->debug(EVENT_MANAGER, "Comparison result: FALSE, skipping condition");
                break;
            case EConditionResult::DeviceParamUndefined:
                setEventToDoNotRemove();
                emit sendDeviceParameterRequest(cond->device->getDeviceId(), cond->param);
                cond->clearResult();
                logger->debug(EVENT_MANAGER, "Skipping condition because of undefined device parameter");
                break;
            case EConditionResult::DeviceInactive:
                logger->debug(EVENT_MANAGER, "Skipping condition because of inactive device");
                break;
            default:
                break;
        }

    }

    return validConditions;
}

QList<COperation*> CEventManager::getOperationsRelatedToConditions(QList<QString> conditions) {
    QList<COperation*> ops;

    for (QString condId : conditions) {
        if (!opContainer->cond2operationMap.contains(condId)) {
            logger->warning(EVENT_MANAGER, "Cond2OperationMap doesn\'t contain ID " + condId);
            continue;
        }

        QString opId = opContainer->cond2operationMap[condId];
        if (!opContainer->operationByIdMap.contains(opId)) {
            logger->warning(EVENT_MANAGER, "OperationByIdMap doesn\'t contain ID " + opId);
            continue;
        }

        ops.push_back(opContainer->operationByIdMap[opId]);
    }

    return ops;
}

void CEventManager::clearConditionsResult() {
    logger->debug(EVENT_MANAGER, "Clearing conditions result");
    for (QString condId : conditionsRelatedToEvent) {
        opContainer->conditionByIdMap[condId]->clearResult();
    }

    conditionsRelatedToEvent.clear();
}


void CEventManager::handleNormalOperation(COperation *operation) {
    logger->info(EVENT_MANAGER, "Normal operation handled: " + operation->name);
    if (checkConditionsInOperation(operation->getRegularConditions(), condTypeRegular, operation)) {
        executeAction(operation);
    }
}

void CEventManager::handleDelayedOperation(COperation *operation) {
    logger->info(EVENT_MANAGER, "Delayed operation handled: " + operation->name);
    if (checkConditionsInOperation(operation->getDelayedStopConditions(), condTypeStop, operation)) {
        removeDelayedAction(operation);
    }

    if (checkConditionsInOperation(operation->getDelayedStartConditions(), condTypeStart, operation)) {
        addDelayedAction(operation);
    }
}

bool CEventManager::checkConditionsInOperation(QSet<QString> conditionKeys, QString condType, COperation *operation) {
    logger->debug(EVENT_MANAGER, "Checking " + condType + " conditions of operation");
    conditionsRelatedToEvent.unite(conditionKeys);
    bool finalResult;

    QString conditionsExtLogic;

    if (condType == condTypeRegular || condType == condTypeStart) {
        conditionsExtLogic = operation->conditionsExtLogic;
    } else {
        conditionsExtLogic = operation->stopConditionsExtLogic;
    }

    if (conditionsExtLogic != "") {
        logger->low_info(EVENT_MANAGER, "Operation contains extended conditions logic: " + conditionsExtLogic);
        finalResult = checkExtendedLogicForConditions(conditionKeys, conditionsExtLogic);
    } else {
        finalResult = checkConditionsResult(conditionKeys);
    }
    
    if (!finalResult) {
        logger->debug(EVENT_MANAGER, "NOT all conditions are consistent");
        return false;
    }

    logger->debug(EVENT_MANAGER, "All conditions are consistent");
    return true;  

}

bool CEventManager::checkExtendedLogicForConditions(QSet<QString> conditionKeys, QString conditionsExtLogic) {

    LibBoolEE::Vals conditionResults;

    for (QString condId : conditionKeys) {
        if (!opContainer->conditionByIdMap.contains(condId)) {
            logger->warning(EVENT_MANAGER, "ConditionByIdMap doesn\'t contain ID " + condId);
            continue;
        }

        CCondition* cond = opContainer->conditionByIdMap[condId];
        logger->debug(EVENT_MANAGER, "Checking condition: " + cond->toString());

        EConditionResult result = cond->getResult();
    
        switch(result) {
            case EConditionResult::True_Val:
                logger->debug(EVENT_MANAGER, "Comparison result: TRUE");
                conditionResults.insert(std::make_pair(cond->extLogicConditionId.toStdString().c_str(), true));
                break;
            case EConditionResult::False_Val:
                logger->debug(EVENT_MANAGER, "Comparison result: FALSE");
                conditionResults.insert(std::make_pair(cond->extLogicConditionId.toStdString().c_str(), false));
                break;
            case EConditionResult::DeviceParamUndefined:
                logger->debug(EVENT_MANAGER, "Comparison result: Device parameter undefined");
                setEventToDoNotRemove();
                emit sendDeviceParameterRequest(cond->device->getDeviceId(), cond->param);
                cond->clearResult();
                return false;
            case EConditionResult::DeviceInactive:
                logger->debug(EVENT_MANAGER, "Comparison result: Device inactive");
                return false;
            default:
                break;
        }
    }

    try {
        return LibBoolEE::resolve(conditionsExtLogic.toStdString().c_str(), conditionResults);
    } catch (std::runtime_error ex) {
        logger->error(EVENT_MANAGER, "Calculating extended conditions logic exception: " + QString(ex.what()));
        return false;
    }
}


bool CEventManager::checkConditionsResult(QSet<QString> conditions) {
    bool finalResult = true;

    for (QString condId : conditions) {

        if (!opContainer->conditionByIdMap.contains(condId)) {
            logger->warning(EVENT_MANAGER, "ConditionByIdMap doesn\'t contain ID " + condId);
            continue;
        }

        CCondition* cond = opContainer->conditionByIdMap[condId];
        logger->debug(EVENT_MANAGER, "Checking condition: " + cond->toString());

        EConditionResult result = cond->getResult();

        switch(result) {
            case EConditionResult::True_Val:
                logger->debug(EVENT_MANAGER, "Comparison result: TRUE");
                break;
            case EConditionResult::False_Val:
                logger->debug(EVENT_MANAGER, "Comparison result: FALSE");
                finalResult = false;
                break;
            case EConditionResult::DeviceParamUndefined:
                logger->debug(EVENT_MANAGER, "Comparison result: Device parameter undefined");
                setEventToDoNotRemove();
                emit sendDeviceParameterRequest(cond->device->getDeviceId(), cond->param);
                cond->clearResult();
                finalResult = false;
                break;
            case EConditionResult::DeviceInactive:
                logger->debug(EVENT_MANAGER, "Comparison result: Device inactive");
                finalResult = false;
                break; 
            default:
                break;
        }
    }

    return finalResult;
}

void CEventManager::accomplishEvent() {
    if (eventToRemove) {
        removeFirstEvent();
    } else {
        moveFirstEventToEndOfTheQueue();
    }

    eventToRemove = true;
}

void CEventManager::handleEvent(CEvent* event) {
    QString eventKey = CTools::generateEventKey(event->getEventType(), event->device, event->parameter, event->value);
    printEventKey(eventKey);

    if (!opContainer->conditionsByEventKeyMap.contains(eventKey)) {
        logger->warning(EVENT_MANAGER, "ConditionsByEventKeyMap doesn\'t contain event key " + eventKey);
        return;
    }

    QSet<QString> conditions = opContainer->conditionsByEventKeyMap[eventKey];
    conditionsRelatedToEvent.unite(conditions);
    QList<QString> validConditions = validateConditions(conditions);
    QList<COperation*> operations = getOperationsRelatedToConditions(validConditions);

    logger->info(EVENT_MANAGER, "Checking operations related to the event");
    for (COperation* operation : operations) {
        if (operation->type == EOperationType::Normal) {
            handleNormalOperation(operation);
        } else if (operation->type == EOperationType::Delayed) {
            handleDelayedOperation(operation);
        }
    }

    clearChangingStatusOFDevice(event);
    clearConditionsResult();
}

void CEventManager::clearChangingStatusOFDevice(CEvent* event) {
    if (event->getEventType() == EEventType::DEVICE_EVENT && event->device != NULL) {
        event->device->clearChangingStatus(event->parameter);
    }
}