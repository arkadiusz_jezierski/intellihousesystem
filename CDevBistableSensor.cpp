#include "CDevBistableSensor.h"

CDevBistableSensor::CDevBistableSensor()
{
    this->setCategory(DevCat::BistableSwitchSensor);

    initParameters();
    initCommandMethods();
    initRequestMethods();
}

CDevBistableSensor::~CDevBistableSensor()
{

}


void CDevBistableSensor::initParameters(){
    Value val;
    val.undefined = true;
    val.value = 0;
    val.changes = EParamChanges::None;

    parameters[PARAM_STATE] = val;
}

void CDevBistableSensor::commandMethodOfBistableSwitchSensor(COMMAND cmd, CBuffer &data){
    if (methodsMap.contains(cmd)){
        try{
            (this->*methodsMap[cmd])(data);
        }catch(CException ex){
            logger->error(DEVICE_MANAGER, "Error in BistableSwitchSensor: " + ex.what());
        }
    }else{
        logger->warning(DEVICE_MANAGER, "No method to invoke for command " + QString::number((int)cmd));
    }
}

void CDevBistableSensor::requestParameterForBistableSwitchSensor(QString param){

    if (requestMethodsMap.contains(param)){
        try{
            (this->*requestMethodsMap[param])();
        }catch(CException ex){
            logger->error(DEVICE_MANAGER, "Error in BistableSwitchSensor: " + ex.what());
        }
    }else{
        logger->warning(DEVICE_MANAGER, "BistableSwitchSensor: No request to invoke for parameter " + param);
    }
}

void CDevBistableSensor::initCommandMethods(){
    methodsMap[CMD_CAN_GET_SENSOR_STATUS] = &CDevBistableSensor::getSensorStatus;
}

void CDevBistableSensor::initRequestMethods(){
    requestMethodsMap[PARAM_STATE] = &CDevBistableSensor::requestSensorState;
}

void CDevBistableSensor::getSensorStatus(CBuffer &data){
    logger->info(DEVICE_MANAGER, this->objectName() + "->getSensorStatus(" + QString::number(data[OFFSET_CAN_DATA]) + ")");

    Value val;
    val.undefined = false;
    val.value = data[OFFSET_CAN_DATA];
    val.changes = checkValueChanged(PARAM_STATE, val.value);

    parameters[PARAM_STATE] = val;
    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_STATE, val.value);
}


void CDevBistableSensor::requestSensorState(){
    logger->info(DEVICE_MANAGER, this->objectName() + "->requestSensorState()");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_GET_SENSOR_STATUS);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
}
