#ifndef COPERATIONCONTAINER_H
#define COPERATIONCONTAINER_H

#include <QObject>
#include <QMap>
#include <COperation.h>
#include <CCondition.h>
#include <CException.h>
#include <CLogger.h>
#include <CSingleton.h>
#include <QList>
#include <CCanDevice.h>
#include <CDevCondition.h>
#include <HTypes.h>
#include <CTools.h>


class COperationContainer : public QObject
{
    Q_OBJECT

    CLogger* logger;

    void printOperation(COperation* operation);

public:
    explicit COperationContainer(QObject *parent = 0);
    ~COperationContainer();

   	QMap<QString, CCondition*> conditionByIdMap;
   	QMap<QString, QSet<QString>> conditionsByEventKeyMap;

	  QMap<QString, COperation*> operationByIdMap;
   	QMap<QString, QString> cond2operationMap;
    QList<qint64> timeConditionValues;


   	void addConditionWithOperation(CCondition* condition, COperation* operation);
   	void addConditionsWithOperation(QList<CCondition*> condition, COperation* operation);


   	void clearContainer();

   	void printOperationList();

};

#endif // COPERATIONCONTAINER_H
