#include "CGsmModem.h"

CGsmModem::CGsmModem()
{
    logger= &CSingleton<CLogger>::Instance();
    config = &CSingleton<CConfig>::Instance();
    enabled = (config->getConfigParam(GSM_MODULE).toInt() == 1);

}

CGsmModem::~CGsmModem()
{
    qDebug("CGsmModem destructor");
}

CGsmModem::EOpenPortResult CGsmModem::open(){
    logger->info(GSM_MODEM, "Trying to open port for Gsm Modem");
    CSerialPortManager *portManager = &CSingleton<CSerialPortManager>::Instance();
    portManager->releasePort(portName);

    QString port = findPort();
    if (port != ""){
        logger->success(GSM_MODEM, "Serial port was found: " + port);
        try{
            configForEchoDisabled();
            //configForOnlineMode();
            configForCharsetGSM();
            configForTextMessageSending();
            configForPrintFullErrorMessages();
            return EOpenPortResult::OK;
        }catch(CException ex){
            logger->error(GSM_MODEM, "Error during configuring Gsm Modem: " + ex.what());
            return EOpenPortResult::CONFIG_ERROR;
        }
    }else{
        logger->warning(GSM_MODEM, "No device was found");
    }
    return EOpenPortResult::PORT_NOT_FOUND;
}



void CGsmModem::openPort(QString portName){
    int res = openSerial(portName, 38400);
    logger->low_info(GSM_MODEM, "Opening port result: " + QString::number(res));
    if (res){
        throw CException(QString("Opening port result code ") + QString::number(res), this);
    }
}

void CGsmModem::closeDevicePort(){
    closePort();
    logger->low_info(GSM_MODEM, "Closing port");

}

QString CGsmModem::findPort(){
    logger = &CSingleton<CLogger>::Instance();
    logger->info(GSM_MODEM, "Searching available serial ports");
    CSerialPortManager *portManager = &CSingleton<CSerialPortManager>::Instance();

    for (QString portName : portManager->getAvailablePorts()){

        logger->low_info(GSM_MODEM, "Checking port: " + portName);
        try{
            openPort(portName);
            if (handshake()){
                this->portName = portName;
                portManager->setPortBusy(portName);
                return portName;
            }
        }catch(CException e){
            logger->error(GSM_MODEM, "Opening port exception: " + e.what());
            closeDevicePort();
        }
    }

    return "";
}

bool CGsmModem::handshake(){
    logger->debug(GSM_MODEM, "Handshake function");

    CModemBuffer buffer;

    sendHandshakeCommand();
    buffer = getFrame();

    if (buffer.isOkResponse()) {
        logger->success(GSM_MODEM, "Handshake response received, received msg: " + buffer.getPrintBuffer(true));
        return true;
    }else{
        logger->error(GSM_MODEM, "No handshake response received, received msg: " + buffer.getPrintBuffer(true));
    }
    return false;

}

CModemBuffer CGsmModem::getFrame(){

    CModemBuffer tmpBuffer;

    unsigned char byte;

    CTimeOut tout;
    tout.startTimeOut(REQUEST_MODEM_TOUT);
    while (!tout.isTimeOut()) {
        try{
            byte = receiveByte(500);
            tmpBuffer << (unsigned char) byte;
        }catch(CException e){
            e.what();
            switch (tmpBuffer.isBufferReady()) {
            case CModemBuffer::EFrameResponeType::EMPTY:
                throw CException (QString("No data received"), this);
                break;
            case CModemBuffer::EFrameResponeType::INVALID:
                throw CException (QString("Invalid data received: ") + tmpBuffer.getPrintBuffer(true), this);
                break;
            case CModemBuffer::EFrameResponeType::OK:
                logger->debug(GSM_MODEM, "Data received: " + tmpBuffer.getResponse().toString());
                return tmpBuffer.getResponse();
                break;
            case CModemBuffer::EFrameResponeType::ERROR:
                throw CErrorException (QString("Error response received: ") + tmpBuffer.getResponse().toString(), this);
                break;
            }
        }
    }

    throw CException (QString("Timeout during receiving data: ") + tmpBuffer.getPrintBuffer(true), this);

}



void CGsmModem::getIncomingData(){

    CModemBuffer tmpBuffer;

    unsigned char byte;

    CTimeOut tout;
    tout.startTimeOut(RECEIVE_MSG_TOUT);
    while (!tout.isTimeOut()) {
        try{
            byte = receiveByte(100);
            tmpBuffer << (unsigned char) byte;
        }catch(CException e){
            e.what();
        }

    }

    if (!tmpBuffer.isEmpty()) {
        logger->info(GSM_MODEM, "Unexpected data received: " + tmpBuffer.toString());        
    }

}


void CGsmModem::sendHandshakeCommand(){
    logger->debug(GSM_MODEM, "Sending handshake command");

    CModemBuffer tmpBuffer;
    tmpBuffer << CMD_AT;
    tmpBuffer << (unsigned char) CODE_CR;
    sendBuffer(tmpBuffer);

}

void CGsmModem::setParameter(QString cmd, QString value){

    logger->debug(GSM_MODEM, "Setting parameter " + cmd + "=" + value);

    CModemBuffer tmpBuffer;
    tmpBuffer << CMD_AT;
    tmpBuffer << "+";
    tmpBuffer << cmd;
    tmpBuffer << "=";
    tmpBuffer << value;
    tmpBuffer << (unsigned char) CODE_CR;
    sendBuffer(tmpBuffer);
}


void CGsmModem::getParameter(QString cmd){

    logger->debug(GSM_MODEM, "Getting parameter " + cmd);

    CModemBuffer tmpBuffer;
    tmpBuffer << CMD_AT;
    tmpBuffer << "+";
    tmpBuffer << cmd;
    tmpBuffer << "=?";
    tmpBuffer << (unsigned char) CODE_CR;
    sendBuffer(tmpBuffer);
    getFrame();
   
}

void CGsmModem::sendCommand(QString cmd){

    logger->debug(GSM_MODEM, "Sending command " + cmd);

    CModemBuffer tmpBuffer;
    tmpBuffer << cmd;
    tmpBuffer << (unsigned char) CODE_CR;
    sendBuffer(tmpBuffer);
}

void CGsmModem::sendMessage(QString number, QString msg){

    logger->debug(GSM_MODEM, "Sending message to " + number + ": " + msg);

    CModemBuffer tmpBuffer, sendingBuffer;
    tmpBuffer << CMD_AT;
    tmpBuffer << "+";
    tmpBuffer << CMD_SEND_MESSAGE;
    tmpBuffer << "=\"";
    tmpBuffer << number;
    tmpBuffer << "\"";
    tmpBuffer << (unsigned char) CODE_CR;
    sendBuffer(tmpBuffer);

    try{
        tmpBuffer = getFrame();
    }catch (CErrorException e) {
        logger->error(GSM_MODEM, "Error during setting destination number for outcoming message: " + e.what());
        throw CException(QString("Modem needs to be restarted"));
    }catch(CException e){
        logger->error(GSM_MODEM, "Error during setting destination number for outcoming message: " + e.what());
    }

 
    sendingBuffer << msg;
    sendingBuffer << (unsigned char) CODE_CTRL_Z;
    sendBuffer(sendingBuffer);

    checkSendingMessageResponse();
}

void CGsmModem::configForTextMessageSending() {
    logger->debug(GSM_MODEM, "Configure Gsm Modem for sending messages in text mode");

    CModemBuffer buffer;

    setParameter(CMD_MESSAGE_FORMAT, VAL_MSG_FORMAT_TEXT_MODE);
    buffer = getFrame();

    if (buffer.isOkResponse()) {
        logger->success(GSM_MODEM, "Configuration OK");
    }else{
        throw CException(QString("Problem with set modem into sending messages in text mode, received data: ")
                         + buffer.getPrintBuffer(true));
    }
}

void CGsmModem::configForOnlineMode() {
    logger->debug(GSM_MODEM, "Configure Gsm Modem for online mode");

    CModemBuffer buffer;

    setParameter(CMD_OPERATION_MODE, VAL_OPERATION_MODE_ONLINE);
    buffer = getFrame();

    if (buffer.isOkResponse()) {
        logger->success(GSM_MODEM, "Configuration OK");
    }else{
        throw CException(QString("Problem with set modem into online mode, received data: ")
                         + buffer.getPrintBuffer(true));
    }
}

//void CGsmModem::resetModem() {
//    logger->debug(GSM_MODEM, "Reset Gsm Modem");

//    CModemBuffer buffer;

//    try{
//        setParameter(CMD_OPERATION_MODE, VAL_OPERATION_MODE_OFFLINE);
//        buffer = getFrame();

//        if (buffer.isOkResponse()) {
//            logger->info(GSM_MODEM, "Modem set into offline mode");
//            setParameter(CMD_OPERATION_MODE, VAL_OPERATION_MODE_RESET);
//            buffer = getFrame();
//            if (buffer.isOkResponse()) {
//                logger->info(GSM_MODEM, "Modem will be restarted, waiting 10 sec...");
//                QThread::sleep(10);
//                logger->success(GSM_MODEM, "Modem was restarted");
//            }else{
//                throw CException(QString("Problem with reset modem, received data: ")
//                                 + buffer.getPrintBuffer(true));
//            }
//        }else{
//            throw CException(QString("Problem with reset modem, received data: ")
//                             + buffer.getPrintBuffer(true));
//        }
//    } catch(CException ex){
//        logger->error(GSM_MODEM, "Exception during reset modem: " + ex.what());
//    }
//}


void CGsmModem::resetModem() {
    logger->debug(GSM_MODEM, "Reseting Gsm Modem");

    CModemBuffer buffer;

    try{
        sendCommand(CMD_RESTORE_SETTINGS);
        buffer = getFrame();

        if (buffer.isOkResponse()) {
            logger->info(GSM_MODEM, "Modem has restored default settings");
            configForEchoDisabled();
        }else{
            throw CException(QString("Problem with reseting modem, received data: ")
                             + buffer.getPrintBuffer(true));
        }
    } catch(CException ex){
        logger->error(GSM_MODEM, "Exception during reseting modem: " + ex.what());
    }
}



void CGsmModem::configForEchoDisabled() {
    logger->debug(GSM_MODEM, "Configuring Gsm Modem for echo disabled");

    CModemBuffer buffer;

    sendCommand(QString(CMD_LOCAL_ECHO) + QString(VAL_ECHO_OFF));
    buffer = getFrame();  // received echo of sent command

    if (buffer.isOkResponse()) {
        logger->success(GSM_MODEM, "Configuration OK");
    }else{
        throw CException(QString("Problem with setting modem for echo disabled, received data: ")
                         + buffer.getPrintBuffer(true));
    }
}

void CGsmModem::configForPrintFullErrorMessages() {
    logger->debug(GSM_MODEM, "Configuring Gsm Modem for print full error messages");

    CModemBuffer buffer;

    setParameter(CMD_ERROR_FORMAT, VAL_ERR_MODE_ERROR_MESSAGE);
    buffer = getFrame();

    if (buffer.isOkResponse()) {
        logger->success(GSM_MODEM, "Configuration OK");
    }else{
        throw CException(QString("Problem with setting modem for printing full error messages, received data: ")
                         + buffer.getPrintBuffer(true));
    }
}


void CGsmModem::configForCharsetGSM() {
    logger->debug(GSM_MODEM, "Configuring Gsm Modem for setting GSM charset");

    CModemBuffer buffer;

    setParameter(CMD_SET_CHARSET, QString("\"") + VAL_CHARSET_GSM + QString("\""));
    buffer = getFrame();

    if (buffer.isOkResponse()) {
        logger->success(GSM_MODEM, "Configuration OK");
    }else{
        throw CException(QString("Problem with setting charset, received data: ")
                         + buffer.getPrintBuffer(true));
    }
}


void CGsmModem::checkNetworkStatus(){

    logger->debug(GSM_MODEM, "Checking network status");

    getParameter(CMD_NETWORK_REG_STATUS);

}


void CGsmModem::getMessagesList(){

    logger->debug(GSM_MODEM, "Checking messages list");

    setParameter(CMD_GET_MSG_LIST, QString("\"") + VAL_LIST_ALL_MESSAGES + QString("\""));
    getFrame();

}

void CGsmModem::checkSendingMessageResponse() {
    logger->debug(GSM_MODEM, "Waiting for sending message response");

    CTimeOut tout;
    CModemBuffer buffer;

    tout.startTimeOut(SEND_MESSAGE_RESPONSE_TOUT);

    while(!tout.isTimeOut()){
        QThread::msleep(1000);

        try{
            buffer = getFrame();
        }catch(CException e){
            logger->warning(GSM_MODEM, "Waiting for sending message response: " + e.what());
            continue;
        }

        if (buffer.isSendMessageResponse()) {
            messagesList.removeFirst();
            return;
        }
    }


    logger->error(GSM_MODEM, "Sending message response not received");
}

void CGsmModem::runGsmModuleService(){
    logger->info(GSM_MODEM, "Starting Gsm Module Service");

    runThread = true;
    EOpenPortResult openResult;

    if (enabled){
        do{
            openResult = this->open();
            if (openResult == EOpenPortResult::CONFIG_ERROR){
                resetModem();
            }

            QThread::msleep(1000);
        } while(openResult != EOpenPortResult::OK && runThread);
    } else {
        logger->info(GSM_MODEM, "Gsm Module disabled");
        return;
    }

    CTimeOut handshakeTout, networkTout;
    handshakeTout.startTimeOut(5000);
    networkTout.startTimeOut(60000);

    while(runThread){
        if (isOpen()) {
            try{
                sendMessageFromQueue();

                if (handshakeTout.isTimeOut()){
                    handshake();
                    handshakeTout.startTimeOut(5000);
                }

                if (networkTout.isTimeOut()){
                    checkNetworkStatus();
                    networkTout.startTimeOut(60000);
                }

                getIncomingData();
                QThread::msleep(100);
            }catch(CException ex){
                logger->error(GSM_MODEM, "Exception: " + ex.what());
                resetModem();
                closeDevicePort();
                QThread::msleep(1000);
            }
        } else {
            openResult = this->open();
            if (openResult == EOpenPortResult::CONFIG_ERROR){
                resetModem();
            }
            QThread::msleep(3000);
        }

    }
    logger->debug(GSM_MODEM, "Gsm Module Service finished");
}

void CGsmModem::addNewMessage(QString phoneNumber, QString message){
    logger->info(GSM_MODEM, "New message created <" + phoneNumber + ">: [" + message + "]");

    MessageValidationResult result = validateMessage(phoneNumber, message);

    if (result & MESSAGE_CONTAINS_FORBIDDEN_SIGNS) {
        logger->error(GSM_MODEM, "Message contains forbidden signs and will be refused");
    }

    if (result & MESSAGE_TOO_LONG) {
        logger->error(GSM_MODEM, "Message is too long and will be refused");
    }

    if (result & MESSAGE_INVALID_PHONE_NUMBER) {
        logger->error(GSM_MODEM, "Invalid phone number, message will be refused");
    }

    if (result == MESSAGE_OK) {
        SShortMessage msg;
        msg.message = message;
        msg.phoneNumber = phoneNumber;

        messagesList.push_back(msg);
    }


}

void CGsmModem::sendMessageFromQueue(){
    SShortMessage message;

    if (messagesList.size() > 0){
        message = messagesList.at(0);
        sendMessage(message.phoneNumber, message.message);
    }
}


MessageValidationResult CGsmModem::validateMessage(QString phoneNumber, QString message) {
    MessageValidationResult result = MESSAGE_OK;

    QRegExp rx("(\\+\\d{11})");
    QList<QChar> forbiddenSigns = {'^', '{', '}', '[', ']', '\\', '|', '`'};

    if (!rx.exactMatch(phoneNumber)){
        result |= MESSAGE_INVALID_PHONE_NUMBER;
    }

    if (message.length() > MAX_MESSAGE_LENGTH) {
        result |= MESSAGE_TOO_LONG;
    }

    for (int i = 0; i < message.length(); i++) {
        if (message.at(i).unicode() < 0x20
                || message.at(i).unicode() > 0x7d
                || forbiddenSigns.contains(message.at(i))) {
            result |= MESSAGE_CONTAINS_FORBIDDEN_SIGNS;
            break;
        }
    }

    return result;

}

void CGsmModem::killThread(){
    runThread = false;
}

