#ifndef CLOGGER_H
#define CLOGGER_H

#include <QObject>
#include <QDebug>
#include <QDateTime>
#include <syslog.h>
#include <QMutex>

#define EVENT_MANAGER       "EventManager"
#define DEVICE_MANAGER      "DeviceManager"
#define TRANSCEIVER         "Transceiver"
#define SYSTEM              "System"
#define UPLOAD_FW           "FirmwareUploading"
#define WEBSERVICE          "WebService"
#define GSM_MODEM           "GsmModem"
#define ALARM_MODULE        "AlarmModule"
#define REST_SERVICE        "RestService"
#define LOGPOSTER           "LoggerPoster"
#define REST_CLIENT         "RestClient"

class CLogger : public QObject
{
    Q_OBJECT

    QString getTime();

    void log2syslog(QString msg);
    void log2syslogOnly(QString msg);
    void log2syslogAndSentToExtSystem(QString msg);
    mutable QMutex loggerMutex;

    QList<QString> filters;


public:

    void info(QString msg);
    void low_info(QString msg);

    void warning(QString msg);
    void error(QString msg);
    void success(QString msg);
    void debug(QString msg);
    void posterLoggerLog(QString msg);

    void info(QString source, QString msg);
    void low_info(QString source, QString msg);

    void warning(QString source, QString msg);
    void error(QString source, QString msg);
    void success(QString source, QString msg);
    void debug(QString source, QString msg);

    void setFilters(QList<QString> filters);

    explicit CLogger(QObject *parent = 0);
    ~CLogger();

signals:
    void postLog(QString);

};

#endif // CLOGGER_H
