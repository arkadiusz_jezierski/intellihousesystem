#ifndef CMAINAPP_H
#define CMAINAPP_H

#include <QObject>
#include <CSingleton.h>
#include <CLogger.h>
#include <CConfig.h>
#include <CException.h>
#include <CTransceiver.h>
#include <CGsmModem.h>
#include <QThread>
#include <CCan232.h>
#include <CCanBuffer.h>
#include "CRestApiService.h"

#include <CEventManager.h>
#include <CDeviceManager.h>


#include <COperationListLoader.h>
#include <COperation.h>
#include <CEvent.h>
#include <CLoggerPoster.h>
#include <CRestClientService.h>

class CMainApp : public QObject
{
    Q_OBJECT

    CLogger *logger = NULL;
    CConfig *config = NULL;
    CTransceiver *transceiver = NULL;
    CGsmModem *gsmModem = NULL;
    CLoggerPoster *loggerPoster = NULL;
    CRestClientService *restClient = NULL;
    CDeviceManager *deviceManager = NULL;
    CEventManager *eventManager = NULL;
    CRestApiService *restService = NULL;
    QFuture<void> webServiceThread;
    QFuture<void> mainThread;
    QFuture<void> deviceManagerThread;
    QFuture<void> eventsThread;
    QFuture<void> gsmModuleThread;
    QFuture<void> restServiceThread;
    QFuture<void> loggerPosterThread;
    QFuture<void> restClientServiceThread;

    bool runMainThread = true;
    bool pausedThread = false;

    void pollTransceiver();
//    void pollDeviceManager();

    void dispatchCanMessage(const CCanBuffer& message);

    void errorMessageHandler(const CCanBuffer& buffer);
    void devStartingUpHandler(const CCanBuffer& buffer);
    void assignEventManager(QList<CCanDevice*> devicesList, CEventManager *eventManager);

    void runThreads();
    void runPausedAndStoppedThreads();
    bool stopThreads();

public:
    void quit();
    void run();

    void assignTransceiver(CTransceiver *transceiver);

    explicit CMainApp(QObject *parent = 0);
    ~CMainApp();

signals:
    void killThreads();
    void pauseStopThreads();
    void unpauseThreads();

public slots:
    void pauseThread();
    void resumeThread();
    void removeDevice(UID uid);
    void removeDevice(DevCat category, int addressBegin, int addressEnd);

};

#endif // CMAINAPP_H
