#include "CRestAction.h"

CRestAction::CRestAction(QString endpoint, QString message) : CAction()
{
    this->endpoint = endpoint;
    this->message = message;
    this->actionType = EActionType::REST_ACTION;
}

CRestAction::~CRestAction()
{

}

QString CRestAction::toString() {
    QString str = "Rest Action [SEND TO " + this->endpoint + " MESSAGE (" + this->message + ")]";

    return str;
}

