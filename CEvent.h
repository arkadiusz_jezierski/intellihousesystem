#ifndef CEVENT_H
#define CEVENT_H

#include "CCanDevice.h"
#include "HTypes.h"
#include <QObject>
#include <QDateTime>
#include <QtGlobal>
#include <CTools.h>

/**
 * @brief The CEvent class
 * This class includes a definition of single event. It means that
 * it consists pointer to device and description of signle event
 * (parameter and its value).
 */
class CEvent
{


protected:

    EEventType eventType;

public:

    // CEvent();

    CEvent(CCanDevice *device, QString param, qint64 val);
    CEvent(qint64 val, EEventType type);
    CEvent(EEventType type);
//    CEvent(CCanDevice *device, QString param, ELogicalConditionType condition, qint64 val);
//    CEvent(const CEvent &event);
    ~CEvent();

    CCanDevice* device;
    QString parameter;
    qint64 value;

    EEventType getEventType();
    QString toString();


};

#endif // CEVENT_H
