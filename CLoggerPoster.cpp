#include "CLoggerPoster.h"

CLoggerPoster::CLoggerPoster(QObject *parent) : QObject(parent)
{
	logger= &CSingleton<CLogger>::Instance();
	config = &CSingleton<CConfig>::Instance();

	logDestinationUrl = config->getConfigParam(LOG_SERVER_URL);

	loadFilters();

	manager = new QNetworkAccessManager(this);

	QObject::connect(manager, SIGNAL(finished(QNetworkReply*)),
        this, SLOT(onPostAnswer(QNetworkReply*)));
}

CLoggerPoster::~CLoggerPoster()
{
	if (manager != NULL) {
		delete manager;
		manager = NULL;
	}
}


void CLoggerPoster::killThread() {
    runThread = false;
}

void CLoggerPoster::runLoggerPoster() {
    logger->info(EVENT_MANAGER, "Starting LoggerPoster");
    runThread = true;
    while(runThread) {
       
        QThread::msleep(1);
    }
    logger->debug(EVENT_MANAGER, "LoggerPoster thread finished");
}

void CLoggerPoster::postLog(QString logData) {

  
    QNetworkRequest request;
	request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setUrl(QUrl(logDestinationUrl));


	logData = logData.replace("\"", "'");
	QString str("{\"log\":\"" + logData + "\"}");
	QByteArray postData = str.toUtf8();

    manager->post(request,postData);
}

void  CLoggerPoster::onPostAnswer(QNetworkReply* reply)
{

    if (reply->error()) {
        logger->posterLoggerLog("Error during sending post message: " + reply->errorString());
    } else {
    	QJsonDocument json = QJsonDocument::fromJson(reply->readAll());
        QJsonObject jsonResponse = json.object();
      
    	bool success = jsonResponse[RESPONSE_SUCCESS].toBool();
    	QString msg = jsonResponse[RESPONSE_MSG].toString();

    	if (success) {
        	logger->posterLoggerLog("Log successfully posted: " + msg);
    	} else {
        	logger->posterLoggerLog("Invalid post sent: " + msg);
    	}
    } 

    delete reply;
 
}

void CLoggerPoster::reloadFiltersList() {
	loadFilters();
}

QString CLoggerPoster::loadFromFile() {
    CConfig *config = &CSingleton<CConfig>::Instance();
    QFile file(config->getConfigParam(LOG_FILTERS_PATH));
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QString val = file.readAll();
    file.close();
    return val;
}

bool CLoggerPoster::saveJsonToFile(QString json) {
    CConfig *config = &CSingleton<CConfig>::Instance();
    QFile file(config->getConfigParam(LOG_FILTERS_PATH));
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        return false;
    }
    QTextStream out(&file);
    qDebug()<<"json"<<json;
    out << json;
    file.close();
    return true;
}


void CLoggerPoster::loadFilters() {
	/*QFile file("settings/logger_filters.json");
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QString val = file.readAll();
    file.close();
*/
    QString val = loadFromFile();

    QJsonDocument json = QJsonDocument::fromJson(val.toUtf8());
    QJsonObject mainObject = json.object();

    QJsonArray strFilters = mainObject[FILTERS].toArray();

    filters.clear();
    foreach (const QJsonValue & value, strFilters) {
        QString filter = value.toString();
        filters.push_back(filter);
    }

    logger->posterLoggerLog("Logs filters:");
	for (QString filter : filters) {
        logger->posterLoggerLog(filter);
    }

    logger->posterLoggerLog("Logger filters loading success");

    logger->setFilters(filters);
}