#ifndef CGSMMODEMACTION_H
#define CGSMMODEMACTION_H

#include <CAction.h>
#include "HTypes.h"
#include <QDateTime>
#include <QtGlobal>

class CGsmModemAction : public CAction
{
public:
    CGsmModemAction(QString phoneNumber, QString message);
    ~CGsmModemAction();

    QString toString();

    QString phoneNumber;
    QString message;
};

#endif // CGSMMODEMACTION_H
