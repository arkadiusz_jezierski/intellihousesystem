#ifndef CDEVICEACTION_H
#define CDEVICEACTION_H

#include "CCanDevice.h"
#include "CAction.h"
#include "HTypes.h"
#include <QDateTime>
#include <QtGlobal>

class CDeviceAction : public CAction
{

public:

    CDeviceAction(CCanDevice *device, QString param, qint64 val);

    ~CDeviceAction();

    CCanDevice *device;
    QString parameter;
    qint64 value;


    QString toString();
};

#endif // CDEVICEACTION_H
