#include "CTransceiver.h"

CTransceiver::CTransceiver()
{
    logger= &CSingleton<CLogger>::Instance();
}

CTransceiver::~CTransceiver(){
    qDebug("CTransceiver destructor");
}

bool CTransceiver::open(){
    logger->info(TRANSCEIVER, "Trying to open port for TRANSCEIVER");

    CSerialPortManager *portManager = &CSingleton<CSerialPortManager>::Instance();
    portManager->releasePort(portName);

    QString port = findPort();
    if (port != ""){
        logger->success(TRANSCEIVER, "Serial port was found: " + port);
        try{
            configForCanB();
            return true;
        }catch(CException ex){
            logger->error(TRANSCEIVER, "Error during config transceiver: " + ex.what());
            return false;
        }
    }else{
        logger->warning(TRANSCEIVER, "No device was found");
    }
    return false;
}



void CTransceiver::openPort(QString portName){
    portName = "";
}

QString CTransceiver::findPort(){
    return "";
}

CCanBuffer CTransceiver::getExCanMessage(){
    CCanBuffer buf;
    return buf;
}

void CTransceiver::sendExCanFrame(const CBuffer &buffer){
    buffer.size();
}

void CTransceiver::setCanMode(const EEcanMode mode){
    qDebug()<<(int)mode;
}

void CTransceiver::sendCanRegularFrame(const CBuffer &can){
    CBuffer x = can;
}
CCanBuffer CTransceiver::getRegularCanFrame(){
    CCanBuffer buf;
    return buf;
}
CCanBuffer CTransceiver::sendRegularFrameWithResponse(const CBuffer &can){
    CBuffer x = can;
    CCanBuffer buf;
    return buf;
}

void CTransceiver::configForCanA(){

}

void CTransceiver::configForCanB(){

}
