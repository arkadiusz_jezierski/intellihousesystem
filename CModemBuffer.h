#ifndef CMODEMBUFFER_H
#define CMODEMBUFFER_H

#include <CBuffer.h>

#define CODE_LF         0x0a
#define CODE_CR         0x0d
#define CODE_CTRL_Z     0x1a

#define CMD_AT                      "AT"
#define CMD_MESSAGE_FORMAT          "CMGF"
#define CMD_ERROR_FORMAT            "CMEE"
#define CMD_SEND_MESSAGE            "CMGS"
#define CMD_OPERATION_MODE          "CFUN"
#define CMD_ERROR_RESPONSE          "CME"
#define CMD_RESTORE_SETTINGS        "ATZ"
#define CMD_LOCAL_ECHO              "ATE"
#define CMD_NETWORK_REG_STATUS      "CREG"
#define CMD_SET_CHARSET             "CSCS"
#define CMD_GET_MSG_LIST            "CMGL"
#define CMD_DELETE_MESSAGE          "CMGD"

#define RESP_OK                     "OK"
#define RESP_ERROR                  "ERROR"
#define RESP_SEND_MSG               "+CMGS:"

#define VAL_OPERATION_MODE_ONLINE   "1"
#define VAL_OPERATION_MODE_OFFLINE  "4"
#define VAL_OPERATION_MODE_RESET    "16"

#define VAL_ERR_MODE_ONLY_ERROR     "0"
#define VAL_ERR_MODE_ERROR_CODE     "1"
#define VAL_ERR_MODE_ERROR_MESSAGE  "2"

#define VAL_MSG_FORMAT_PDU_MODE     "0"
#define VAL_MSG_FORMAT_TEXT_MODE    "1"

#define VAL_ECHO_OFF                "0"
#define VAL_ECHO_ON                 "1"

#define VAL_CHARSET_GSM             "GSM"

#define VAL_LIST_ALL_MESSAGES       "ALL"
#define VAL_LIST_REC_UNREAD_MESSAGES    "REC UNREAD"
#define VAL_LIST_REC_READ_MESSAGES      "REC READ"
#define VAL_LIST_STO_UNSENT_MESSAGES    "STO UNSENT"
#define VAL_LIST_STO_SENT_MESSAGES  "STO SENT"

#define VAL_DEL_MSG_INDEX           "0"
#define VAL_DEL_ALL_READ_MSG        "1"
#define VAL_DEL_ALL_AND_SENT_MSG    "2"
#define VAL_DEL_ALL_SENT_UNSENT_MSG "3"
#define VAL_DEL_ALL_MSG             "4"


class CModemBuffer : public CBuffer
{


public:
    CModemBuffer();
    CModemBuffer (const CModemBuffer & buf);
    CModemBuffer (const CBuffer &) : CBuffer() {}
    ~CModemBuffer();

    enum class EFrameResponeType{
        INVALID,
        EMPTY,
        OK,
        ERROR
    };


    EFrameResponeType isBufferReady();
    bool isOkResponse();
    bool isSendMessageResponse();
    bool isEmpty();

    QString toString();
    CModemBuffer getResponse();
};

#endif // CMODEMBUFFER_H
