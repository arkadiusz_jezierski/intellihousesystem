#ifndef CDELAYACTION_H
#define CDELAYACTION_H

#include <QObject>
#include <CAction.h>

/**
 * @brief The CDelayAction class
 * This class includes description of delayed action.
 */
class CDelayAction : public QObject
{
    Q_OBJECT

    QDateTime executionTime;
    CAction *action;
public:
    explicit CDelayAction(QObject *parent = 0);
    ~CDelayAction();

};

#endif // CDELAYACTION_H
