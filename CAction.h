#ifndef CACTION_H
#define CACTION_H


#include "CCanDevice.h"
#include "HTypes.h"
#include <QDateTime>
#include <QtGlobal>

/**
 * @brief The CAction class
 * Action class includes definition of single action. It means it
 * consists a list of conditions events for executing other events.
 */

class CAction
{

protected:

    EActionType actionType;

public:

    CAction();

    virtual ~CAction();


    EActionType getActionType();

    QString actionId;

    virtual QString toString(){
    	return QString("");
    }


};

#endif // CACTION_H
