#include "CException.h"

//CException::CException(const char * msg){
//    message = msg;
//    objName = new char(1);
//    objName[0] = "";
//}

//CException::CException(const char * msg, QObject* obj){
//    message = msg;
//    objName = obj->metaObject()->className();
//}

CException::CException(QString msg) {
    strMessage = msg;
}

CException::CException(QString msg, const QObject* obj){
    strMessage = msg;
    objName = QString(obj->metaObject()->className());
}


QString CException::what(){
    QString output = "";
    if (strMessage != "") {
        output = strMessage;
    }

    if (objName != ""){
        return output + " [in object " + objName + "]";
    }else{
        return output;
    }
}
