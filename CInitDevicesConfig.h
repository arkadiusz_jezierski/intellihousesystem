#ifndef CINITDEVICESCONFIG_H
#define CINITDEVICESCONFIG_H

#include <QObject>
#include <QCoreApplication>
#include <CSingleton.h>
#include <CLogger.h>
#include <QDebug>
#include <QFile>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <CException.h>
#include "HTypes.h"
#include "CDeviceConfig.h"

#define DEV_CONFIG	                "devices_config"
#define DEV_CONFIG_CATEGORY         "category"
#define DEV_CONFIG_ADDRESS          "address"
#define DEV_CONFIG_PARAM            "parameter"
#define DEV_CONFIG_VALUE            "value"

class CInitDevicesConfig : public QObject
{
    Q_OBJECT

    CLogger *logger;
    void loadConfig();
    QString loadFromFile();
    QList<CDeviceConfig> devicesConfigList;

public:
    explicit CInitDevicesConfig(QObject *parent = 0);
    ~CInitDevicesConfig();

    QList<CDeviceConfig> getDevicesConfig();
    QList<CDeviceConfig> getConfigForDevice(DevCat category, unsigned char address);
  
	
};

#endif // CINITDEVICESCONFIG_H