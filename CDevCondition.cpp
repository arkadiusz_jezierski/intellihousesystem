#include "CDevCondition.h"

CDevCondition::CDevCondition()
{
	this->type = EConditionType::DEVICE;
}

CDevCondition::~CDevCondition()
{

}

CDevCondition::CDevCondition(CCanDevice *device, QString param, ELogicalConditionType condition, qint64 val) : CCondition(){
    this->logicalCondition = condition;
    this->value = val;
    this->device = device;
    this->param = param;
    this->type = EConditionType::DEVICE;
}

QString CDevCondition::toString() {
    QString str = "";
    if (this->extLogicConditionId != "") {
        str = this->extLogicConditionId + ": ";
    }
    str += "Device Condition [IF " + this->device->objectName() + " HAS " + this->param + " " +
    CCondition::conditionToString(this->logicalCondition) + " " + QString::number(this->value) + "]";

    return str;
}

void CDevCondition::calculateCondition() {

    if (this->logicalCondition == ELogicalConditionType::ANY) {
        if (device->parameters[this->param].changes != EParamChanges::None) {
            logger->debug(EVENT_MANAGER, "Value of devices parameter has changed, condition: ANY, comparison result: TRUE");
            this->result = EConditionResult::True_Val;
        } else {
            logger->debug(EVENT_MANAGER, "Value of devices parameter has not changed, condition: ANY, comparison result: FALSE");
            this->result = EConditionResult::False_Val;
        }
    } else if (this->logicalCondition == ELogicalConditionType::RISING) {
        if (device->parameters[this->param].changes == EParamChanges::Rising) {
            logger->debug(EVENT_MANAGER, "Value of devices parameter is rising, condition: RISING, comparison result: TRUE");
            this->result = EConditionResult::True_Val;
        }else{
            logger->debug(EVENT_MANAGER, "Value of devices parameter is not rising, condition: RISING, comparison result: FALSE");
            this->result = EConditionResult::False_Val;
        }
    } else if (this->logicalCondition == ELogicalConditionType::FALLING) {
        if (device->parameters[this->param].changes == EParamChanges::Falling) {
            logger->debug(EVENT_MANAGER, "Value of devices parameter is falling, condition: FALLING, comparison result: TRUE");
            this->result = EConditionResult::True_Val;
        }else{
            logger->debug(EVENT_MANAGER, "Value of devices parameter is not falling, condition: FALLING, comparison result: FALSE");
            this->result = EConditionResult::False_Val;
        }
    } else {
        if (compareParameterValues(device->parameters[this->param].value, this->value, this->logicalCondition)) {
            logger->debug(EVENT_MANAGER, "POSITIVE comparison, comparison result: TRUE");
            this->result = EConditionResult::True_Val;
        }else{
            logger->debug(EVENT_MANAGER, "NEGATIVE comparison, comparison result: FALSE");
            this->result = EConditionResult::False_Val;
        }
    }
}

bool CDevCondition::compareParameterValues(qint64 value1, qint64 value2, ELogicalConditionType condition) {
    return compareValues("device param value", value1, "expected param value", value2, condition);
}