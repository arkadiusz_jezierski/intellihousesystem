#ifndef CEVENTMANAGER_H
#define CEVENTMANAGER_H

#include <QObject>
#include <CEvent.h>
#include <CLogger.h>
#include <CSingleton.h>
#include <QThread>
#include <COperationListLoader.h>
#include <COperationContainer.h>
#include <COperation.h>
#include <CCanDevice.h>
#include <CDeviceManager.h>
#include <CAlarmModule.h>
#include <QMutex>
#include <CTools.h>
#include <QStringList>
#include <CRestClientService.h>
#include "libbool/LibBoolEE.h"

class CEventManager : public QObject
{

    Q_OBJECT

    mutable QMutex eventQueueMutex;

    struct SDelayedOperation{
        COperation *operation;
        qint64 executionTime;
    };

    CLogger *logger = NULL;
    CAlarmModule *alarmModule = NULL;
    COperationContainer *opContainer = NULL;

    const QString condTypeStart = "start";
    const QString condTypeStop = "stop";
    const QString condTypeRegular = "regular";


    bool realoadOperationsRequest = false;

    bool lastAlarmState;

    QList<CEvent*> eventsList;
    
    QMap<QString, SDelayedOperation> delayedOperationsList;
    bool runThread = true;
    bool pausedThread = false;

    CEvent* getEvent();
    void removeFirstEvent();
    void moveFirstEventToEndOfTheQueue();

    
    void executeAction(COperation *operation);
    void addDelayedAction(COperation *operation);
    void removeDelayedAction(COperation *operation);
    void checkDelayedActions();
    
    void setEventToDoNotRemove();

    CDevCondition *currentCondition;
    CEvent *currentEvent;

    bool eventToRemove;

    qint64 lastTimeValuesAndAlarmCheck = QDateTime::currentMSecsSinceEpoch();

    void checkOtherConditions();

    void addNewTimeEvent(qint64 value);
    void addNewAlarmEvent();

    void addEventToQueue(CEvent* event);

    void printEventKey(QString key);
    QList<QString> validateConditions(QSet<QString> conditions);
    QList<COperation*> getOperationsRelatedToConditions(QList<QString> conditions);

    void clearConditionsResult();

    void handleNormalOperation(COperation *operation);
    void handleDelayedOperation(COperation *operation);

    bool checkConditionsInOperation(QSet<QString>conditionKeys , QString label, COperation *operation);
    bool checkConditionsResult(QSet<QString> conditions);
    bool checkExtendedLogicForConditions(QSet<QString> conditionKeys,  QString conditionsExtLogic);

    void accomplishEvent();
    void handleEvent(CEvent* event);

    QSet<QString> conditionsRelatedToEvent;
    void clearChangingStatusOFDevice(CEvent* event);


public:
    CEventManager(QObject *parent = 0);
    ~CEventManager();

    void runEventManager();

//    void assignDeviceManager(CDeviceManager *devManager);


signals:
    void sendDeviceParameterRequest(QString deviceId, QString parameter);
    void sendDeviceAction(DevCat category, unsigned char address, QString parameter, qint64 value);
    void sendMessageSignal(QString number, QString message);
    void sendPostRequest(QString endpoint, QString message);

public slots:
    void addNewDeviceEvent(DevCat category, unsigned char address, QString parameter, qint64 value);
    void reloadOperationList();
    void killThread();
    void pauseThread();
    void resumeThread();
};

#endif // CEVENTMANAGER_H
