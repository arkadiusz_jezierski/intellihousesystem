#include "CRestHandler.h"

CRestHandler::CRestHandler()
{

}

void CRestHandler::onRequest(const Http::Request& request, Http::ResponseWriter response) {

        QString str = QString::fromStdString(request.body());
        CRestApiService *service = &CSingleton<CRestApiService>::Instance();
        service->requestDispatcher(str, response);

}
