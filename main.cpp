#include <QCoreApplication>
#include <CMainApp.h>
#include <csignal>

void handleShutDownSignal( int signalId );
void setShutDownSignal( int signalId );

CMainApp *application;



int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    setShutDownSignal( SIGINT  );   // shut down on ctrl-c
    setShutDownSignal( SIGTERM );   // shut down on killall

    application = new CMainApp;
    application->run();

    return a.exec();
}



void setShutDownSignal( int signalId )
{
  struct sigaction sa;
  sa.sa_flags = 0;
  sigemptyset(&sa.sa_mask);
  sa.sa_handler = handleShutDownSignal;
  if (sigaction(signalId, &sa, NULL) == -1)
  {
    perror("setting up termination signal");
    exit(1);
  }
}


void handleShutDownSignal( int /*signalId*/ )
{
  application->quit();
  if (application != NULL){
//    delete application;
    application = NULL;
  }
  QCoreApplication::exit(0);
}

