#include "COperationListLoader.h"

COperationListLoader::COperationListLoader(QObject *parent) : QObject(parent)
{

}

COperationListLoader::~COperationListLoader()
{

}

QString COperationListLoader::loadFromFile() {
    CConfig *config = &CSingleton<CConfig>::Instance();
    QFile file(config->getConfigParam(OPERATIONS_PATH));
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QString val = file.readAll();
    file.close();
    return val;
}

bool COperationListLoader::saveJsonToFile(QString json) {
    CConfig *config = &CSingleton<CConfig>::Instance();
    QFile file(config->getConfigParam(OPERATIONS_PATH));
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        return false;
    }
    QTextStream out(&file);
    out << json;
    file.close();
    return true;
}

void COperationListLoader::loadOperations() {
    CLogger *logger = &CSingleton<CLogger>::Instance();
    COperationContainer *opContainer =  &CSingleton<COperationContainer>::Instance();
    opContainer->clearContainer();
    
    COperation *operation;
    CCanDevice *device;
    CAction *action;
    CDevCondition *condition;
    QString type;
    CDeviceManager *deviceManager = &CSingleton<CDeviceManager>::Instance();

    try{
        QJsonDocument json = QJsonDocument::fromJson(loadFromFile().toUtf8());
        QJsonObject mainObject = json.object();
        QJsonArray operations = mainObject["Operations"].toArray();
        bool allDevicesExists;

        for (int i = 0; i < operations.size(); i++) {
            
            QJsonObject opObject = operations[i].toObject();
            if (opObject["active"].toString() != "1") {
                continue;
            }
            operation = new COperation;
            QMap<QString, CCondition*> conditionsList;
            allDevicesExists = true;

            operation->name = opObject["name"].toString();
            operation->conditionsExtLogic = opObject["extCondLogic"].toString();
                       // qDebug()<<"Load operation  "<<operation->name;
            type = opObject["type"].toString();

            if (type == "normal") {
                //                qDebug()<<"operation type: NORMAL ";
                operation->type = EOperationType::Normal;

                QJsonArray conditions = opObject["conditions"].toArray();

                for (int con = 0; con < conditions.size() && allDevicesExists; con++) {
                    QJsonObject jsonCondition = conditions[con].toObject();
                    if (jsonCondition["type"].toString() == "device") {

                        device = deviceManager->getCanDevice(
                                    static_cast<DevCat>(jsonCondition["category"].toString().toInt()),
                                jsonCondition["address"].toString().toInt()
                                );
                        //                        qDebug()<<"add device condition "<<device->objectName();
                        if (device != NULL) {
                            condition = new CDevCondition(
                                        device,
                                        jsonCondition["param"].toString(),
                                    CCondition::stringToCondition(jsonCondition["condition"].toString()),
                                    jsonCondition["value"].toString().toInt()
                                    );
                            condition->extLogicConditionId = jsonCondition["extId"].toString();
                            operation->deviceConditions.insert(condition->conditionId);
                            conditionsList[condition->conditionId] = condition;
                        } else {
                            allDevicesExists = false;
                        }
                    }
                    if (jsonCondition["type"].toString() == "time") {
                        //                        qDebug()<<"add time condition ";

                        CTimeCondition* condition = new CTimeCondition(
                            CCondition::stringToCondition(jsonCondition["condition"].toString()),
                            jsonCondition["value"].toString().toInt()
                        );
                        condition->extLogicConditionId = jsonCondition["extId"].toString();
                        operation->timeConditions.insert(condition->conditionId);
                        conditionsList[condition->conditionId] = condition;
                    }
                    if (jsonCondition["type"].toString() == "alarm") {
                        CAlarmCondition* condition = new CAlarmCondition(jsonCondition["value"].toString() == "1");
                        condition->extLogicConditionId = jsonCondition["extId"].toString();
                        operation->alarmConditions.insert(condition->conditionId);
                        conditionsList[condition->conditionId] = condition;
                    }
                }

                QJsonArray actions = opObject["actions"].toArray();
                for (int act = 0; act < actions.size() && allDevicesExists; act++) {
                    QJsonObject actionObj = actions[act].toObject();
                    if (actionObj["type"].toString() == "device") {
                        device = deviceManager->getCanDevice(static_cast<DevCat>(actionObj["category"].toString().toInt()),
                                actionObj["address"].toString().toInt());
                        if (device != NULL) {
                            action = new CDeviceAction(device, actionObj["param"].toString(), actionObj["value"].toString().toInt());
                            operation->actions.push_back(action);
                        } else {
                            allDevicesExists = false;
                        }
                    }
                    if (actionObj["type"].toString() == "gsmModem") {

                        action = new CGsmModemAction(actionObj["number"].toString(), actionObj["message"].toString());
                        operation->actions.push_back(action);

                    }
                    if (actionObj["type"].toString() == "rest") {

                        action = new CRestAction(actionObj["endpoint"].toString(), actionObj["message"].toString());
                        operation->actions.push_back(action);

                    }
                }
            } else if (type == "delayed") {
                operation->type = EOperationType::Delayed;
                operation->stopConditionsExtLogic = opObject["stopExtCondLogic"].toString();

                QJsonArray conditions = opObject["conditions_start"].toArray();
                for (int con = 0; con < conditions.size() && allDevicesExists; con++) {
                    QJsonObject jsonCondition = conditions[con].toObject();
                    // qDebug()<<"condition: "<<jsonCondition;
                    if (jsonCondition["type"].toString() == "device") {
                        device = deviceManager->getCanDevice(static_cast<DevCat>(jsonCondition["category"].toString().toInt()),
                                jsonCondition["address"].toString().toInt()
                                );
                        if (device != NULL) {
                        
                            condition = new CDevCondition(
                                        device,
                                        jsonCondition["param"].toString(),
                                    CCondition::stringToCondition(jsonCondition["condition"].toString()),
                                    jsonCondition["value"].toString().toInt()
                                    );
                            condition->extLogicConditionId = jsonCondition["extId"].toString();
                            operation->deviceConditions.insert(condition->conditionId);
                            conditionsList[condition->conditionId] = condition;

                        } else {
                            allDevicesExists = false;
                        }
                    }
                    if (jsonCondition["type"].toString() == "time") {
                        CTimeCondition* condition = new CTimeCondition(
                             CCondition::stringToCondition(jsonCondition["condition"].toString()),
                             jsonCondition["value"].toString().toInt()
                        );
                        condition->extLogicConditionId = jsonCondition["extId"].toString();
                        operation->timeConditions.insert(condition->conditionId);
                        conditionsList[condition->conditionId] = condition;
                    }
                    if (jsonCondition["type"].toString() == "alarm") {
                        CAlarmCondition* condition = new CAlarmCondition(jsonCondition["value"].toString() == "1");
                        condition->extLogicConditionId = jsonCondition["extId"].toString();
                        operation->alarmConditions.insert(condition->conditionId);
                        conditionsList[condition->conditionId] = condition;
                    }
                }

                conditions = opObject["conditions_stop"].toArray();
                for (int con = 0; con < conditions.size() && allDevicesExists; con++) {
                    QJsonObject jsonCondition = conditions[con].toObject();
                    if (jsonCondition["type"].toString() == "device") {
                        device = deviceManager->getCanDevice(static_cast<DevCat>(jsonCondition["category"].toString().toInt()),
                                jsonCondition["address"].toString().toInt()
                                );
                        if (device != NULL) {
                            condition = new CDevCondition(
                                        device,
                                        jsonCondition["param"].toString(),
                                    CCondition::stringToCondition(jsonCondition["condition"].toString()),
                                    jsonCondition["value"].toString().toInt());
                            condition->extLogicConditionId = jsonCondition["extId"].toString();
                            operation->stopConditions.insert(condition->conditionId);
                            conditionsList[condition->conditionId] = condition;

                        } else {
                            allDevicesExists = false;
                        }
                    }
                    if (jsonCondition["type"].toString() == "time") {
                        CTimeCondition* condition = new CTimeCondition(
                              CCondition::stringToCondition(jsonCondition["condition"].toString()),
                              jsonCondition["value"].toString().toInt()
                        );
                        condition->extLogicConditionId = jsonCondition["extId"].toString();
                        operation->timeStopConditions.insert(condition->conditionId);
                        conditionsList[condition->conditionId] = condition;
                    }
                    if (jsonCondition["type"].toString() == "alarm") {
                        CAlarmCondition* condition = new CAlarmCondition(jsonCondition["value"].toString() == "1");
                        condition->extLogicConditionId = jsonCondition["extId"].toString();
                        operation->alarmStopConditions.insert(condition->conditionId);
                        conditionsList[condition->conditionId] = condition;
                    }
                }

                QJsonArray actions = opObject["actions"].toArray();
                for (int act = 0; act < actions.size() && allDevicesExists; act++) {
                    QJsonObject actionObj = actions[act].toObject();
                    if (actionObj["type"].toString() == "device") {
                        device = deviceManager->getCanDevice(static_cast<DevCat>(actionObj["category"].toString().toInt()),
                                actionObj["address"].toString().toInt());
                        if (device != NULL) {
                            action = new CDeviceAction(device, actionObj["param"].toString(), actionObj["value"].toString().toInt());
                            operation->actions.push_back(action);
                        } else {
                            allDevicesExists = false;
                        }
                    }
                    if (actionObj["type"].toString() == "gsmModem") {

                        action = new CGsmModemAction(actionObj["number"].toString(), actionObj["message"].toString());
                        operation->actions.push_back(action);

                    }
                    if (actionObj["type"].toString() == "rest") {

                        action = new CRestAction(actionObj["endpoint"].toString(), actionObj["message"].toString());
                        operation->actions.push_back(action);

                    }
                }

                operation->delayForActions = opObject["delay"].toString().toInt();
            }
            if (validateOperation(operation, conditionsList) && allDevicesExists) {
                //                qDebug()<<"operation is valid";
                opContainer->addConditionsWithOperation(conditionsList.values(), operation);
                logger->low_info(EVENT_MANAGER, "New valid operation has been added to operation list " + operation->name);

            } else {
                logger->warning(EVENT_MANAGER, "Invalid operation couldn\'t be added to operation list: " + operation->name);

                delete operation;
                for (QString key : conditionsList.keys()) {
                    delete conditionsList[key];
                    conditionsList[key] = NULL;
                }
                //                qDebug()<<"operation invalid ";
            }
        }
        logger->success(EVENT_MANAGER, "Operations list loaded successfully");
        opContainer->printOperationList();
    } catch(CException ex) {
        logger->error(EVENT_MANAGER, "Error during loading operations list: " + ex.what());
        opContainer->clearContainer();
    } catch(std::exception ex) {
        logger->error(EVENT_MANAGER, "Error during loading operations list: " + QString(ex.what()));
        opContainer->clearContainer();
    }

}



bool COperationListLoader::validateOperation(COperation *operation, QMap<QString, CCondition*> conditions) {

    if (operation->actions.size() == 0 ||
            (operation->deviceConditions.size() == 0
             && operation->timeConditions.size() == 0
             && operation->alarmConditions.size() == 0)) {
        return false;
    }

    if (operation->type == EOperationType::Delayed) {
        if (operation->stopConditions.size() == 0
                && operation->timeStopConditions.size() == 0
                && operation->alarmStopConditions.size() == 0) {
            return false;
        }
    }

    for (QString condId : operation->deviceConditions) {
        if (conditions.contains(condId) && conditions[condId]->device == NULL) {
            return false;
        }
    }

    for (CAction *action : operation->actions) {
        if (action->getActionType() == EActionType::DEVICE_ACTION) {
            if (static_cast<CDeviceAction*>(action)->device == NULL) {
                return false;
            }
        } else if (action->getActionType() == EActionType::GSM_MODULE_ACTION) {
            if (static_cast<CGsmModemAction*>(action)->phoneNumber == "") {
                return false;
            }
        } else if (action->getActionType() == EActionType::REST_ACTION) {
            if (static_cast<CRestAction*>(action)->endpoint == "") {
                return false;
            }
        }

    }

    for (QString condId : operation->stopConditions) {
        if (conditions.contains(condId) && conditions[condId]->device == NULL) {
            return false;
        }
    }

    if (!validateOperationWithExtendedConditionsLogic(operation, conditions)) {
        return false;
    }

    return true;
}

bool COperationListLoader::validateOperationWithExtendedConditionsLogic(COperation *operation, QMap<QString, CCondition*> conditions) {
    if (operation->conditionsExtLogic != "") {
        QRegExp logic("^[A-Z()!|&]+$");
        QRegExp id("^[A-Z]$");
        QSet<QString> ids;
        qint64 idNumber = 0;

        if (!logic.exactMatch(operation->conditionsExtLogic)){
            return false;
        }

        for (QString condId : operation->deviceConditions) {
            if (!id.exactMatch(conditions[condId]->extLogicConditionId)) {
                return false;
            }
            idNumber++;
            ids.insert(conditions[condId]->extLogicConditionId);
        }

        for (QString condId : operation->timeConditions) {
            if (!id.exactMatch(conditions[condId]->extLogicConditionId)) {
                return false;
            }
            idNumber++;
            ids.insert(conditions[condId]->extLogicConditionId);
        }

        for (QString condId : operation->alarmConditions) {
            if (!id.exactMatch(conditions[condId]->extLogicConditionId)) {
                return false;
            }
            idNumber++;
            ids.insert(conditions[condId]->extLogicConditionId);
        }

        if (idNumber != ids.size()) {
            return false;
        }

        for (qint64 index = 0; index < operation->conditionsExtLogic.length(); index++) {
            QString sign = operation->conditionsExtLogic.mid(index, 1);
            if (id.exactMatch(sign) && !ids.contains(sign)) {
                return false;
            }
        }
    }

    if (operation->stopConditionsExtLogic != "" && operation->type == EOperationType::Delayed) {
        QRegExp logic("^[A-Z()|&]+$");
        QRegExp id("^[A-Z]$");
        QSet<QString> idsStop;
        qint64 idStopNumber = 0;

        if (!logic.exactMatch(operation->stopConditionsExtLogic)){
            return false;
        }

        for (QString condId : operation->stopConditions) {
            if (!id.exactMatch(conditions[condId]->extLogicConditionId)) {
                return false;
            }
            idStopNumber++;
            idsStop.insert(conditions[condId]->extLogicConditionId);
        }

        for (QString condId : operation->timeStopConditions) {
            if (!id.exactMatch(conditions[condId]->extLogicConditionId)) {
                return false;
            }
            idStopNumber++;
            idsStop.insert(conditions[condId]->extLogicConditionId);
        }

        for (QString condId : operation->alarmStopConditions) {
            if (!id.exactMatch(conditions[condId]->extLogicConditionId)) {
                return false;
            }
            idStopNumber++;
            idsStop.insert(conditions[condId]->extLogicConditionId);
        }

        if (idStopNumber != idsStop.size()) {
            return false;
        }

        for (qint64 index = 0; index < operation->stopConditionsExtLogic.length(); index++) {
            QString sign = operation->stopConditionsExtLogic.mid(index, 1);
            if (id.exactMatch(sign) && !idsStop.contains(sign)) {
                return false;
            }
        }
    }

    return true;
}

