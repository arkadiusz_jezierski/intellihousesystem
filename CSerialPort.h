#ifndef CSERIALPORT_H
#define CSERIALPORT_H

#include <QObject>
#include <termios.h>
#include <fcntl.h>
#include <unistd.h>
#include <CTimeOut.h>
#include <CException.h>
#include <CErrorException.h>
#include <CBuffer.h>
#include <CSerialPortManager.h>

#define NO_GLOBAL_ERROR 0
#define PORT_NAME_LEN 256
#define BUFF_SIZE   4000

class CSerialPort : public QObject
{
    Q_OBJECT

    int openTermio();

    char m_PortName[PORT_NAME_LEN + 1];
    long m_Baud;
    long m_DBits;
    long m_SBits;
    long m_ParityOn;
    long m_ParityMode;
    long m_Mode;
    long m_PortFd;
    int m_RxTout;
    struct termios m_OldTio, m_ActTio;


    unsigned char buf[BUFF_SIZE];

    int sendBuffer(int bufferSize);

protected:
    QString portName;

public:

    bool isOpen();
    int openSerial(QString PortName, long Baud = 9600, int DBits = 8, int SBits = 2, int Parity = 0);
    void closePort();

    unsigned char receiveByte(int timeOut = 50);


    void sendBuffer(const CBuffer &buffer);


    int flushReceiveBuffer(void);


    explicit CSerialPort(QObject *parent = 0);
    ~CSerialPort();

};

#endif // CSERIALPORT_H
