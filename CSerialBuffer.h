#ifndef CSERIALBUFFER_H
#define CSERIALBUFFER_H

#include <CBuffer.h>
#include <CCanBuffer.h>
#include <HCommConstans.h>

class CSerialBuffer : public CBuffer
{

public:
    CSerialBuffer();
    CSerialBuffer (const CSerialBuffer & buf);
    CSerialBuffer (const CBuffer &) : CBuffer() {}
    ~CSerialBuffer();

    bool isBufferReady();
    bool isHandshakeResponse();
    unsigned char calculateCRC();
    bool isBadCrcBuffer();
    unsigned char getResponsedBufferCommand();
    unsigned char getRequestedBufferLengthByte();
    void printErrorBuffer();
    void printNoDataFrame();
    void printDataFrame();
    unsigned long getCanID() ;
    bool isACK();
    unsigned char getCRC() const;
    
};

#endif // CSERIALBUFFER_H
