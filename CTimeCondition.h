#ifndef CTIMECONDITION_H
#define CTIMECONDITION_H

#include "CCondition.h"

class CTimeCondition : public CCondition
{
	bool compareTimeValues(qint64 value1, qint64 value2, ELogicalConditionType condition);
	void calculateCondition();

public:
    CTimeCondition(ELogicalConditionType condition, qint64 val);

    QString toString();

    QString convertTimeValue(qint64 val);
};

#endif // CTIMECONDITION_H
