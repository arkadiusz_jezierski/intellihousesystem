#include "CModemBuffer.h"

CModemBuffer::CModemBuffer()
{

}

CModemBuffer::~CModemBuffer()
{

}

CModemBuffer::EFrameResponeType CModemBuffer::isBufferReady() {
    if (this->size() == 0) {
        return EFrameResponeType::EMPTY;
    } else if (this->size() < 2){
        return EFrameResponeType::INVALID;
    } else {
        for (size_t i = 0; i < this->size(); i++) {
            if (
                    (this->operator[](i) == CODE_CR &&
                    this->size() > (i + 1) &&
                    this->operator[](i + 1) == CODE_LF)
                    ||
                    (this->operator[](i) == CODE_CR &&
                     this->size() == (i + 1))
                    ) {
                if (this->toString().contains(RESP_ERROR, Qt::CaseInsensitive)){
                   return EFrameResponeType::ERROR;
                } else {
                    return EFrameResponeType::OK;
                }

            }
        }

        return EFrameResponeType::INVALID;
    }
}

bool CModemBuffer::isEmpty() {
    return (this->size() == 0);
}

bool CModemBuffer::isOkResponse() {
    return (this->toString().contains(RESP_OK, Qt::CaseInsensitive));
}

bool CModemBuffer::isSendMessageResponse(){
    return (this->toString().contains(RESP_SEND_MSG, Qt::CaseInsensitive));
}

QString CModemBuffer::toString(){
    QString msg = "";

    for (size_t i = 0; i < this->size(); i++) {
        if (this->operator[](i) >= 0x20 && this->operator[](i) <= 0x7e){
            msg += QString(this->operator[](i));
        }
    }

    return msg;
}

CModemBuffer CModemBuffer::getResponse(){
    CModemBuffer retBuf;
    if (this->size() < 2) {
        return retBuf;
    }

    for (size_t i = 0; i < this->size(); i++) {
        if (this->operator[](i) == CODE_CR &&
                this->size() > (i + 1) &&
                this->operator[](i + 1) == CODE_LF) {
            i++;

            if (retBuf.size() > 0) {
                retBuf << " ";
            }
            continue;
        }

        if (this->operator[](i) >= 0x20 && this->operator[](i) <= 0x7e) {
            retBuf << (unsigned char) this->operator[](i);
        }

    }

    return retBuf;
}
