#include "COperation.h"

COperation::COperation()
{
	this->operationId = "OPR" + QString::number(QDateTime::currentMSecsSinceEpoch()) + QString::number(qrand());
}

COperation::~COperation()
{

}


QSet<QString> COperation::getRegularConditions() {
	return deviceConditions|timeConditions|alarmConditions;
}
    
QSet<QString> COperation::getDelayedStopConditions() {
	return stopConditions|timeStopConditions|alarmStopConditions;
}

QSet<QString> COperation::getDelayedStartConditions() {
	return deviceConditions|timeConditions|alarmConditions;
}
