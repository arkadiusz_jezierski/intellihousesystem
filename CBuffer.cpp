#include "CBuffer.h"

CBuffer::CBuffer()
{
    logger= &CSingleton<CLogger>::Instance();
}

CBuffer::~CBuffer()
{

}

void CBuffer::operator << (const unsigned char &data){
    buffer.push_back((unsigned char) data);
}

void CBuffer::operator << (const unsigned short &data){
    buffer.push_back((unsigned char) ((data >> 8) & 0xff));
    buffer.push_back((unsigned char) (data & 0xff));
}

void CBuffer::operator << (const unsigned int &data){
    buffer.push_back((unsigned char) ((data >> 24) & 0xff));
    buffer.push_back((unsigned char) ((data >> 16) & 0xff));
    buffer.push_back((unsigned char) ((data >> 8) & 0xff));
    buffer.push_back((unsigned char) (data & 0xff));
}

void CBuffer::operator << (const int &data){
    this->operator <<((unsigned int) data);
}

void CBuffer::operator << (const CBuffer &data){
    for (size_t i = 0; i < data.size(); i++){
        buffer.push_back(data[i]);
    }
}

void CBuffer::operator <<(const QVector<unsigned char> &data){
    for (int i = 0; i < data.size(); i++){
        buffer.push_back(data[i]);
    }
}

void CBuffer::operator <<(const QString &data){
    for (int i = 0; i < data.length(); i++){
        buffer.push_back(static_cast<unsigned char>(data[i].unicode()));
    }
}

void CBuffer::subbuffer(const CBuffer &input, const unsigned int &index, const unsigned int &length){
    if (index >= input.size()){
        logger->warning("Trying to get element " + QString::number(index) + ". from buffer with size " + QString::number(buffer.size()));
        return;
    }
    if((index + length) > input.size()){
        logger->warning("Trying to get more elements than buffer contains");
        return;
    }
    size_t len = length;
    buffer.clear();
    for (size_t i = index; (i < input.size()) && (len--); i++){
        buffer.push_back(input[i]);
    }
}

CBuffer CBuffer::subbuffer(unsigned int start, unsigned int length) {

    CBuffer subBuffer;
    unsigned int end = length + start;
    if (length == 0)
        end = buffer.size();

    for (unsigned int i = start; i < end && i < this->size(); i++)
        subBuffer << (unsigned char) this->operator [](i);

    return subBuffer;
}

unsigned char CBuffer::operator[] (const unsigned int &index) const{
    if ((unsigned int)buffer.size() > index){
        return buffer[index];
    } else {
        logger->warning("Trying to get " + QString::number(index) + ". element from buffer with size " + QString::number(buffer.size()));
        return 0;
    }
}

bool CBuffer::operator ==(const CBuffer &buffer){
    if (buffer.size() != this->size()){
        return false;
    }
    for (size_t i = 0; i < buffer.size(); i++){
        if (buffer[i] != this->operator [](i)){
            return false;
        }
    }

    return true;
}

QString CBuffer::getPrintBuffer(bool hex) const{
    QString buf;
    for (auto el : buffer){
        if (hex){
            buf += "0x";
            buf += QString::number(el, 16) + " ";
        }else{
            buf += QString::number(el) + " ";
        }
    }

    return buf;
}

void CBuffer::printBuffer(bool hex) const{
    logger->debug("Buffer ["+getPrintBuffer(hex)+"]");
}


size_t CBuffer::size() const {
    return (size_t)buffer.size();
}

unsigned char CBuffer::getCommand() const{
    return command;
}

void CBuffer::setCommand(unsigned char value){
    command = value;
}


unsigned char CBuffer::getCRC() const{
    return 0;
}

void CBuffer::setCanID(const unsigned int id){
    command = id;
}

unsigned int CBuffer::getCanID() const{
    return 0;
}

void CBuffer::clear(){
    command = 0;
    buffer.clear();
}


