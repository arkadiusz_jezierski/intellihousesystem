#include "CInitDevicesConfig.h"

CInitDevicesConfig::CInitDevicesConfig(QObject *parent) : QObject(parent)
{
    logger = &CSingleton<CLogger>::Instance();
    loadConfig();

}

CInitDevicesConfig::~CInitDevicesConfig()
{
    qDebug("InitDeviceConfig destructor");
}


QString CInitDevicesConfig::loadFromFile(){
    QFile file("settings/init_devices_config.json");
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QString val = file.readAll();
    file.close();
    return val;
}

void CInitDevicesConfig::loadConfig(){

    CLogger *logger = &CSingleton<CLogger>::Instance();

    try{
        QJsonDocument json = QJsonDocument::fromJson(loadFromFile().toUtf8());
        QJsonObject mainObject = json.object();

        QJsonArray configs = mainObject[DEV_CONFIG].toArray();

        foreach (const QJsonValue & value, configs) {
            QJsonObject config = value.toObject();

            devicesConfigList.push_back(CDeviceConfig(
            	config[DEV_CONFIG_CATEGORY].toString(),
            	config[DEV_CONFIG_ADDRESS].toString(),
            	config[DEV_CONFIG_PARAM].toString(),
            	config[DEV_CONFIG_VALUE].toString()
            ));
 
        }

        logger->debug(SYSTEM, "Init Devices Configuration:");
        quint16 i = 1;
        for (CDeviceConfig conf : devicesConfigList) {
            logger->debug(SYSTEM, QString::number(i++)+ ". " + conf.toString());
        }

        logger->success(SYSTEM, "Init devices configuration loading success");


    }catch(CException ex){
        logger->error("Error during loading init devices configuration file: " + ex.what());

    }catch(std::exception ex){
        logger->error("Error during loading init devices configuration file: " + QString(ex.what()));

    }

}


QList<CDeviceConfig> CInitDevicesConfig::getDevicesConfig() {
    return devicesConfigList;
}

QList<CDeviceConfig> CInitDevicesConfig::getConfigForDevice(DevCat category, unsigned char address) {
    QList<CDeviceConfig> deviceConfig;

    for (CDeviceConfig conf : devicesConfigList) {
        if (conf.getDevCategory() == category && conf.getDevAddress() == address) {
            deviceConfig.push_back(conf);
        }
    }

    return deviceConfig;
}

