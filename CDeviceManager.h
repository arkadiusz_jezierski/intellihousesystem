#ifndef CDEVICEMANAGER_H
#define CDEVICEMANAGER_H

#include <QObject>
#include <QVector>
#include <QMap>
#include <QList>
#include <CCanDevice.h>
#include <CCanBuffer.h>
#include <CLogger.h>
#include <QDataStream>
#include <QFile>
#include <CDevBistableSwitch.h>
#include <CDevBistableSensor.h>
#include <CDevPwmDriver.h>
#include <CDevRgbDriver.h>
#include <CDevMotionDetector.h>
#include <CDevBroadcast.h>
#include <qtconcurrentrun.h>
#include <CTransceiver.h>
#include <CEvent.h>
#include <QMutex>
#include "CDeviceConfig.h"
#include <CInitDevicesConfig.h>

#define DEVICES_FILE_DATA   "devices"



struct SDeviceAction{
    CCanDevice *device;
    QString parameter;
    qint64 value;
};

struct SDeviceExpectedStatus{
    SDeviceAction expectedStatus;
    SDeviceAction action;
    qint64 startTime;
};

struct SDeviceRequest{
    CCanDevice *device;
    QString parameter;
    qint64 lastParamRequestTmstp;
};



class   CDeviceManager : public QObject
{
    Q_OBJECT

    mutable QMutex devManagerMutex;
    mutable QMutex requestQueueMutex;
    mutable QMutex expDeviceParamMutex;
    mutable QMutex actionsQueueMutex;

    QList<SDeviceRequest> requestQueue;
    QList<SDeviceExpectedStatus> expectedDeviceParameters;
    QList<SDeviceAction> actionsQueue;

    QMap<UID, QVector<CCanDevice*>> tempDevicesList;
    QVector<UID>currentlyAddressedDevicesUID;

    QMap<QString, CCanDevice*> devicesList;
    QVector<CCanDevice*> devicesWithPingRequest;
    QVector<CCanDevice*> specialDevicesList;

    QDateTime lastPing;
    QDateTime lastDeviceAvailabilityChecking;

    CLogger *logger = NULL;
    CTransceiver *transceiver = NULL;
    CConfig *config = NULL;
    CInitDevicesConfig *devicesConfig = NULL;

    CCanDevice* getTheLatestSeenDevice();
    bool checkDevicesWithPingRequestList(CCanDevice* device);

    void destroyDevices(QMap<QString, CCanDevice*> devices);
    void destroyDevices(QVector<CCanDevice *> &devices);
    void updateDevicesFileData();
    UID getUID(unsigned char address, DevCat category);

    void removeDevicesFromLists(QVector<CCanDevice*> deviceToRemove);
    void removeActionsFromQueueForDevice(CCanDevice* device);
    void removeExpectedParameterFromQueueForDevice(CCanDevice *device);
    void removeRequestParameterFromQueueForDevice(CCanDevice *device);

    void addNewDeviceParameterRequest(CCanDevice *device, QString parameter);
    void addNewActionForDevice(CCanDevice *device, QString parameter, qint64 value);
    void addNewExpectedDeviceParameter(CCanDevice *device, QString expectedParameter, qint64 expectedValue,
                                            QString actionParameter, qint64 actionValue);

    CCanDevice *getCategoryBroadcastDevice(DevCat category, unsigned char address);
    CCanDevice *getAddressBroadcastDevice(DevCat category, unsigned char address);
    bool isBroadCastDevice(CCanDevice *device);

    void configureDevices(QList<CDeviceConfig> configs);

    bool devicesLocked = false;
    QString firmwareFileName;

    int pingInterval;
    int maxLastSeenTime;
    int expectedParamInterval;

public:


    explicit CDeviceManager(QObject *parent = 0);
    ~CDeviceManager();

    void assignTransceiverPointer(CTransceiver *transceiver);

    void loadDevices();
    void setDeviceName(CCanDevice *device, QString name);

    unsigned char getFirstFreeAddress(DevCat category);//jesli loadAddressesForUID zwroci false
    CCanDevice* createNewCanDevice(DevCat category, UID uid, unsigned char address);
    CCanDevice* createDeviceForCategory(DevCat category);
    void loadAddressesForUID(UID uid);
    EAddressingStatus getAddresses(const CCanBuffer &buffer, QVector<unsigned char> &addresses);
    DevCat getRequestedCategory(const CCanBuffer &buffer, size_t index);
    unsigned char readAddressForCategory(const UID uid, const DevCat category);
    void loadDevicesFileData();


    CCanDevice* getCanDevice(const CCanBuffer &incomingBuffer);
    CCanDevice* getFirstCanDevice(const UID &uid);
    CCanDevice* getCanDevice(DevCat category, unsigned char address);
    CCanDevice* getCanDevice(QString deviceId);
    
    QList<CCanDevice*> getCanDevices();
    QList<CCanDevice*> getCanDevices(const UID &uid);
//    bool getExistedCanDevices(QVector<CCanDevice*> &devices);

    void executeActionForCommand(const CCanBuffer &incomingBuffer);

    void clearDeviceStatus(const UID &uid);


    void pingDevice();
    void pingResponse(const CCanBuffer &incomingBuffer);

    void checksDevicesAvailability();

    void checkDeviceParameterRequestQueue();
    void checkActionsQueue();
    void checkExpectedDeviceParameterStatus();
    void uploadFirmware();
    void removeDevice(UID uid);
    void removeDevice(DevCat category, int addressBegin, int addressEnd);
    void assignAddresses(const CCanBuffer& buffer);

    void configureDevices();

    void handleStartingUpDevice(const UID &uid, SDeviceStartupInfo startupInfo);

signals:
    void sendDevicesListChangedSignal();
    void sendPauseThreadsSignal();
    void sendResumeThreadSignal();
public slots:
    void requestDeviceParameter(DevCat category, unsigned char address, QString parameter);
    void requestDeviceParameter(QString deviceId, QString parameter);
    void addNewDeviceParameterStatus(DevCat category, unsigned char address, QString parameter, qint64 value);
    void startUploadFirmware(QString fileName);
    void bootDevice(UID uid);
    void resetAddressesInDevices();

};

#endif // CDEVICEMANAGER_H
