#ifndef CSERIALPORTMANAGER_H
#define CSERIALPORTMANAGER_H

#include <CSingleton.h>
#include <QtSerialPort/QSerialPortInfo>
#include <QList>

class CSerialPortManager : public QObject
{
    Q_OBJECT

    QList<QString> busyPorts;

public:
    explicit CSerialPortManager(QObject *parent = 0);
    ~CSerialPortManager();

    void setPortBusy(QString portName);
    void releasePort(QString portName);

    QList<QString> getAvailablePorts();
};

#endif // CSERIALPORTMANAGER_H
