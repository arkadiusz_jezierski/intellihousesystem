#ifndef CRESTAPISERVICE_H
#define CRESTAPISERVICE_H

#include <QtCore/qobject.h>
#include <QJsonObject>
#include <QJsonDocument>
#include <CLogger.h>
#include "CSingleton.h"
#include "COperationListLoader.h"
#include "HRestApiServiceConsts.h"
#include "CDeviceManager.h"
#include "CCanDevice.h"
#include "CFirmwareLoader.h"
#include "HTypes.h"
#include "CGsmModem.h"
#include "CAlarmModule.h"
#include <QDebug>
#include <CRestHandler.h>
#include "pistache/endpoint.h"
#include <CLoggerPoster.h>

using namespace Net;

class CRestApiService : public QObject
{
    Q_OBJECT

    CLogger *logger = NULL;
    CAlarmModule *alarm;
    CDeviceManager *deviceManager = NULL;

    typedef void (CRestApiService::*requestMethod)(QJsonObject, Http::ResponseWriter &response);
    QMap<QString, requestMethod> requestMethodsMap;

    CCanDevice* getCanDevice(QJsonObject device);
    CCanDevice* getCanDevice(const UID &uid);

    void initRequestMethods();

    void sendResponse(Http::ResponseWriter &response, QJsonObject jsonResponse);

    void sendPausedThreadResponse(Http::ResponseWriter &response);

    void setDeviceParameter(QJsonObject request, Http::ResponseWriter &response);
    void getJson(QJsonObject request, Http::ResponseWriter &response);
    void setDeviceName(QJsonObject request, Http::ResponseWriter &response);
    void saveJson(QJsonObject request, Http::ResponseWriter &response);
    void getDeviceParameter(QJsonObject request, Http::ResponseWriter &response);
    void getDevicesList(QJsonObject request, Http::ResponseWriter &response);
    void getFilesList(QJsonObject request, Http::ResponseWriter &response);
    void uploadFirmware(QJsonObject request, Http::ResponseWriter &response);
    void bootDevice(QJsonObject request, Http::ResponseWriter &response);
    void initDevices(QJsonObject request, Http::ResponseWriter &response);
    void removeDevice(QJsonObject request, Http::ResponseWriter &response);
    void removeDeviceAdvance(QJsonObject request, Http::ResponseWriter &response);
    void getDeviceDetails(QJsonObject request, Http::ResponseWriter &response);
    void getDeviceParameters(QJsonObject request, Http::ResponseWriter &response);
    void getCategories(QJsonObject request, Http::ResponseWriter &response);
    void sendMessage(QJsonObject request, Http::ResponseWriter &response);
    void armSystem(QJsonObject request, Http::ResponseWriter &response);
    void getArmSystemStatus(QJsonObject request, Http::ResponseWriter &response);

public:
    CRestApiService();
    Http::Endpoint *server;
    void requestDispatcher(QString request, Http::ResponseWriter &response);
    void run();

    bool runThread;
    bool threadPaused;

signals:
    void sendDeviceAction(DevCat category, unsigned char address, QString parameter, qint64 value);
    void sendReloadOperationsSignal();
    void sendDeviceParameterRequest(DevCat category, unsigned char address, QString parameter);
    void sendUploadFirmwareSignal(QString fileName);
    void sendBootDeviceSignal(UID uid);
    void sendInitDeviceSignal();
    void sendRemoveDeviceSignal(UID uid);
    void sendRemoveDeviceAdvanceSignal(DevCat cat, int addressBegin, int addressEnd);
    void sendMessageSignal(QString number, QString message);
    void sendReloadLogFiltersSignal();

public slots:
    void killThread();
    void pauseThread();
    void unpauseThread();

};

#endif // CRESTAPISERVICE_H
