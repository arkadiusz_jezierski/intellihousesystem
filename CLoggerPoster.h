#ifndef CLOGGERPOSTER_H
#define CLOGGERPOSTER_H

#include <QObject>
#include <QDebug>
#include <QDateTime>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QUrl>
#include <CLogger.h>
#include <CSingleton.h>
#include <QThread>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <CConfig.h>

#define 	RESPONSE_SUCCESS	"success"
#define 	RESPONSE_MSG		"msg"
#define 	FILTERS 			"filters"

class CLoggerPoster : public QObject
{
	Q_OBJECT

	QNetworkAccessManager *manager = NULL;

	bool runThread = true;
    bool pausedThread = false;

    CLogger *logger = NULL;
    CConfig *config = NULL;

    QString logDestinationUrl;
    QList<QString> filters;

    void loadFilters();

public:
    CLoggerPoster(QObject *parent = 0);
    ~CLoggerPoster();

    void runLoggerPoster();
  
    static QString loadFromFile();
	static bool saveJsonToFile(QString json);  

public slots:
    void onPostAnswer(QNetworkReply* reply);
    void postLog(QString logData);
    void reloadFiltersList();
    void killThread();
};

#endif // CLOGGERPOSTER_H