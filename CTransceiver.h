#ifndef CPROTOCOL_H
#define CPROTOCOL_H

#include <CSerialPort.h>
#include <CBuffer.h>
#include <CLogger.h>
#include <CSingleton.h>
#include <CCanBuffer.h>

class CTransceiver : public CSerialPort
{
    Q_OBJECT


public:

    enum class EEcanMode{
        ECAN_MODE_0 = MODE_0,
        ECAN_MODE_1 = MODE_1
    };


    explicit CTransceiver();
    ~CTransceiver();
    bool open();
    CLogger *logger;
    virtual CCanBuffer getExCanMessage();
    virtual void sendExCanFrame(const CBuffer &buffer);
    virtual void setCanMode(const EEcanMode mode);
    virtual void sendCanRegularFrame(const CBuffer &can);
    virtual CCanBuffer getRegularCanFrame();
    virtual CCanBuffer sendRegularFrameWithResponse(const CBuffer &can);

    virtual void configForCanA();
    virtual void configForCanB();

protected:
    virtual void openPort(QString portName);
    virtual QString findPort();


    
};

#endif // CPROTOCOL_H
