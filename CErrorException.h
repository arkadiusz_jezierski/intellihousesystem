#ifndef CERROREXCEPTION_H
#define CERROREXCEPTION_H

#include <QObject>
#include <QException>
#include <CException.h>

class CErrorException : public CException
{
public:
    CErrorException *clone() const { return new CErrorException(*this); }
//    CException(const char * msg);
    CErrorException(const QString msg) : CException(msg){}
//    CException(const char * msg, QObject* obj);
    CErrorException(const QString msg, const QObject* obj) : CException(msg, obj){}
    ~CErrorException() throw() {}
};

#endif // CERROREXCEPTION_H
