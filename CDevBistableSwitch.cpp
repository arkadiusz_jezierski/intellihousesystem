#include "CDevBistableSwitch.h"

CDevBistableSwitch::CDevBistableSwitch()
{
    this->setCategory(DevCat::BistableSwitch);

    initParameters();
    initCommandMethods();
    initRequestMethods();
    initExecuteMethods();
}

CDevBistableSwitch::~CDevBistableSwitch()
{

}


void CDevBistableSwitch::initParameters(){
    Value val, justForCommand;
    val.undefined = true;
    val.value = 0;
    val.changes = EParamChanges::None;

    justForCommand.undefined = false;

    parameters[PARAM_STATE] = val;
    commands[PARAM_SWITCH] = justForCommand;
    commands[PARAM_UNBLOCK] = justForCommand;
    commands[PARAM_STATE] = justForCommand;
    commands[PARAM_OFF] = justForCommand;

}

void CDevBistableSwitch::commandMethodOfBistableSwitch(COMMAND cmd, CBuffer &data){

    if (methodsMap.contains(cmd)){
        try{
            (this->*methodsMap[cmd])(data);
        }catch(CException ex){
            logger->error(DEVICE_MANAGER, "Error in BistableSwitch: " + ex.what());
        }
    }else{
        logger->warning(DEVICE_MANAGER, "No method to invoke for command " + QString::number((int)cmd));
    }
}


void CDevBistableSwitch::requestParameterForBistableSwitch(QString param){

    if (requestMethodsMap.contains(param)){
        try{
            (this->*requestMethodsMap[param])();
        }catch(CException ex){
            logger->error(DEVICE_MANAGER, "Error in BistableSwitch: " + ex.what());
        }
    }else{
        logger->warning(DEVICE_MANAGER, "BistableSwitch: No request to invoke for parameter " + param);
    }
}

SCommandSet CDevBistableSwitch::executeCommandForBistableSwitch(SCommandSet commSet){
    SCommandSet retVal;
    if (executeDeviceCommandMap.contains(commSet.parameter)){
        try{
            retVal = (this->*executeDeviceCommandMap[commSet.parameter])(commSet.value);
        }catch(CException ex){
            logger->error(DEVICE_MANAGER, "Error in BistableSwitch: " + ex.what());
        }
    }else{
        logger->warning(DEVICE_MANAGER, "BistableSwitch: No execution to invoke for parameter " + commSet.parameter);
    }
    return retVal;
}


void CDevBistableSwitch::initCommandMethods(){
    methodsMap[CMD_CAN_GET_OUTPUT_STATE] = &CDevBistableSwitch::getOutputState;
    methodsMap[CMD_CAN_SET_OUTPUT] = &CDevBistableSwitch::setOutputState;
}

void CDevBistableSwitch::initRequestMethods(){
    requestMethodsMap[PARAM_STATE] = &CDevBistableSwitch::requestOutputState;
}

void CDevBistableSwitch::initExecuteMethods(){
    executeDeviceCommandMap[PARAM_STATE] = &CDevBistableSwitch::executeSetOutputState;
    executeDeviceCommandMap[PARAM_OFF] = &CDevBistableSwitch::executeSetOutputsOff;
    executeDeviceCommandMap[PARAM_SWITCH] = &CDevBistableSwitch::executeSetOutputStateSwitched;
    executeDeviceCommandMap[PARAM_UNBLOCK] = &CDevBistableSwitch::executeSetOutputUnblocked;
}

void CDevBistableSwitch::getOutputState(CBuffer &data){
    logger->info(DEVICE_MANAGER, this->objectName() + "->getOutputState(" + QString::number(data[OFFSET_CAN_DATA]) + ")");

    Value val;
    val.undefined = false;
    val.value = data[OFFSET_CAN_DATA];
    val.changes = checkValueChanged(PARAM_STATE, val.value);

    parameters[PARAM_STATE] = val;

    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_STATE, val.value);
}

void CDevBistableSwitch::setOutputState(CBuffer &data){
    logger->info(DEVICE_MANAGER, this->objectName() + "->setOutputState(" + QString::number(data[OFFSET_CAN_DATA]) + ")");

    Value val;
    val.undefined = false;
    val.value = data[OFFSET_CAN_DATA];
    val.changes = checkValueChanged(PARAM_STATE, val.value);

    parameters[PARAM_STATE] = val;

    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_STATE, val.value);
}


void CDevBistableSwitch::requestOutputState(){
    logger->info(DEVICE_MANAGER, this->objectName() + "->requestOutputState()");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_GET_OUTPUT_STATE);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
}

SCommandSet CDevBistableSwitch::executeSetOutputState(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeSetOutputState(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_SET_OUTPUT);
    buffer << static_cast<unsigned char>(value);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;
    commSet.parameter = PARAM_STATE;
    commSet.value = value;

    return commSet;
}

SCommandSet CDevBistableSwitch::executeSetOutputStateSwitched(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeSetOutputStateSwitched()");
    value = value;
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_SET_OUTPUT_SWITCHED);
    buffer << static_cast<unsigned char>(0);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;
    if (!parameters[PARAM_STATE].undefined){
        commSet.parameter = PARAM_STATE;
        if (parameters[PARAM_STATE].value == 1){
            commSet.value = 0;
        }else{
            commSet.value = 1;
        }
    }else{
        commSet.expected = false;
    }

    return commSet;
}

SCommandSet CDevBistableSwitch::executeSetOutputUnblocked(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeSetOutputUnblocked(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_SET_OUTPUT_RELAY_UNBLOCK);
    buffer << static_cast<unsigned char>(value);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;
    commSet.expected = false;

    return commSet;
}

SCommandSet CDevBistableSwitch::executeSetOutputsOff(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeSetOutputsOff(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_ALL_OFF);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;
    commSet.parameter = PARAM_STATE;
    commSet.value = 0;

    return commSet;
}
