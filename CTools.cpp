#include "CTools.h"

CTools::CTools()
{

}

QString CTools::generateEventKey(EConditionType condType, CCanDevice* device, QString param, qint64 val) {
	QString ev;
	if (condType == EConditionType::DEVICE) {
		ev = QString(KEY_DEVICE) + "-" + device->getDeviceId() + "-" + param;
	} else if (condType == EConditionType::TIME) {
		ev = QString(KEY_TIME) + "-" + QString::number(val);
	} else if (condType == EConditionType::ALARM) {
		ev = QString(KEY_ALARM);
	} 

	return ev;
}

QString CTools::generateEventKey(EEventType eventType, CCanDevice* dev, QString param, qint64 value) {
	QString ev;
	if (eventType == EEventType::DEVICE_EVENT) {
		ev = QString(KEY_DEVICE) + "-" + dev->getDeviceId() + "-" + param;
	} else if (eventType == EEventType::TIME_EVENT) {
		ev = QString(KEY_TIME) + "-" + QString::number(value);
	} else if (eventType == EEventType::ALARM_EVENT) {
		ev = QString(KEY_ALARM);
	} 

	return ev;
}

QString CTools::secondsSinceMidnightToHourFormat(qint64 secs) {
	qint64 hrs = secs / 3600;
	qint64 mins = secs - (hrs * 3600);
	mins = mins / 60;
	secs = secs % 60;

	return QString::number(hrs) + QString(":") + ((mins < 10) ? QString("0") : QString("")) +
			QString::number(mins) + QString(":") + ((secs < 10) ? QString("0") : QString("")) + QString::number(secs);
}