#ifndef CSINGLETON_H
#define CSINGLETON_H


#include <QObject>

template <class T>
class CSingleton
{
public:
    static T& Instance()
    {

        static T _instance;

        return _instance;   // return it
    }

private:

    CSingleton();	// hide constructor
    ~CSingleton();	// hide destructor
    CSingleton(const CSingleton &); // hide copy constructor
    CSingleton& operator=(const CSingleton &); // hide assign op
};

#endif // CSINGLETON_H
