#include "CCan232.h"

CCan232::CCan232()
{
}

/**
 * @brief CCan232::findPort
 * Searches all available serial ports. Open each port and send
 * "ping" message on open port. Stop searching when valid response
 * message is received.
 * @return name of port when ping message was responsed or empty string
 * when valid port hasn't been found.
 */
QString CCan232::findPort(){
    logger = &CSingleton<CLogger>::Instance();
    logger->info(TRANSCEIVER, "Searching available serial ports");
    config = &CSingleton<CConfig>::Instance();
    CSerialPortManager *portManager = &CSingleton<CSerialPortManager>::Instance();

    for (QString portName : portManager->getAvailablePorts()){

        logger->low_info(TRANSCEIVER, "Checking port: " + portName);
        try{
            openPort(portName);
            if (handshake()){
                this->portName = portName;
                portManager->setPortBusy(portName);
                return portName;
            }
        }catch(CException e){
            logger->error(TRANSCEIVER, "Opening port exception: " + e.what());
            closePort();
        }
    }
    
    return "";
}

/**
 * @brief CCan232::openPort
 * Open serial port with custom parameters
 * @param portName port's name to open
 */
void CCan232::openPort(QString portName){
    int res = openSerial(portName, 115200);
    logger->low_info(TRANSCEIVER, "Opening port result: " + QString::number(res));
    if (res){
        throw CException(QString("Opening port result code ") + QString::number(res), this);
    }
}

CSerialBuffer CCan232::getFrame(){
    
    CSerialBuffer tmpBuffer;
    
    unsigned char byte;
    try {
        byte = receiveByte(5000);
        if (byte == RESP_FRAME_HDR) {
            tmpBuffer << (unsigned char) byte;
            CTimeOut tout;
            tout.startTimeOut(REQUEST_TOUT);
            while (!tout.isTimeOut()) {
                byte = receiveByte(2000);
                tmpBuffer << (unsigned char) byte;
                if (tmpBuffer.isBufferReady()) {
                    if (tmpBuffer.isBadCrcBuffer()) {
                        throw CException (QString("Bad CRC frame was received"), this);
                    }
                    return tmpBuffer;
                }
            }
        } else {
            throw CException (QString("Invalid byte received after request sent: ") + QString::number(byte), this);
        }
    } catch (CException ex) {
        throw CException (QString("getFrame method exception: ") + ex.what() + QString(", tempBuffer: ") + tmpBuffer.getPrintBuffer(true), this);
    }
    
    return tmpBuffer;
    
}

void CCan232::sendFrame(const CBuffer &buffer){
    
//    logger->debug("Sending buffer data: " + buffer.getPrintBuffer(1));
    
    CSerialBuffer tmpBuffer;
    unsigned char crc = buffer.getCRC();
    tmpBuffer << (unsigned char) REQ_FRAME_HDR;
    tmpBuffer << (unsigned char) (5 + buffer.size());
    tmpBuffer << (unsigned char) buffer.getCommand();
    
    tmpBuffer << buffer;
    tmpBuffer << (unsigned char) crc;
    tmpBuffer << (unsigned char) REQ_FRAME_FTR;
    sendBuffer(tmpBuffer);
    
}

/**
 * @brief CCan232::handshake
 * Sends handshake message to device and waits for valid response
 * @return true when valid response was received
 */
bool CCan232::handshake(){
    QMutexLocker locker(&serialPortMutex);
    logger->debug(TRANSCEIVER, "Handshake function");
    
    CSerialBuffer buffer;
    buffer.setCommand(CMD_REQ_HNDSHK);

    sendFrame(buffer);
    buffer = getFrame();

    if (buffer.isHandshakeResponse()) {
        logger->success(TRANSCEIVER, "Handshake response received");
        return true;
    }else{
        logger->error(TRANSCEIVER, "No handshake response received");
    }
    return false;
    
}


/**
 * @brief CCan232::setInitCommand
 * Restarts can232 device with new ecan mode
 * @param mode eecan mode (0 or 1)
 */
void CCan232::setCanMode(const EEcanMode mode) {
    QMutexLocker locker(&serialPortMutex);
    logger->info(TRANSCEIVER, "Sending INIT command");
    CSerialBuffer buffer;
    buffer.setCommand(CMD_REQ_INIT);
    buffer << (unsigned char) mode;
    
    sendFrame(buffer);
    buffer = getFrame();

    if (buffer.isACK()){
        logger->success(TRANSCEIVER, "ACK frame received");
    }else{
        logger->error(TRANSCEIVER, "No ACK received");
    }
}

/**
 * @brief CCan232::setRestartCommand
 * Restarts can232 device. Aborts all transmit pending frames.
 */
void CCan232::setRestartCommand() {

    logger->info(TRANSCEIVER, "Sending RESTART command");
    CSerialBuffer buffer;
    buffer.setCommand(CMD_REQ_RESET);
    
    sendFrame(buffer);
    buffer = getFrame();

    if (buffer.isACK()){
        logger->success(TRANSCEIVER, "ACK frame received");
    }else{
        logger->error(TRANSCEIVER, "No ACK received");
    }
    QThread::msleep(config->getConfigParam(TRANS_RESTART_TIME).toInt());
}

/**
 * @brief CCan232::setExFilter
 * Sets filter value for can-bus messages acceptance
 * @param id filter's ID
 * @param filter filter's value
 */
void CCan232::setExFilter(unsigned char id, unsigned int filter) {
    QMutexLocker locker(&serialPortMutex);
    logger->low_info(TRANSCEIVER, "Setting filter");
    if (id > 15) {
        logger->warning(TRANSCEIVER, "Incorrect filter ID ["+QString::number(id, 10)+"]");
        return;
    }
    if (filter > 0x1fffffff) {
        logger->warning(TRANSCEIVER, "Incorrect filter value ["+QString::number(filter, 16)+"]");
        return;
    }
    CSerialBuffer buffer;
    buffer.setCommand(CMD_REQ_FILT);
    buffer << (unsigned char) id;
    buffer << (unsigned int) filter;
    
    sendFrame(buffer);
    buffer = getFrame();

    if (buffer.isACK()) {
        logger->low_info(TRANSCEIVER, "Filter " + QString::number(id, 10) + ": " + QString::number(filter, 16));
        logger->success(TRANSCEIVER, "ACK frame received");
    }else{
        logger->error(TRANSCEIVER, "No ACK received");
    }
    
}

void CCan232::setRegularFilter(unsigned char id, unsigned int filter) {
    QMutexLocker locker(&serialPortMutex);
    logger->low_info(TRANSCEIVER, "Setting filter");
    if (id > 5) {
        logger->warning(TRANSCEIVER, "Incorrect filter ID ["+QString::number(id, 10)+"]");
        return;
    }
    if (filter > 0x3ff) {
        logger->warning(TRANSCEIVER, "Incorrect filter value ["+QString::number(filter, 16)+"]");
        return;
    }
    CSerialBuffer buffer;
    buffer.setCommand(CMD_REQ_FILT);
    buffer << (unsigned char) id;
    buffer << (unsigned int) filter;

    sendFrame(buffer);
    buffer = getFrame();

    if (buffer.isACK()) {
        logger->low_info(TRANSCEIVER, "Filter " + QString::number(id, 10) + ": " + QString::number(filter, 16));
        logger->success(TRANSCEIVER, "ACK frame received");
    }else{
        logger->error(TRANSCEIVER, "No ACK received");
    }

}

/**
 * @brief CCan232::setExMask
 * Sets mask value for can-bus messages acceptance
 * @param id mask's ID
 * @param mask mask's value
 */
void CCan232::setExMask(unsigned char id, unsigned int mask) {
    QMutexLocker locker(&serialPortMutex);
    logger->low_info(TRANSCEIVER, "Setting mask");
    if (id > 1) {
        logger->warning(TRANSCEIVER, "Incorrect mask ID ["+QString::number(id, 10)+"]");
        return;
    }
    if (mask > 0x1fffffff) {
        logger->warning(TRANSCEIVER, "Incorrect mask value ["+QString::number(mask, 16)+"]");
        return;
    }
    CSerialBuffer buffer;
    buffer.setCommand(CMD_REQ_MASK);
    buffer << (unsigned char) id;
    buffer << (unsigned int) mask;
    
    
    sendFrame(buffer);
    buffer = getFrame();
    
    if (buffer.isACK()) {
        logger->low_info(TRANSCEIVER, "Mask " + QString::number(id, 10) + ": " + QString::number(mask, 16));
        logger->success(TRANSCEIVER, "ACK frame received");
    }else{
        logger->error(TRANSCEIVER, "No ACK received");
    }
    
}


void CCan232::setRegularMask(unsigned char id, unsigned int mask) {
    QMutexLocker locker(&serialPortMutex);
    logger->low_info(TRANSCEIVER, "Setting mask");
    if (id > 1) {
        logger->warning(TRANSCEIVER, "Incorrect mask ID ["+QString::number(id, 10)+"]");
        return;
    }
    if (mask > 0x3ff) {
        logger->warning(TRANSCEIVER, "Incorrect mask value ["+QString::number(mask, 16)+"]");
        return;
    }
    CSerialBuffer buffer;
    buffer.setCommand(CMD_REQ_MASK);
    buffer << (unsigned char) id;
    buffer << (unsigned int) mask;


    sendFrame(buffer);
    buffer = getFrame();

    if (buffer.isACK()) {
        logger->low_info(TRANSCEIVER, "Mask " + QString::number(id, 10) + ": " + QString::number(mask, 16));
        logger->success(TRANSCEIVER, "ACK frame received");
    }else{
        logger->error(TRANSCEIVER, "No ACK received");
    }

}

/**
 * @brief CCan232::clearRXbuffer
 * Clears RX CAN buffer in transceiver
 */
void CCan232::clearRXbuffer() {
    QMutexLocker locker(&serialPortMutex);
    logger->low_info(TRANSCEIVER, "Clearing RX buffer");
    
    CSerialBuffer buffer;
    buffer.setCommand(CMD_REQ_DELETE);
    
    sendFrame(buffer);
    buffer = getFrame();
    
    if (buffer.isACK()) {
        logger->success(TRANSCEIVER, "ACK frame received");
    }else{
        logger->error(TRANSCEIVER, "No ACK received");
    }
    
}

/**
 * @brief CCan232::getPendingFrame
 * Reads from can232 device pending message.
 */
CCanBuffer CCan232::getExCanMessage() {
    QMutexLocker locker(&serialPortMutex);

    CSerialBuffer buffer;
    CCanBuffer canBuffer;
    canBuffer.clear();
    buffer.setCommand(CMD_REQ_GET);
    
    sendFrame(buffer);
    buffer = getFrame();

    switch (buffer.getResponsedBufferCommand()) {
    case CMD_RESP_ERROR:
        buffer.printErrorBuffer();
        lastNoDataBuffer.clear();
        setRestartCommand();
        break;
    case CMD_RESP_NO_DATA:
        if(lastNoDataBuffer == buffer){
            break;
        }else{
            lastNoDataBuffer = buffer;
        }
        buffer.printNoDataFrame();
        break;
    case CMD_RESP_DATA:
        lastNoDataBuffer.clear();
        buffer.printDataFrame();
        canBuffer.setCanID(buffer.getCanID());
        if (buffer.size() >= FRAME_UART2CAN_MIN_SIZE){
            canBuffer.subbuffer(buffer, OFFSET_UART2CAN_DATA, buffer.size() - 1 - OFFSET_UART2CAN_DATA);
        }
        break;
    }
    
    return canBuffer;
}

CCanBuffer CCan232::getRegularCanFrame() {

    CSerialBuffer buffer;
    CCanBuffer canBuffer;
    canBuffer.clear();
    buffer.setCommand(CMD_REQ_GET);

    sendFrame(buffer);
    buffer = getFrame();


    switch (buffer.getResponsedBufferCommand()) {

    case CMD_RESP_NO_DATA:
        break;
    case CMD_RESP_DATA:
        buffer.printDataFrame();
        canBuffer.setCanID(buffer.getCanID());
        if (buffer.size() >= FRAME_UART2CAN_MIN_SIZE){
            canBuffer.subbuffer(buffer, OFFSET_UART2CAN_DATA, buffer.size() - 1 - OFFSET_UART2CAN_DATA);
        }
        break;
    }

    return canBuffer;
}

void CCan232::sendExCanFrame(const CBuffer &can) {
    QMutexLocker locker(&serialPortMutex);
    CSerialBuffer buffer;
    buffer.setCommand(CMD_REQ_TRANS);
    buffer << can.getCanID();
    if (can.getCommand() != 0){
        logger->info(TRANSCEIVER, "Sending CAN frame [ID: " + QString::number(can.getCanID(), 16) + " " + CCanBuffer::shortDescriptionForDestinationID(can.getCanID()) + " , CMD: " + QString::number(can.getCommand()) + ", DATA: " + can.getPrintBuffer(true) + "]");
        buffer << static_cast<unsigned char>(can.getCommand());
        if (can.size() > 7){
            logger->warning(TRANSCEIVER, "Trying to send more than 7 data bytes with 1 command byte in CAN frame: [" + can.getPrintBuffer() + "]");
        }
    }else{
        logger->info(TRANSCEIVER, "Sending CAN frame [ID: " + QString::number(can.getCanID(), 16) + " " + CCanBuffer::shortDescriptionForDestinationID(can.getCanID()) + " , DATA: " + can.getPrintBuffer(true) + "]");
        if (can.size() > 8){
            logger->warning(TRANSCEIVER, "Trying to send more than 8 data bytes in CAN frame: [" + can.getPrintBuffer() + "]");
        }
    }
    buffer << can;
    
    sendFrame(buffer);
    buffer = getFrame();
    switch (buffer.getResponsedBufferCommand()) {
    case CMD_RESP_ACK:
        logger->success(TRANSCEIVER, "CAN frame sent successfully");
        break;
    case CMD_RESP_NACK:
        logger->error(TRANSCEIVER, "CAN frame sending error");
        break;
    }
    
}

void CCan232::sendCanRegularFrame(const CBuffer &can) {

    logger->low_info("Sending CAN regular frame");

    CSerialBuffer buffer;
    buffer.setCommand(CMD_REQ_TRANS_REG);
    buffer << static_cast<unsigned short>(can.getCanID());
    if (can.getCommand() != 0){
        buffer << static_cast<unsigned char>(can.getCommand());
        if (can.size() > 7){
            logger->warning(TRANSCEIVER, "Trying to send more than 7 data bytes with 1 command byte in CAN frame: [" + can.getPrintBuffer() + "]");
        }
    }else{
        if (can.size() > 8){
            logger->warning(TRANSCEIVER, "Trying to send more than 8 data bytes in CAN frame: [" + can.getPrintBuffer() + "]");
        }
    }

    buffer << can;
    sendFrame(buffer);
    buffer = getFrame();

    switch (buffer.getResponsedBufferCommand()) {
    case CMD_RESP_ACK:
        logger->success(TRANSCEIVER, "CAN frame sent successfully");
        break;
    case CMD_RESP_NACK:
        logger->error(TRANSCEIVER, "CAN frame sending error");
        break;
    }

}

CCanBuffer CCan232::sendRegularFrameWithResponse(const CBuffer &can){

    sendCanRegularFrame(can);
    CCanBuffer buffer, tmpBuffer;
    CTimeOut tout;

    tout.startTimeOut(50);

    do{
        buffer = getRegularCanFrame();
    }while(buffer.size() == 0 && ! tout.isTimeOut());

    if (buffer.size() > 0 && buffer[0] == BOOT_COMMAND_ACK){
        do{
            tmpBuffer = getRegularCanFrame();
        }while(tmpBuffer.size() > 0 && tmpBuffer[0] == BOOT_COMMAND_ACK);
    }

    return buffer;
}

void CCan232::configForCanB(){
    
    logger->info(TRANSCEIVER, "Configuring transceiver for CAN B");
    
    
    setExMask(1, MASTER_MASK_1);
    
    setExFilter(2, MASTER_FILTER_N);
    setExFilter(3, MASTER_FILTER_N);
    setExFilter(4, MASTER_FILTER_N);
    setExFilter(5, MASTER_FILTER_N);
    setExFilter(6, MASTER_FILTER_N);
    setCanMode(EEcanMode::ECAN_MODE_1);
//    setExMask(0,0);
    //setExMask(0, MASTER_MASK_0);
//    setExFilter(0, MASTER_FILTER_0);
//    setExFilter(1, MASTER_FILTER_1);
    setCanMode(EEcanMode::ECAN_MODE_1);

    clearRXbuffer();

    logger->success(TRANSCEIVER, "Tranceiver configuration complete");
}

void CCan232::configForCanA(){


    logger->info(TRANSCEIVER, "Configuring transceiver for CAN A");


//    setRegularMask(0, MASTER_MASK_0_CAN_A);
    setRegularMask(1, MASTER_MASK_1_CAN_A);

//    setRegularFilter(0, MASTER_FILTER_0_CAN_A);
//    setRegularFilter(1, MASTER_FILTER_1_CAN_A);
    setRegularFilter(2, MASTER_FILTER_2_CAN_A);
    setRegularFilter(3, MASTER_FILTER_3_CAN_A);
    setRegularFilter(4, MASTER_FILTER_4_CAN_A);
    setRegularFilter(5, MASTER_FILTER_5_CAN_A);
    setCanMode(EEcanMode::ECAN_MODE_0);

    clearRXbuffer();

    logger->success(TRANSCEIVER, "Tranceiver configuration complete");
}



