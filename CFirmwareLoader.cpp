#include "CFirmwareLoader.h"

CFirmwareLoader::CFirmwareLoader()
{
    logger = &CSingleton<CLogger>::Instance();
    config = &CSingleton<CConfig>::Instance();
}

CFirmwareLoader::~CFirmwareLoader()
{

}


void CFirmwareLoader::printFileList() {
    QStringList fileList = getFileList();
    logger->info(UPLOAD_FW, "Firmware files list:");
    int index = 1;
    for (QString fileName : fileList) {
        logger->info(UPLOAD_FW, QString::number(index++) + ".\t" + fileName);
    }
}

QStringList CFirmwareLoader::getFileList() {
    CConfig *config = &CSingleton<CConfig>::Instance();
    CLogger *logger = &CSingleton<CLogger>::Instance();
    QStringList fileList;
    QString path = QDir::currentPath() + "/" + config->getConfigParam(FIRMWARE_PATH);
    QDir dir(path);
    fileList = dir.entryList();
    fileList.removeAll(".");
    fileList.removeAll("..");
    if (fileList.size() == 0) {
        logger->error(UPLOAD_FW, "Reading firmware path failed or directory not found");
    }
    return fileList;
}

CFirmwareBuffer CFirmwareLoader::readFile(QString filename) {

    RawData data;
    CFirmwareBuffer buffer;
    CBuffer dataBuffer;
    unsigned int exAddress = 0;
    QString fileContent;
    QFile myfile(config->getConfigParam(FIRMWARE_PATH) + filename);
    myfile.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream in(&myfile);

    if (myfile.isOpen()) {
        logger->info(UPLOAD_FW, "Starting firmware [" + filename + "] loading...");
        while (!in.atEnd()) {
            fileContent = "";
            fileContent = in.readLine();
            if (fileContent != "") {
                data = parseData(fileContent);
                if (checkInputData(data)) {
                    switch (data[OFFSET_FIRMWARE_TYPE]) {
                        case TYPE_DATA:
//                            cout << "TYPE_DATA" << endl;
                            dataBuffer = getData(data);
                            if (dataBuffer.size()) {
                                if (exAddress) {
                                    buffer.addExBufferData(exAddress | getAddress(data), dataBuffer);
                                } else {
                                    buffer.addData(getAddress(data), dataBuffer);
                                }
                            }
                            break;
                        case TYPE_EXADR:
//                            cout << "TYPE_EXADR" << endl;
                            exAddress = getHighExtendedAddress(data);
                            break;
                        case TYPE_EOF:
//                            cout << "TYPE_EOF" << endl;
                            myfile.close();
                            return buffer;
                            break;
                    }

                } else {
                    throw CException(QString("Incorrect firmware file, broken in line: ") + fileContent);
                }
            }
        }
        myfile.close();
    } else
        throw CException(QString("Unable to open file ") + filename, this);

    return buffer;

}

bool CFirmwareLoader::checkCRC(RawData &input) {
    unsigned char crc = 0;
    for (unsigned char d : input) {
        crc += d;
    }
    return crc == 0;
}

bool CFirmwareLoader::checkInputData(RawData &input) {
    if (input.size() >= 5) {
        int inputSize = input[OFFSET_FIRMWARE_LENGTH] + 5;
        return (inputSize == input.size()) && checkCRC(input);
    }
    return false;
}

unsigned int CFirmwareLoader::getAddress(RawData &input) {
    unsigned int address = 0;
    if (input.size() >= 4) {
        address |= ((input[OFFSET_FIRMWARE_ADDR] << 8) & 0xff00);
        address |= ((input[OFFSET_FIRMWARE_ADDR + 1] << 0) & 0x00ff);
    }
    return address;
}

unsigned int CFirmwareLoader::getHighExtendedAddress(RawData &input) {
    unsigned int address = 0;
    if (input.size() >= 6) {
        address |= ((input[OFFSET_FIRMWARE_DATA] << 8) & 0xff00);
        address |= ((input[OFFSET_FIRMWARE_DATA + 1] << 0) & 0x00ff);
    }
    address <<= 16;
    return address & 0xffff0000;
}

CBuffer CFirmwareLoader::getData(RawData &input) {
    CBuffer buffer;
    if (input.size() > 5) {
        for (size_t i = 0; i < input[OFFSET_FIRMWARE_LENGTH]; i++) {
            buffer << (unsigned char) input[OFFSET_FIRMWARE_DATA + i];
        }
    }
    return buffer;
}

RawData CFirmwareLoader::parseData(QString &input) {
    RawData data;
    int index = 0;
    if (input[index] == ':') {
        index++;
        while (index <= (input.length() - 2)) {
            //data.push_back(static_cast<unsigned char> (fromQString<unsigned int>(input.substr(index, 2), 1)));
            bool ok;
            data.push_back(static_cast<unsigned char> ( input.mid(index, 2).toInt(&ok, 16)));  //substr(index, 2), 1)  );
            if (!ok){
                throw CException(QString("Number conversion error"), this);
            }
            index += 2;
        }
    }
    return data;
}
