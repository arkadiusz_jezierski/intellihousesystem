#include "CLogger.h"

CLogger::CLogger(QObject *parent) : QObject(parent)
{
    openlog("IntelliHouseSystem", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL7);
}

CLogger::~CLogger()
{
    closelog();
    qDebug("Logger destructor");
}

QString CLogger::getTime(){
    return QDateTime::currentDateTime().toString("yy/MM/dd-HH:mm:ss.zzz");
}

void CLogger::low_info(QString msg){
    QString dateTime = getTime();
    log2syslogAndSentToExtSystem("[LOW_INFO  ][" + dateTime + "]: " + msg);
}

void CLogger::info(QString msg){
    QString dateTime = getTime();
    log2syslogAndSentToExtSystem("[INFO      ][" + dateTime + "]: " + msg);
}

void CLogger::warning(QString msg){
    QString dateTime = getTime();
    log2syslogAndSentToExtSystem("[WARNING   ][" + dateTime + "]: " + msg);
}

void CLogger::error(QString msg){
    QString dateTime = getTime();
    log2syslogAndSentToExtSystem("[ERROR     ][" + dateTime + "]: " + msg);
}

void CLogger::success(QString msg){
    QString dateTime = getTime();
    log2syslogAndSentToExtSystem("[SUCCESS   ][" + dateTime + "]: " + msg);
}

void CLogger::debug(QString msg){
    QString dateTime = getTime();
    log2syslogAndSentToExtSystem("[DEBUG     ][" + dateTime + "]: " + msg);
}

void CLogger::posterLoggerLog(QString msg) {
    QString dateTime = getTime();
    log2syslogOnly("[INFO      ][" + dateTime + "]: <" + LOGPOSTER + "> " + msg);
}

void CLogger::low_info(QString source, QString msg){
    QString dateTime = getTime();
    log2syslogAndSentToExtSystem("[LOW_INFO  ][" + dateTime + "]: <" + source + "> " + msg);
}

void CLogger::info(QString source, QString msg){
    QString dateTime = getTime();
    log2syslogAndSentToExtSystem("[INFO      ][" + dateTime + "]: <" + source + "> " + msg);
}

void CLogger::warning(QString source, QString msg){
    QString dateTime = getTime();
    log2syslogAndSentToExtSystem("[WARNING   ][" + dateTime + "]: <" + source + "> " + msg);
}

void CLogger::error(QString source, QString msg){
    QString dateTime = getTime();
    log2syslogAndSentToExtSystem("[ERROR     ][" + dateTime + "]: <" + source + "> " + msg);
}

void CLogger::success(QString source, QString msg){
    QString dateTime = getTime();
    log2syslogAndSentToExtSystem("[SUCCESS   ][" + dateTime + "]: <" + source + "> " + msg);
}

void CLogger::debug(QString source, QString msg){
    QString dateTime = getTime();
    log2syslogAndSentToExtSystem("[DEBUG     ][" + dateTime + "]: <" + source + "> " + msg);
}

void CLogger::log2syslog(QString msg){
    QByteArray array;
    array.append(msg);
    syslog(LOG_NOTICE, "%s", array.constData());
}

 void CLogger::log2syslogOnly(QString msg) {
    QMutexLocker locker(&loggerMutex);
    log2syslog(msg);
 }

 void CLogger::log2syslogAndSentToExtSystem(QString msg) {
    QMutexLocker locker(&loggerMutex);
    log2syslog(msg);
    for (QString filter : filters) {
        if (msg.contains(filter, Qt::CaseInsensitive)) {
            emit postLog(msg);
        }
    }
 }

void CLogger::setFilters(QList<QString> filters) {
    this->filters.clear();
    this->filters = filters;
}