#include "CMainApp.h"

CMainApp::CMainApp(QObject *parent) : QObject(parent)
{
    logger = &CSingleton<CLogger>::Instance();

}

CMainApp::~CMainApp()
{
    qDebug("Main app destructor");
    if (transceiver != NULL){
        delete transceiver;
        transceiver = NULL;
    }
    if (deviceManager != NULL){
        delete deviceManager;
        deviceManager = NULL;
    }
    if (eventManager != NULL){
        delete eventManager;
        eventManager = NULL;
    }

    if (gsmModem != NULL){
        delete gsmModem;
        gsmModem = NULL;
    }
    if (restService != NULL) {
        delete restService;
        restService = NULL;
    }
    if (loggerPoster != NULL) {
        delete loggerPoster;
        loggerPoster = NULL;
    }

    if (restClient != NULL) {
        delete restClient;
        restClient = NULL;
    }
}

void CMainApp::assignTransceiver(CTransceiver *transceiver){
    this->transceiver = transceiver;
}

void CMainApp::pollTransceiver(){

    CCanBuffer buffer;
    runMainThread = true;

    while(runMainThread){
        while(pausedThread){
            deviceManager->uploadFirmware();
            QThread::msleep(100);
        }

        try{
            buffer = transceiver->getExCanMessage();
            dispatchCanMessage(buffer);
            deviceManager->pingDevice();
            deviceManager->checksDevicesAvailability();
            deviceManager->checkDeviceParameterRequestQueue();
            deviceManager->checkActionsQueue();
            deviceManager->checkExpectedDeviceParameterStatus();


        }catch(CException ex){
            logger->error(SYSTEM, "Exception: " + ex.what());
            transceiver->open();
            QThread::msleep(1000);
        }

        QThread::msleep(1);

    }
    logger->debug(SYSTEM, "Main thread finished");
}


void CMainApp::dispatchCanMessage(const CCanBuffer &message){

    if (message.getFrameType() == CCanBuffer::EFrameType::ADDRESS_REQUEST){
        logger->low_info(TRANSCEIVER, "DISPATCHER: Request addresses frame received");
        deviceManager->assignAddresses(message);
        assignEventManager(deviceManager->getCanDevices(), eventManager);
    }else if (message.getFrameType() == CCanBuffer::EFrameType::ERROR_INFO){
        logger->low_info(TRANSCEIVER, "DISPATCHER: Error frame received");
        errorMessageHandler(message);
    }else if (message.getFrameType() == CCanBuffer::EFrameType::DEV_STARTING_UP){
        logger->low_info(TRANSCEIVER, "DISPATCHER: Device starting up frame received");
        devStartingUpHandler(message);
    }else if (message.getFrameType() == CCanBuffer::EFrameType::REGULAR){

        if (message.getCommand() == CMD_CAN_PONG){
            logger->low_info(TRANSCEIVER, "DISPATCHER: Pong frame received");
            deviceManager->pingResponse(message);
        }else{
            logger->low_info(TRANSCEIVER, "DISPATCHER: Regular frame received");
            deviceManager->executeActionForCommand(message);
            if(message.getSourceCategoryTypeId() == CATEGORY_TYPE_SENSOR){
                CCanDevice *device = deviceManager->getCanDevice(message);
                if (device != NULL){
                    device->sendACKForSensor(message);
                }
            }
        }
    }
}


void CMainApp::errorMessageHandler(const CCanBuffer &buffer){
    try{
        CCanDevice *device = deviceManager->getFirstCanDevice(buffer.getSourceUID());
        if (device != NULL){
            logger->error(TRANSCEIVER, "Error occured in device with UID: " 
                + QString::number(buffer.getSourceUID(), 16) 
                + QString(" (") + device->objectName() + QString(")")
            );

            CCanDevice::printDeviceErrorInfo(buffer);
            deviceManager->clearDeviceStatus(buffer.getSourceUID());
            device->sendErrorFrameACK();
        }
    }catch(CException ex){
        logger->error(TRANSCEIVER, "Error in error message handler: " + ex.what());
    }
}

void CMainApp::devStartingUpHandler(const CCanBuffer &buffer){
    unsigned int uid = buffer.getSourceUID();
    SDeviceStartupInfo startupInfo;
    startupInfo.startupInfo = (EStartupInfoType)buffer.getStartupInfo();
    startupInfo.RCONRegister = buffer.getRCONRegisterValue();
    deviceManager->handleStartingUpDevice(uid, startupInfo);
}

void CMainApp::assignEventManager(QList<CCanDevice *> devicesList, CEventManager *eventManager){

    qRegisterMetaType<DevCat>("DevCat");

    for (CCanDevice *device : devicesList){
        if (device->assignedToEventManager){
            continue;
        }

        if (eventManager != NULL){
            connect(device, SIGNAL(sendEvent(DevCat,unsigned char,QString,qint64)), eventManager, SLOT(addNewDeviceEvent(DevCat,unsigned char,QString,qint64)));
            device->assignedToEventManager = true;
        }else{
            logger->warning(SYSTEM, "Trying to assign invalid pointer of Event Manager");
        }
    }
}

void CMainApp::pauseThread(){
    pausedThread = true;
}

void CMainApp::resumeThread(){
    pausedThread = false;
}

void CMainApp::run(){
    logger->info(SYSTEM, "Start application");
    config = &CSingleton<CConfig>::Instance();
    if (!config->isConfigComplete()){
        logger->error(SYSTEM, "Configuration INCOMPLETE, system cannot be started");
        return;
    }

    gsmModem = new CGsmModem;

    transceiver = new CCan232;
    if (!transceiver->open()){
        logger->warning(SYSTEM, "No transceiver device found");
    }

    loggerPoster = new CLoggerPoster();
    restClient = new CRestClientService();

    eventManager = new CEventManager;
    deviceManager = &CSingleton<CDeviceManager>::Instance();
    restService = &CSingleton<CRestApiService>::Instance();

    deviceManager->assignTransceiverPointer(transceiver);
    
    qRegisterMetaType<DevCat>("DevCat");
    qRegisterMetaType<UID>("UID");


    connect(this, SIGNAL(killThreads()), eventManager, SLOT(killThread()));
    connect(this, SIGNAL(killThreads()), gsmModem, SLOT(killThread()));
    connect(this, SIGNAL(killThreads()), restService, SLOT(killThread()));
    connect(this, SIGNAL(killThreads()), loggerPoster, SLOT(killThread()));
    connect(this, SIGNAL(killThreads()), restClient, SLOT(killThread()));
    connect(this, SIGNAL(pauseStopThreads()), eventManager, SLOT(killThread()));
    connect(this, SIGNAL(pauseStopThreads()), gsmModem, SLOT(killThread()));
    connect(this, SIGNAL(pauseStopThreads()), restService, SLOT(pauseThread()));
    connect(this, SIGNAL(pauseStopThreads()), loggerPoster, SLOT(killThread()));
    connect(this, SIGNAL(pauseStopThreads()), restClient, SLOT(killThread()));
    connect(this, SIGNAL(unpauseThreads()), restService, SLOT(unpauseThread()));
    connect(deviceManager, SIGNAL(sendPauseThreadsSignal()), this, SLOT(pauseThread()), Qt::DirectConnection);
    connect(deviceManager, SIGNAL(sendPauseThreadsSignal()), eventManager, SLOT(pauseThread()), Qt::DirectConnection);
    connect(deviceManager, SIGNAL(sendResumeThreadSignal()), this, SLOT(resumeThread()), Qt::DirectConnection);
    connect(deviceManager, SIGNAL(sendResumeThreadSignal()), eventManager, SLOT(resumeThread()), Qt::DirectConnection);
    connect(eventManager, SIGNAL(sendDeviceParameterRequest(QString,QString)), deviceManager, SLOT(requestDeviceParameter(QString,QString)));
    connect(eventManager, SIGNAL(sendDeviceAction(DevCat,unsigned char,QString,qint64)), deviceManager, SLOT(addNewDeviceParameterStatus(DevCat,unsigned char,QString,qint64)));
    connect(eventManager, SIGNAL(sendMessageSignal(QString,QString)), gsmModem, SLOT(addNewMessage(QString,QString)));
    connect(deviceManager, SIGNAL(sendDevicesListChangedSignal()), eventManager, SLOT(reloadOperationList()));
    connect(eventManager, SIGNAL(sendPostRequest(QString,QString)), restClient, SLOT(sendPostRequest(QString,QString)));

    connect(restService, SIGNAL(sendDeviceAction(DevCat,unsigned char,QString,qint64)), deviceManager, SLOT(addNewDeviceParameterStatus(DevCat,unsigned char,QString,qint64)));
    connect(restService, SIGNAL(sendDeviceParameterRequest(DevCat,unsigned char,QString)), deviceManager, SLOT(requestDeviceParameter(DevCat,unsigned char,QString)));
    connect(restService, SIGNAL(sendBootDeviceSignal(UID)), deviceManager, SLOT(bootDevice(UID)));
    connect(restService, SIGNAL(sendRemoveDeviceSignal(UID)), this, SLOT(removeDevice(UID)));
    connect(restService, SIGNAL(sendRemoveDeviceAdvanceSignal(DevCat, int, int)), this, SLOT(removeDevice(DevCat, int, int)));
    connect(restService, SIGNAL(sendUploadFirmwareSignal(QString)), deviceManager, SLOT(startUploadFirmware(QString)));
    connect(restService, SIGNAL(sendInitDeviceSignal()), deviceManager, SLOT(resetAddressesInDevices()));
    connect(restService, SIGNAL(sendMessageSignal(QString,QString)), gsmModem, SLOT(addNewMessage(QString,QString)));
    connect(restService, SIGNAL(sendReloadOperationsSignal()), eventManager, SLOT(reloadOperationList()));
    connect(restService, SIGNAL(sendReloadLogFiltersSignal()), loggerPoster, SLOT(reloadFiltersList()));

    connect(logger, SIGNAL(postLog(QString)), loggerPoster, SLOT(postLog(QString)));

    deviceManager->loadDevices();   // must be executed after connecting deviceManager to eventManager
    assignEventManager(deviceManager->getCanDevices(), eventManager);   //must be executed after devices loading
    deviceManager->configureDevices();
    runThreads();

}

void CMainApp::quit(){
    logger->info(SYSTEM, "Stopping system...");
    emit killThreads();

    runMainThread = false;
    mainThread.waitForFinished();
    deviceManagerThread.waitForFinished();
    eventsThread.waitForFinished();
    gsmModuleThread.waitForFinished();
    restServiceThread.waitForFinished();
    loggerPosterThread.waitForFinished();
    restClientServiceThread.waitForFinished();
    logger->info(SYSTEM, "System is down");
}


void CMainApp::runThreads(){
    logger->info(SYSTEM, "Starting all 6 threads...");
    QThreadPool::globalInstance()->setMaxThreadCount(5);
    mainThread =  QtConcurrent::run(this, &CMainApp::pollTransceiver);
    gsmModuleThread = QtConcurrent::run(gsmModem, &CGsmModem::runGsmModuleService);
    eventsThread = QtConcurrent::run(eventManager, &CEventManager::runEventManager);
    restServiceThread =  QtConcurrent::run(restService, &CRestApiService::run);
    loggerPosterThread =  QtConcurrent::run(loggerPoster, &CLoggerPoster::runLoggerPoster);
    restClientServiceThread =  QtConcurrent::run(restClient, &CRestClientService::runRestClientService);
}

void CMainApp::runPausedAndStoppedThreads() {
    logger->info(SYSTEM, "Starting all 5 stopped threads...");
    emit unpauseThreads();
    QThreadPool::globalInstance()->setMaxThreadCount(5);
    if (!mainThread.isRunning()) {
        mainThread =  QtConcurrent::run(this, &CMainApp::pollTransceiver);
    }

    if (!gsmModuleThread.isRunning()) {
        gsmModuleThread = QtConcurrent::run(gsmModem, &CGsmModem::runGsmModuleService);
    }

    if (!eventsThread.isRunning()) {
        eventsThread = QtConcurrent::run(eventManager, &CEventManager::runEventManager);
    }

    if (!loggerPosterThread.isRunning()) {
        loggerPosterThread = QtConcurrent::run(loggerPoster, &CLoggerPoster::runLoggerPoster);
    }

    if (!restClientServiceThread.isRunning()) {
        restClientServiceThread = QtConcurrent::run(restClient, &CRestClientService::runRestClientService);
    }
}

bool CMainApp::stopThreads(){
    logger->info(SYSTEM, "Stopping/pausing threads...");
    runMainThread = false;
    emit pauseStopThreads();    //dont kill rest service

    int i = 0;
    while(mainThread.isRunning()
          || eventsThread.isRunning()
          || gsmModuleThread.isRunning()
          || loggerPosterThread.isRunning()
          || restClientServiceThread.isRunning()){
        i++;
        QThread::msleep(100);
        if (i > 30){
            QString threads = "";
            threads += (mainThread.isRunning()) ? "main, " : "";
            threads += (eventsThread.isRunning()) ? "events, " : "";
            threads += (gsmModuleThread.isRunning()) ? "gsm, " : "";
            threads += (loggerPosterThread.isRunning()) ? "loggerposter, " : "";
            threads += (restClientServiceThread.isRunning()) ? "restclient, " : "";

            if (threads.length() > 0) {
                threads = threads.left(threads.length() - 2);
            }

            logger->warning(SYSTEM, "Threads still running [" + threads + "]");
            return false;
        }
    }


    logger->info(SYSTEM, "All threads stopped/paused");
    return true;

}

void CMainApp::removeDevice(UID uid){
    if (stopThreads()){
        deviceManager->removeDevice(uid);
    } else {
        logger->error(DEVICE_MANAGER, "Device has not been removed - threads cannot be stopped");
    }

    runPausedAndStoppedThreads();
}

void CMainApp::removeDevice(DevCat category, int addressBegin, int addressEnd){
    if (stopThreads()){
        deviceManager->removeDevice(category, addressBegin, addressEnd);
    } else {
        logger->error(DEVICE_MANAGER, "Device has not been removed - threads cannot be stopped");
    }

    runPausedAndStoppedThreads();
}
