#include "CSerialPortManager.h"

CSerialPortManager::CSerialPortManager(QObject *parent) : QObject(parent)
{

}

CSerialPortManager::~CSerialPortManager()
{

}

void CSerialPortManager::setPortBusy(QString portName){
    busyPorts.push_back(portName);
}

void CSerialPortManager::releasePort(QString portName){
    busyPorts.removeAll(portName);
}

QList<QString> CSerialPortManager::getAvailablePorts(){
    QList<QString> ports;

    for (auto port : QSerialPortInfo::availablePorts()){
        QString portName = "/dev/" + port.portName();
        if (!busyPorts.contains(portName)) {
            ports.push_back(portName);
        }
    }

    return ports;
}
