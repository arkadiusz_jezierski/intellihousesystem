#include "CDeviceAction.h"


CDeviceAction::CDeviceAction(CCanDevice *device, QString param, qint64 val) : CAction() {
    this->device = device;
    this->parameter = param;
    this->value = val;
    this->actionType = EActionType::DEVICE_ACTION;
}


CDeviceAction::~CDeviceAction()
{

}

QString CDeviceAction::toString(){
    QString output = "Device Action [SET " + this->device->objectName() + " PARAMETER " + this->parameter + " TO " + QString::number(this->value) + "]";
    return output;
}

