#include "CTimeCondition.h"

CTimeCondition::CTimeCondition(ELogicalConditionType condition, qint64 val): CCondition()
{
    this->value = val;
    this->logicalCondition = condition;
    this->type = EConditionType::TIME;
}



QString CTimeCondition::toString() {
    QString str = "";
    if (this->extLogicConditionId != "") {
        str = this->extLogicConditionId + ": ";
    }
    str += "Time Condition [IF current time " +
    CCondition::conditionToString(this->logicalCondition) + " " + QString::number(this->value) + "(" + convertTimeValue(value) + ")]";

    return str;
}

void CTimeCondition::calculateCondition() {
	QTime current = QTime::currentTime();
    if (compareTimeValues(current.msecsSinceStartOfDay() / 1000, this->value, this->logicalCondition)) {
        logger->debug(EVENT_MANAGER, "POSITIVE comparison, comparison result: TRUE");
        this->result = EConditionResult::True_Val;
    }else{
        logger->debug(EVENT_MANAGER, "NEGATIVE comparison, comparison result: FALSE");
        this->result = EConditionResult::False_Val;
    }
}


bool CTimeCondition::compareTimeValues(qint64 value1, qint64 value2, ELogicalConditionType condition) {

    QString firstValLabel = "current time value";
    QString secondValLabel = "expected time value";

    switch(condition) {
    case ELogicalConditionType::EQUAL:
        logger->debug(EVENT_MANAGER, "comparison " + firstValLabel + " " + convertTimeValue(value1) + " = " + secondValLabel + " " + convertTimeValue(value2));
        return value1 == value2;
        break;
    case ELogicalConditionType::GREATER_OR_EQUAL:
        logger->debug(EVENT_MANAGER, "comparison " + firstValLabel + " " + convertTimeValue(value1) + " >= " + secondValLabel + " " + convertTimeValue(value2));
        return value1 >= value2;
        break;
    case ELogicalConditionType::GREATER_THAN:
        logger->debug(EVENT_MANAGER, "comparison " + firstValLabel + " " + convertTimeValue(value1) + " > " + secondValLabel + " " + convertTimeValue(value2));
        return value1 > value2;
        break;
    case ELogicalConditionType::LESS_OR_EQUAL:
        logger->debug(EVENT_MANAGER, "comparison " + firstValLabel + " " + convertTimeValue(value1) + " <= " + secondValLabel + " " + convertTimeValue(value2));
        return value1 <= value2;
        break;
    case ELogicalConditionType::LESS_THAN:
        logger->debug(EVENT_MANAGER, "comparison " + firstValLabel + " " + convertTimeValue(value1) + " < " + secondValLabel + " " + convertTimeValue(value2));
        return value1 < value2;
        break;
    case ELogicalConditionType::NOT_EQUAL:
        logger->debug(EVENT_MANAGER, "comparison " + firstValLabel + " " + convertTimeValue(value1) + " <> " + secondValLabel + " " + convertTimeValue(value2));
        return value1 != value2;
        break;
    case ELogicalConditionType::UNDEFINED:
        return false;
        break;
    case ELogicalConditionType::ANY:
    case ELogicalConditionType::RISING:
    case ELogicalConditionType::FALLING:
        break;
    }
    return false;
}


QString CTimeCondition::convertTimeValue(qint64 val) {
    return CTools::secondsSinceMidnightToHourFormat(val);
}
