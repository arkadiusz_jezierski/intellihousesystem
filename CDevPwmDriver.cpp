#include "CDevPwmDriver.h"

CDevPwmDriver::CDevPwmDriver()
{
    this->setCategory(DevCat::PWM);

    initParameters();
    initCommandMethods();
    initRequestMethods();
    initExecuteMethods();
}

CDevPwmDriver::~CDevPwmDriver()
{

}


void CDevPwmDriver::initParameters(){
    Value val, justForCommand;
    val.undefined = true;
    val.value = 0;
    val.changes = EParamChanges::None;

    justForCommand.undefined = false;

    parameters[PARAM_DUTY_CYCLE] = val;
    commands[PARAM_DUTY_CYCLE] = val;
    commands["dutyCycleAll"] = justForCommand;
    commands["dutyCycleAllTheSame"] = justForCommand;
    commands["dutyUp"] = justForCommand;
    commands["dutyDown"] = justForCommand;
    commands["dutyUpAll"] = justForCommand;
    commands["dutyDownAll"] = justForCommand;
    commands[PARAM_OFF] = justForCommand;

}

void CDevPwmDriver::commandMethodOfPwmDriver(COMMAND cmd, CBuffer &data){

    if (methodsMap.contains(cmd)){
        try{
            (this->*methodsMap[cmd])(data);
        }catch(CException ex){
            logger->error(DEVICE_MANAGER, "Error in PwmDriver: " + ex.what());
        }
    }else{
        logger->warning(DEVICE_MANAGER, "No method to invoke for command " + QString::number((int)cmd));
    }
}


void CDevPwmDriver::requestParameterForPwmDriver(QString param){

    if (requestMethodsMap.contains(param)){
        try{
            (this->*requestMethodsMap[param])();
        }catch(CException ex){
            logger->error(DEVICE_MANAGER, "Error in PwmDriver: " + ex.what());
        }
    }else{
        logger->warning(DEVICE_MANAGER, "PwmDriver: No request to invoke for parameter " + param);
    }
}


SCommandSet CDevPwmDriver::executeCommandForPwmDriver(SCommandSet commSet){
    SCommandSet retVal;
    if (executeDeviceCommandMap.contains(commSet.parameter)){
        try{
            retVal = (this->*executeDeviceCommandMap[commSet.parameter])(commSet.value);
        }catch(CException ex){
            logger->error(DEVICE_MANAGER, "Error in PwmDriver: " + ex.what());
        }
    }else{
        logger->warning(DEVICE_MANAGER, "PwmDriver: No execution to invoke for parameter " + commSet.parameter);
    }
    return retVal;
}



void CDevPwmDriver::initCommandMethods(){
    methodsMap[CMD_CAN_SET_PWM] = &CDevPwmDriver::setDutyCycle;
    methodsMap[CMD_CAN_GET_PWM] = &CDevPwmDriver::getDutyCycle;
}

void CDevPwmDriver::initRequestMethods(){
    requestMethodsMap[PARAM_DUTY_CYCLE] = &CDevPwmDriver::requestDutyCycle;
}

void CDevPwmDriver::initExecuteMethods(){

    executeDeviceCommandMap[PARAM_DUTY_CYCLE] = &CDevPwmDriver::executeSetDutyCycle;
    executeDeviceCommandMap[PARAM_OFF] = &CDevPwmDriver::executeSetDutyCycleZero;
    executeDeviceCommandMap["dutyCycleAll"] = &CDevPwmDriver::executeSetDutyCycleForCoupleDevices;
    executeDeviceCommandMap["dutyCycleAllTheSame"] = &CDevPwmDriver::executeSetSameDutyCycleForCoupleDevices;
    executeDeviceCommandMap["dutyUp"] = &CDevPwmDriver::executeIncreaseDutyCycle;
    executeDeviceCommandMap["dutyDown"] = &CDevPwmDriver::executeDecreaseDutyCycle;
    executeDeviceCommandMap["dutyUpAll"] = &CDevPwmDriver::executeIncreaseDutyCycleForCoupleDevices;
    executeDeviceCommandMap["dutyDownAll"] = &CDevPwmDriver::executeDecreaseDutyCycleForCoupleDevices;

}


void CDevPwmDriver::getDutyCycle(CBuffer &data){
    logger->info(DEVICE_MANAGER, this->objectName() + "->getDutyCycle(" + QString::number(data[OFFSET_CAN_PWM_VALUE]) + ")");

    Value val;
    val.undefined = false;
    val.value = data[OFFSET_CAN_PWM_VALUE];
    val.changes = checkValueChanged(PARAM_DUTY_CYCLE, val.value);

    parameters[PARAM_DUTY_CYCLE] = val;

    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_DUTY_CYCLE, val.value);
}

void CDevPwmDriver::setDutyCycle(CBuffer &data){
    logger->info(DEVICE_MANAGER, this->objectName() + "->setDutyCycle(" + QString::number(data[OFFSET_CAN_PWM_VALUE]) + ")");

    Value val;
    val.undefined = false;
    val.value = data[OFFSET_CAN_PWM_VALUE];
    val.changes = checkValueChanged(PARAM_DUTY_CYCLE, val.value);

    parameters[PARAM_DUTY_CYCLE] = val;

    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_DUTY_CYCLE, val.value);
}

void CDevPwmDriver::requestDutyCycle(){
    logger->info(DEVICE_MANAGER, this->objectName() + "->requestDutyCycle()");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_GET_PWM);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
}

SCommandSet CDevPwmDriver::executeSetDutyCycle(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeSetDutyCycle(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_SET_PWM);
    buffer << static_cast<unsigned char>(value);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;
    commSet.parameter = PARAM_DUTY_CYCLE;
    commSet.value = value;

    return commSet;
}

SCommandSet CDevPwmDriver::executeSetDutyCycleZero(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeSetDutyCycleZero(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_ALL_OFF);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
    SCommandSet commSet;
    commSet.parameter = PARAM_DUTY_CYCLE;
    commSet.value = 0;

    return commSet;
}


SCommandSet CDevPwmDriver::executeSetDutyCycleForCoupleDevices(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeSetDutyCycleForCoupleDevices(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_SET_PWM_ALL);
    buffer.addPwmValuesToBuffer(value);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;
    commSet.parameter = PARAM_DUTY_CYCLE;
    commSet.value = (value >> 48) & 0xff;

    return commSet;
}

SCommandSet CDevPwmDriver::executeSetSameDutyCycleForCoupleDevices(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeSetSameDutyCycleForCoupleDevices(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_SET_PWM_ALL_THE_SAME);
    buffer << static_cast<unsigned char>(value);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;
    commSet.parameter = PARAM_DUTY_CYCLE;
    commSet.value = value;

    return commSet;
}

SCommandSet CDevPwmDriver::executeIncreaseDutyCycleForCoupleDevices(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeIncreaseDutyCycleForCoupleDevices(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_PWM_UP_ALL);
    buffer << static_cast<unsigned char>(value);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;

    if (!parameters[PARAM_DUTY_CYCLE].undefined){
        commSet.parameter = PARAM_DUTY_CYCLE;
        if (parameters[PARAM_DUTY_CYCLE].value > (255 - value)){
            commSet.value = 255;
        }else{
            commSet.value = parameters[PARAM_DUTY_CYCLE].value + value;
        }
    }else{
        commSet.expected = false;
    }

    return commSet;
}

SCommandSet CDevPwmDriver::executeIncreaseDutyCycle(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeIncreaseDutyCycle(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_PWM_UP);
    buffer << static_cast<unsigned char>(value);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;

    if (!parameters[PARAM_DUTY_CYCLE].undefined){
        commSet.parameter = PARAM_DUTY_CYCLE;
        if (parameters[PARAM_DUTY_CYCLE].value > (255 - value)){
            commSet.value = 255;
        }else{
            commSet.value = parameters[PARAM_DUTY_CYCLE].value + value;
        }
    }else{
        commSet.expected = false;
    }

    return commSet;
}

SCommandSet CDevPwmDriver::executeDecreaseDutyCycleForCoupleDevices(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeDecreaseDutyCycleForCoupleDevices(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_PWM_DOWN_ALL);
    buffer << static_cast<unsigned char>(value);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;

    if (!parameters[PARAM_DUTY_CYCLE].undefined){
        commSet.parameter = PARAM_DUTY_CYCLE;
        if (parameters[PARAM_DUTY_CYCLE].value < value){
            commSet.value = 0;
        }else{
            commSet.value = parameters[PARAM_DUTY_CYCLE].value - value;
        }
    }else{
        commSet.expected = false;
    }

    return commSet;
}

SCommandSet CDevPwmDriver::executeDecreaseDutyCycle(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeDecreaseDutyCycle(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_PWM_DOWN);
    buffer << static_cast<unsigned char>(value);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;

    if (!parameters[PARAM_DUTY_CYCLE].undefined){
        commSet.parameter = PARAM_DUTY_CYCLE;
        if (parameters[PARAM_DUTY_CYCLE].value < value){
            commSet.value = 0;
        }else{
            commSet.value = parameters[PARAM_DUTY_CYCLE].value - value;
        }
    }else{
        commSet.expected = false;
    }

    return commSet;
}
