#ifndef CALARMCONDITION_H
#define CALARMCONDITION_H

#include "CCondition.h"

class CAlarmCondition : public CCondition
{

	void calculateCondition();
	
public:
    CAlarmCondition(bool armed);

    bool operator == (const CAlarmCondition &event);

    bool systemArmed;

    QString toString();
};

#endif // CALARMCONDITION_H
