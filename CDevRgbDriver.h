#ifndef CDEVRGBDRIVER_H
#define CDEVRGBDRIVER_H


#include <QObject>
#include <CCanDevice.h>

#define PARAM_SWITCH    "switch"
#define PARAM_MODE      "mode"
#define PARAM_SPEED     "speed"
#define PARAM_OFF       "off"
#define PARAM_RGB       "rgb"
#define PARAM_RED       "red"
#define PARAM_GREEN     "green"
#define PARAM_BLUE      "blue"


class CDevRgbDriver : public CCanDevice
{
    Q_OBJECT
public:
    explicit CDevRgbDriver();
    ~CDevRgbDriver();

    void initParameters();
    void initCommandMethods();
    void initRequestMethods();
    void initExecuteMethods();

    typedef void (CDevRgbDriver::*method)(CBuffer &data);
    QMap<COMMAND, method> methodsMap;

    typedef void (CDevRgbDriver::*requestMethod)();
    QMap<QString, requestMethod> requestMethodsMap;

    typedef SCommandSet (CDevRgbDriver::*executeDeviceCommand)(qint64);
    QMap<QString, executeDeviceCommand> executeDeviceCommandMap;

    void setAllRgbValues(CBuffer &data);
    void setMode(CBuffer &data);
    void switchMode(CBuffer &data);
    void setSpeed(CBuffer &data);
    // void getAllRgbValues(CBuffer &data);
    void getRedValue(CBuffer &data);
    void getGreenValue(CBuffer &data);
    void getBlueValue(CBuffer &data);
    void getMode(CBuffer &data);
    void getSpeed(CBuffer &data);

    void requestRedValue();
    void requestGreenValue();
    void requestBlueValue();
    void requestRgbValues();
    void requestMode();
    void requestSpeed();

    SCommandSet executeSetRedValue(qint64 value);
    SCommandSet executeSetGreenValue(qint64 value);
    SCommandSet executeSetBlueValue(qint64 value);
    SCommandSet executeSetRgbValues(qint64 value);
    SCommandSet executeSetMode(qint64 value);
    SCommandSet executeSetSpeed(qint64 value);
    SCommandSet executeSetRgbOff(qint64 value);
    SCommandSet executeSwitchMode(qint64 value);

private:
    unsigned long convertBuffer(CBuffer &buf);

public slots:
    void commandMethodOfRgbDriver(COMMAND cmd, CBuffer &data);
    void requestParameterForRgbDriver(QString param);
    SCommandSet executeCommandForRgbDriver(SCommandSet commSet);

};

#endif // CDEVRGBDRIVER_H
