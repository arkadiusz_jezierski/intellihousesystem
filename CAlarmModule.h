#ifndef CALARMMODULE_H
#define CALARMMODULE_H

#include <QObject>
#include <QMutex>
#include <CLogger.h>
#include <CConfig.h>
#include <QCryptographicHash>

class CAlarmModule : public QObject
{
    Q_OBJECT

    mutable QMutex alarmFlagMutex;

    CLogger *logger;

    QString salt;
    QString code;
    qint64 alarmStartTime = 0;
    qint64 alarmDelayTime = 0;

    QString getMd5(QString input);
    bool checkUnlockCode(QString code);

public:

    void armSystem();
    bool disarmSystem(QString code);
    bool systemIsArmed();

    explicit CAlarmModule(QObject *parent = 0);
    ~CAlarmModule();
};

#endif // CALARMMODULE_H
