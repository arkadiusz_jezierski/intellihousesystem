#include "CCondition.h"

CCondition::CCondition()
{
	logger = &CSingleton<CLogger>::Instance();
    alarmModule = &CSingleton<CAlarmModule>::Instance();
    this->result = EConditionResult::NotReady;
	this->conditionId = "COND" + QString::number(QDateTime::currentMSecsSinceEpoch()) + QString::number(qrand());
}

CCondition::~CCondition()
{
}

ELogicalConditionType CCondition::stringToCondition(QString condition){
    if (condition.toLower() == "equal"){
        return ELogicalConditionType::EQUAL;
    } else if (condition.toLower() == "greater"){
        return ELogicalConditionType::GREATER_THAN;
    } else if (condition.toLower() == "greater_equal"){
        return ELogicalConditionType::GREATER_OR_EQUAL;
    } else if (condition.toLower() == "less"){
        return ELogicalConditionType::LESS_THAN;
    } else if (condition.toLower() == "less_equal"){
        return ELogicalConditionType::LESS_OR_EQUAL;
    } else if (condition.toLower() == "any"){
        return ELogicalConditionType::ANY;
    } else if (condition.toLower() == "rising"){
        return ELogicalConditionType::RISING;
    } else if (condition.toLower() == "falling"){
        return ELogicalConditionType::FALLING;
    } else if (condition.toLower() == "not_equal"){
        return ELogicalConditionType::NOT_EQUAL;
    } else {
        return ELogicalConditionType::UNDEFINED;
    }
}

QString CCondition::conditionToString(ELogicalConditionType condition) {
	if (condition == ELogicalConditionType::EQUAL) {
		return "equals";
	} else if (condition == ELogicalConditionType::GREATER_THAN) {
		return "is greather than";
	} else if (condition == ELogicalConditionType::GREATER_OR_EQUAL) {
		return "is greather or equals";
	} else if (condition == ELogicalConditionType::LESS_THAN) {
		return "is less than";
	} else if (condition == ELogicalConditionType::LESS_OR_EQUAL) {
		return "is less or equals";
	} else if (condition == ELogicalConditionType::ANY) {
		return "is any";
	} else if (condition == ELogicalConditionType::RISING) {
		return "is rising";
	} else if (condition == ELogicalConditionType::FALLING) {
		return "is falling";
	} else if (condition == ELogicalConditionType::NOT_EQUAL) {
        return "not equals";
    } else {
		return "is undefined";
	} 
}

bool CCondition::deviceParamUndefined() {
	if (this->device != NULL) {
		if (this->device->parameters.contains(this->param)) {
			return this->device->parameters[this->param].undefined;
		} else {
			return true;
		}
		
	} else {
		return false;
	}
}

void CCondition::clearResult() {
    logger->debug(EVENT_MANAGER, "Clearing result in condition: " + this->toString());
	this->result = EConditionResult::NotReady;
}
    
EConditionResult CCondition::getResult() {
    if (this->type == EConditionType::DEVICE) {
        if (this->device == NULL) {
            logger->error(EVENT_MANAGER, "NULL pointer of device");
            this->result = EConditionResult::DeviceInactive;
        }

        if (!this->device->isActive()) {
            logger->error(EVENT_MANAGER, "Device inactive: " + this->device->objectName());
            this->result = EConditionResult::DeviceInactive;
        }

        if (!this->device->parameters.contains(this->param)) {
            logger->error(EVENT_MANAGER, "Invalid parameter " + this->param + " for Device " + this->device->objectName());
            this->result = EConditionResult::DeviceInactive;
        }

        if (this->device->parameters[this->param].undefined) {
            logger->error(EVENT_MANAGER, "Device parameter [" + this->device->objectName() + "]:" + this->param + " is undefined ");
            this->result = EConditionResult::DeviceParamUndefined;
        }
    }
	if (this->result == EConditionResult::NotReady) {
		calculateCondition();
	}

	return this->result;
}


bool CCondition::compareValues(QString firstValLabel, qint64 value1, QString secondValLabel, qint64 value2, ELogicalConditionType condition) {//} QString (*convertVal)(qint64)) {

    switch(condition) {
    case ELogicalConditionType::EQUAL:
        logger->debug(EVENT_MANAGER, "Comparison " + firstValLabel + " " + QString::number(value1) + " = " + secondValLabel + " " + QString::number(value2));
        return value1 == value2;
        break;
    case ELogicalConditionType::GREATER_OR_EQUAL:
        logger->debug(EVENT_MANAGER, "Comparison " + firstValLabel + " " + QString::number(value1) + " >= " + secondValLabel + " " + QString::number(value2));
        return value1 >= value2;
        break;
    case ELogicalConditionType::GREATER_THAN:
        logger->debug(EVENT_MANAGER, "Comparison " + firstValLabel + " " + QString::number(value1) + " > " + secondValLabel + " " + QString::number(value2));
        return value1 > value2;
        break;
    case ELogicalConditionType::LESS_OR_EQUAL:
        logger->debug(EVENT_MANAGER, "Comparison " + firstValLabel + " " + QString::number(value1) + " <= " + secondValLabel + " " + QString::number(value2));
        return value1 <= value2;
        break;
    case ELogicalConditionType::LESS_THAN:
        logger->debug(EVENT_MANAGER, "Comparison " + firstValLabel + " " + QString::number(value1) + " < " + secondValLabel + " " + QString::number(value2));
        return value1 < value2;
        break;
    case ELogicalConditionType::NOT_EQUAL:
        logger->debug(EVENT_MANAGER, "Comparison " + firstValLabel + " " + QString::number(value1) + " <> " + secondValLabel + " " + QString::number(value2));
        return value1 != value2;
        break;
    case ELogicalConditionType::UNDEFINED:
        return false;
        break;
    case ELogicalConditionType::ANY:
    case ELogicalConditionType::RISING:
    case ELogicalConditionType::FALLING:
        break;
    }
    return false;
}

