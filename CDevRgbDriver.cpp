#include "CDevRgbDriver.h"

CDevRgbDriver::CDevRgbDriver()
{
    this->setCategory(DevCat::RGB);

    initParameters();
    initCommandMethods();
    initRequestMethods();
    initExecuteMethods();
}

CDevRgbDriver::~CDevRgbDriver()
{

}



void CDevRgbDriver::initParameters(){
    Value val, justForCommand;
    val.undefined = true;
    val.value = 0;
    val.changes = EParamChanges::None;

    justForCommand.undefined = false;

    parameters[PARAM_RED] = val;
    parameters[PARAM_GREEN] = val;
    parameters[PARAM_BLUE] = val;
    //parameters[PARAM_RGB] = val;
    parameters[PARAM_MODE] = val;
    parameters[PARAM_SPEED] = val;
    commands[PARAM_RED] = justForCommand;
    commands[PARAM_GREEN] = justForCommand;
    commands[PARAM_BLUE] = justForCommand;
    commands[PARAM_RGB] = justForCommand;
    commands[PARAM_MODE] = justForCommand;
    commands[PARAM_SPEED] = justForCommand;
    commands[PARAM_OFF] = justForCommand;
    commands[PARAM_SWITCH] = justForCommand;

}

void CDevRgbDriver::commandMethodOfRgbDriver(COMMAND cmd, CBuffer &data){

    if (methodsMap.contains(cmd)){
        try{
            (this->*methodsMap[cmd])(data);
        }catch(CException ex){
            logger->error(DEVICE_MANAGER, "Error in RgbDriver: " + ex.what());
        }
    }else{
        logger->warning(DEVICE_MANAGER, "No method to invoke for command " + QString::number((int)cmd));
    }
}


void CDevRgbDriver::requestParameterForRgbDriver(QString param){

    if (requestMethodsMap.contains(param)){
        try{
            (this->*requestMethodsMap[param])();
        }catch(CException ex){
            logger->error(DEVICE_MANAGER, "Error in RgbDriver: " + ex.what());
        }
    }else{
        logger->warning(DEVICE_MANAGER, "RgbDriver: No request to invoke for parameter " + param);
    }
}

SCommandSet CDevRgbDriver::executeCommandForRgbDriver(SCommandSet commSet){
    SCommandSet retVal;
    if (executeDeviceCommandMap.contains(commSet.parameter)){
        try{
            retVal = (this->*executeDeviceCommandMap[commSet.parameter])(commSet.value);
        }catch(CException ex){
            logger->error(DEVICE_MANAGER, "Error in RgbDriver: " + ex.what());
        }
    }else{
        logger->warning(DEVICE_MANAGER, "RgbDriver: No execution to invoke for parameter " + commSet.parameter);
    }
    return retVal;
}


void CDevRgbDriver::initCommandMethods(){
    methodsMap[CMD_CAN_SET_RGB_CHANNEL_ALL] = &CDevRgbDriver::setAllRgbValues;
    methodsMap[CMD_CAN_SET_RGB_MODE] = &CDevRgbDriver::setMode;
    methodsMap[CMD_CAN_SET_RGB_SPEED] = &CDevRgbDriver::setSpeed;
    methodsMap[CMD_CAN_GET_RGB_CHANNEL_RED] = &CDevRgbDriver::getRedValue;
    methodsMap[CMD_CAN_GET_RGB_CHANNEL_GREEN] = &CDevRgbDriver::getGreenValue;
    methodsMap[CMD_CAN_GET_RGB_CHANNEL_BLUE] = &CDevRgbDriver::getBlueValue;
    // methodsMap[CMD_CAN_GET_RGB_CHANNEL_ALL] = &CDevRgbDriver::getAllRgbValues;
    methodsMap[CMD_CAN_GET_RGB_MODE] = &CDevRgbDriver::getMode;
    methodsMap[CMD_CAN_GET_RGB_SPEED] = &CDevRgbDriver::getSpeed;
    methodsMap[CMD_CAN_SWITCH_MODE] = &CDevRgbDriver::switchMode;

}

void CDevRgbDriver::initRequestMethods(){
    requestMethodsMap[PARAM_RED] = &CDevRgbDriver::requestRedValue;
    requestMethodsMap[PARAM_GREEN] = &CDevRgbDriver::requestGreenValue;
    requestMethodsMap[PARAM_BLUE] = &CDevRgbDriver::requestBlueValue;
    //requestMethodsMap[PARAM_RGB] = &CDevRgbDriver::requestRgbValues;
    requestMethodsMap[PARAM_MODE] = &CDevRgbDriver::requestMode;
    requestMethodsMap[PARAM_SPEED] = &CDevRgbDriver::requestSpeed;
}

void CDevRgbDriver::initExecuteMethods(){

    executeDeviceCommandMap[PARAM_RED] = &CDevRgbDriver::executeSetRedValue;
    executeDeviceCommandMap[PARAM_GREEN] = &CDevRgbDriver::executeSetGreenValue;
    executeDeviceCommandMap[PARAM_BLUE] = &CDevRgbDriver::executeSetBlueValue;
    executeDeviceCommandMap[PARAM_RGB] = &CDevRgbDriver::executeSetRgbValues;
    executeDeviceCommandMap[PARAM_MODE] = &CDevRgbDriver::executeSetMode;
    executeDeviceCommandMap[PARAM_SPEED] = &CDevRgbDriver::executeSetSpeed;
    executeDeviceCommandMap[PARAM_OFF] = &CDevRgbDriver::executeSetRgbOff;
    executeDeviceCommandMap[PARAM_SWITCH] = &CDevRgbDriver::executeSwitchMode;
}

unsigned long CDevRgbDriver::convertBuffer(CBuffer &buf){
    unsigned long val = 0;
    for (size_t i = 1; i < buf.size(); i++){
        val <<= 8;
        val += buf[i];
    }
    return val;
}

void CDevRgbDriver::setAllRgbValues(CBuffer &data){

    logger->info(DEVICE_MANAGER, "RgbDriver->setAllRgbValues(" + QString::number(convertBuffer(data)) + ")");

    Value valRed;
    valRed.undefined = false;
    valRed.value = data[OFFSET_CAN_RGB_VALUE] & 0x0f;
    valRed.value <<= 8;
    valRed.value |= data[OFFSET_CAN_RGB_VALUE + 1];
    valRed.changes = checkValueChanged(PARAM_RED, valRed.value);
//    qDebug()<<"RED: "<<valRed.value;

    Value valGreen;
    valGreen.undefined = false;
    valGreen.value = data[OFFSET_CAN_RGB_VALUE + 2];
    valGreen.value <<= 4;
    valGreen.value |= ((data[OFFSET_CAN_RGB_VALUE + 3] & 0xf0) >> 4);
    valGreen.changes = checkValueChanged(PARAM_GREEN, valGreen.value);
//    qDebug()<<"green: "<<valGreen.value;
    Value valBlue;
    valBlue.undefined = false;
    valBlue.value = data[OFFSET_CAN_RGB_VALUE + 3] & 0x0f;
    valBlue.value <<= 8;
    valBlue.value |= data[OFFSET_CAN_RGB_VALUE + 4];
    valBlue.changes = checkValueChanged(PARAM_BLUE, valBlue.value);
//    qDebug()<<"blue: "<<valBlue.value;
    Value valRgb;
    valRgb.undefined = false;
    valRgb.value = (valRed.value << 24) | (valGreen.value << 12) | valBlue.value;
    valRgb.changes = checkValueChanged(PARAM_RGB, valRgb.value);

    parameters[PARAM_RED] = valRed;
    parameters[PARAM_GREEN] = valGreen;
    parameters[PARAM_BLUE] = valBlue;
    //parameters[PARAM_RGB] = valRgb;

    if (valRed.changes != EParamChanges::None) {
        emit sendEvent(this->getCategory(), this->getAddress(), PARAM_RED, valRed.value);
    }
    if (valGreen.changes != EParamChanges::None) {
        emit sendEvent(this->getCategory(), this->getAddress(), PARAM_GREEN, valGreen.value);
    }
    if (valBlue.changes != EParamChanges::None) {
        emit sendEvent(this->getCategory(), this->getAddress(), PARAM_BLUE, valBlue.value);
    }
}
void CDevRgbDriver::setMode(CBuffer &data){
    logger->info(DEVICE_MANAGER, "RgbDriver->setMode(" + QString::number(data[OFFSET_CAN_RGB_VALUE]) + ")");

    Value val;
    val.undefined = false;
    val.value = data[OFFSET_CAN_RGB_VALUE];
    val.changes = checkValueChanged(PARAM_MODE, val.value);

    parameters[PARAM_MODE] = val;
    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_MODE, val.value);
}
void CDevRgbDriver::switchMode(CBuffer &data){
    logger->info(DEVICE_MANAGER, "RgbDriver->switchMode()");
    data = data;
    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_SWITCH, 0);
}
void CDevRgbDriver::setSpeed(CBuffer &data){
    logger->info(DEVICE_MANAGER, "RgbDriver->setSpeed(" + QString::number(data[OFFSET_CAN_RGB_VALUE]) + ")");

    Value val;
    val.undefined = false;
    val.value = data[OFFSET_CAN_RGB_VALUE];
    val.changes = checkValueChanged(PARAM_SPEED, val.value);

    parameters[PARAM_SPEED] = val;
    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_SPEED, val.value);

}
// void CDevRgbDriver::getAllRgbValues(CBuffer &data){
//     logger->info(DEVICE_MANAGER, this->objectName() + "->getAllRgbValues(" + QString::number(convertBuffer(data)) + ")");

//     Value valRed;
//     valRed.undefined = false;
//     valRed.value = data[OFFSET_CAN_RGB_VALUE] & 0x0f;
//     valRed.value <<= 8;
//     valRed.value |= data[OFFSET_CAN_RGB_VALUE + 1];
//     valRed.changes = checkValueChanged(PARAM_RED, valRed.value);

//     Value valGreen;
//     valGreen.undefined = false;
//     valGreen.value = data[OFFSET_CAN_RGB_VALUE + 2];
//     valGreen.value <<= 4;
//     valGreen.value |= ((data[OFFSET_CAN_RGB_VALUE + 3] & 0xf0) >> 4);
//     valGreen.changes = checkValueChanged(PARAM_GREEN, valGreen.value);

//     Value valBlue;
//     valBlue.undefined = false;
//     valBlue.value = data[OFFSET_CAN_RGB_VALUE + 3] & 0x0f;
//     valBlue.value <<= 8;
//     valBlue.value |= data[OFFSET_CAN_RGB_VALUE + 4];
//     valBlue.changes = checkValueChanged(PARAM_BLUE, valBlue.value);

//     Value valRgb;
//     valRgb.undefined = false;
//     valRgb.value = (valRed.value << 24) | (valGreen.value << 12) | valBlue.value;
//     valRgb.changes = checkValueChanged(PARAM_RGB, valRgb.value);

//     parameters[PARAM_RED] = valRed;
//     parameters[PARAM_GREEN] = valGreen;
//     parameters[PARAM_BLUE] = valBlue;
//     parameters[PARAM_RGB] = valRgb;

//     if (valRed.changes != EParamChanges::None) {
//         emit sendEvent(this->getCategory(), this->getAddress(), PARAM_RED, valRed.value);
//     }
//     if (valGreen.changes != EParamChanges::None) {
//         emit sendEvent(this->getCategory(), this->getAddress(), PARAM_GREEN, valGreen.value);
//     }
//     if (valBlue.changes != EParamChanges::None) {
//         emit sendEvent(this->getCategory(), this->getAddress(), PARAM_BLUE, valBlue.value);
//     }
// }
void CDevRgbDriver::getRedValue(CBuffer &data){
    logger->info(DEVICE_MANAGER, this->objectName() + "->getRedValue(" + QString::number(convertBuffer(data)) + ")");

    Value val;
    val.undefined = false;
    val.value = data[OFFSET_CAN_RGB_VALUE] & 0x0f;
    val.value <<= 8;
    val.value |= data[OFFSET_CAN_RGB_VALUE + 1];
    val.changes = checkValueChanged(PARAM_RED, val.value);

    parameters[PARAM_RED] = val;
    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_RED, val.value);
}
void CDevRgbDriver::getGreenValue(CBuffer &data){
    logger->info(DEVICE_MANAGER, this->objectName() + "->getGreenValue(" + QString::number(convertBuffer(data)) + ")");

    Value val;
    val.undefined = false;
    val.value = data[OFFSET_CAN_RGB_VALUE] & 0x0f;
    val.value <<= 8;
    val.value |= data[OFFSET_CAN_RGB_VALUE + 1];
    val.changes = checkValueChanged(PARAM_GREEN, val.value);

    parameters[PARAM_GREEN] = val;
    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_GREEN, val.value);
}
void CDevRgbDriver::getBlueValue(CBuffer &data){
    logger->info(DEVICE_MANAGER, this->objectName() + "->getBlueValue(" + QString::number(convertBuffer(data)) + ")");

    Value val;
    val.undefined = false;
    val.value = data[OFFSET_CAN_RGB_VALUE] & 0x0f;
    val.value <<= 8;
    val.value |= data[OFFSET_CAN_RGB_VALUE + 1];
    val.changes = checkValueChanged(PARAM_BLUE, val.value);

    parameters[PARAM_BLUE] = val;
    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_BLUE, val.value);
}
void CDevRgbDriver::getMode(CBuffer &data){
    logger->info(DEVICE_MANAGER, this->objectName() + "->getMode(" + QString::number(data[OFFSET_CAN_RGB_VALUE]) + ")");

    Value val;
    val.undefined = false;
    val.value = data[OFFSET_CAN_RGB_VALUE];
    val.changes = checkValueChanged(PARAM_MODE, val.value);

    parameters[PARAM_MODE] = val;
    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_MODE, val.value);
}
void CDevRgbDriver::getSpeed(CBuffer &data){
    logger->info(DEVICE_MANAGER, this->objectName() + "->getSpeed(" + QString::number(data[OFFSET_CAN_RGB_VALUE]) + ")");

    Value val;
    val.undefined = false;
    val.value = data[OFFSET_CAN_RGB_VALUE];
    val.changes = checkValueChanged(PARAM_SPEED, val.value);

    parameters[PARAM_SPEED] = val;
    emit sendEvent(this->getCategory(), this->getAddress(), PARAM_SPEED, val.value);
}

void CDevRgbDriver::requestRedValue(){
    logger->info(DEVICE_MANAGER, this->objectName() + "->requestRedValue()");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_GET_RGB_CHANNEL_RED);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
}
void CDevRgbDriver::requestGreenValue(){
    logger->info(DEVICE_MANAGER, this->objectName() + "->requestGreenValue()");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_GET_RGB_CHANNEL_GREEN);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
}
void CDevRgbDriver::requestBlueValue(){
    logger->info(DEVICE_MANAGER, this->objectName() + "->requestBlueValue()");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_GET_RGB_CHANNEL_BLUE);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
}
void CDevRgbDriver::requestRgbValues(){
    logger->info(DEVICE_MANAGER, this->objectName() + "->requestRgbValues()");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_GET_RGB_CHANNEL_ALL);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
}
void CDevRgbDriver::requestMode(){
    logger->info(DEVICE_MANAGER, this->objectName() + "->requestMode()");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_GET_RGB_MODE);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
}
void CDevRgbDriver::requestSpeed(){
    logger->info(DEVICE_MANAGER, this->objectName() + "->requestSpeed()");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_GET_RGB_SPEED);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
}

SCommandSet CDevRgbDriver::executeSetRgbOff(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeSetRgbOff(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_ALL_OFF);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;
    commSet.parameter = PARAM_MODE;
    commSet.value = 0;

    return commSet;
}
SCommandSet CDevRgbDriver::executeSetRedValue(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeSetRedValue(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_SET_RGB_CHANNEL_RED);
    buffer << static_cast<unsigned short>(value);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;
    commSet.parameter = PARAM_RED;
    commSet.value = value;

    return commSet;
}
SCommandSet CDevRgbDriver::executeSetGreenValue(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeSetGreenValue(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_SET_RGB_CHANNEL_GREEN);
    buffer << static_cast<unsigned short>(value);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;
    commSet.parameter = PARAM_GREEN;
    commSet.value = value;
  
    return commSet;
}
SCommandSet CDevRgbDriver::executeSetBlueValue(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeSetBlueValue(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_SET_RGB_CHANNEL_BLUE);
    buffer << static_cast<unsigned short>(value);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;
    commSet.parameter = PARAM_BLUE;
    commSet.value = value;
   
    return commSet;
}
SCommandSet CDevRgbDriver::executeSetRgbValues(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeSetRgbValues(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_SET_RGB_CHANNEL_ALL);
    buffer.addRgbValuesToBuffer(value);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;
    if (parameters[PARAM_MODE].value == 4){
        commSet.parameter = PARAM_BLUE;
        commSet.value = value & 0xfff;
    }else{
        commSet.expected = false;
    }

    return commSet;
}
SCommandSet CDevRgbDriver::executeSetMode(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeSetMode(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_SET_RGB_MODE);
    buffer << static_cast<unsigned char>(value);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;
    commSet.parameter = PARAM_MODE;
    commSet.value = value;

    return commSet;
}
SCommandSet CDevRgbDriver::executeSwitchMode(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeSwitchMode()");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_SWITCH_MODE);
    buffer << static_cast<unsigned char>(0);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;
    commSet.expected = false;

    value = value;

    return commSet;
}

SCommandSet CDevRgbDriver::executeSetSpeed(qint64 value){
    logger->info(DEVICE_MANAGER, this->objectName() + "->executeSetSpeed(" + QString::number(value) + ")");
    CCanBuffer buffer;
    buffer.setCommand(CMD_CAN_SET_RGB_SPEED);
    buffer << static_cast<unsigned char>(value);
    buffer.setTransmitDeviceId(this->getCategory(), this->getAddress());
    if (transceiver != NULL){
        transceiver->sendExCanFrame(buffer);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }

    SCommandSet commSet;
    commSet.parameter = PARAM_SPEED;
    commSet.value = value;

    return commSet;
}
