#ifndef CCONDITION_H
#define CCONDITION_H

#include "CCanDevice.h"
#include "HTypes.h"
#include <QDateTime>
#include <QtGlobal>
#include <CEvent.h>
#include <CLogger.h>
#include <CAlarmModule.h>
#include <CTools.h>

class CCondition
{

protected:

    CAlarmModule* alarmModule = NULL;
    CLogger *logger = NULL;
    bool compareValues(
        QString firstValLabel, qint64 value1, QString secondValLabel, qint64 value2, 
        ELogicalConditionType condition);


    virtual void calculateCondition() {}
    EConditionResult result;

public:
    CCondition();
    virtual ~CCondition();

	EConditionType type;
    ELogicalConditionType logicalCondition;
    qint64 value;
    QString param;
    CCanDevice* device;
    QString extLogicConditionId;

    static ELogicalConditionType stringToCondition(QString condition);
    static QString conditionToString(ELogicalConditionType condition);

    QString conditionId;

    bool deviceParamUndefined();
    EConditionResult getResult();
    void clearResult();

    virtual QString toString() {
        return QString("");
    }
    

};

#endif // CCONDITION_H
