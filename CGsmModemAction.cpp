#include "CGsmModemAction.h"

CGsmModemAction::CGsmModemAction(QString phoneNumber, QString message) : CAction()
{
    this->phoneNumber = phoneNumber;
    this->message = message;
    this->actionType = EActionType::GSM_MODULE_ACTION;
}

CGsmModemAction::~CGsmModemAction()
{

}

QString CGsmModemAction::toString() {
    QString str = "GSM Action [SEND TO " + this->phoneNumber + " MESSAGE (" + this->message + ")]";

    return str;
}

