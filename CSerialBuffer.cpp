#include "CSerialBuffer.h"

CSerialBuffer::CSerialBuffer()
{
}

CSerialBuffer::~CSerialBuffer()
{
}

unsigned char CSerialBuffer::getCRC() const{
    unsigned char crc = REQ_FRAME_HDR + command + CR + (unsigned char) size() + 5;
    for (size_t i = 0; i < size(); i++) {
        crc += this->operator[](i);
    }

    return crc;

}


bool CSerialBuffer::isBufferReady() {
    if (getRequestedBufferLengthByte() == this->size()) {
        unsigned char crc = 0;
        for (size_t i = 0; i < this->size() - 1; i++) {
            crc += this->operator[](i);
        }
        return (crc == this->operator[](this->size() - 1));
    }
    return false;
}



bool CSerialBuffer::isHandshakeResponse() {
    if (getResponsedBufferCommand() == CMD_RESP_HNDSHK) {
        return (this->operator[](OFFSET_UART2CAN_COMMAND + 1) == 'C' &&
                this->operator[](OFFSET_UART2CAN_COMMAND + 2) == 'A' &&
                this->operator[](OFFSET_UART2CAN_COMMAND + 3) == 'N' &&
                this->operator[](OFFSET_UART2CAN_COMMAND + 4) == '2' &&
                this->operator[](OFFSET_UART2CAN_COMMAND + 5) == '3' &&
                this->operator[](OFFSET_UART2CAN_COMMAND + 6) == '2');
    }
    return false;
}

unsigned char CSerialBuffer::calculateCRC() {
    unsigned char crc = REQ_FRAME_HDR + command + CR + (unsigned char) size() + 5;
    for (size_t i = 0; i < size(); i++) {
        crc += this->operator[](i);
    }

    return crc;
}

bool CSerialBuffer::isBadCrcBuffer() {
    return (getResponsedBufferCommand() == CMD_RESP_BAD_CRC);
}

unsigned char CSerialBuffer::getResponsedBufferCommand() {
    return this->operator[](OFFSET_UART2CAN_COMMAND);
}

unsigned char CSerialBuffer::getRequestedBufferLengthByte() {
    return this->operator[](OFFSET_UART2CAN_LENGTH);
}

void CSerialBuffer::printErrorBuffer() {
    logger->error(TRANSCEIVER, "Error frame has been received");
    logger->error(TRANSCEIVER, "Reading error counter: " + QString::number( this->operator[](OFFSET_RESP_RXERR)));
    logger->error(TRANSCEIVER, "Transmit error counter: " + QString::number( this->operator[](OFFSET_RESP_TXERR)));
    logger->error(TRANSCEIVER, "COMSTAT register: " + QString::number( this->operator[](OFFSET_RESP_COMSTAT), 16));
    logger->error(TRANSCEIVER, "TXB0CON register: " + QString::number( (this->operator[](OFFSET_RESP_TXCON) & 0x0f), 16));
    logger->error(TRANSCEIVER, "TXB1CON register: " + QString::number( ((this->operator[](OFFSET_RESP_TXCON) >> 4) & 0x0f), 16));
    logger->error(TRANSCEIVER, "TXB2CON register: " + QString::number( (this->operator[](OFFSET_RESP_TXCON + 1) & 0x0f), 16));
    logger->error(TRANSCEIVER, "B4CON register: " + QString::number( (this->operator[](OFFSET_RESP_TXCON + 2) & 0x0f), 16));
    logger->error(TRANSCEIVER, "B5CON register: " + QString::number( ((this->operator[](OFFSET_RESP_TXCON + 2) >> 4) & 0x0f), 16));
}

void CSerialBuffer::printNoDataFrame() {

    logger->info(TRANSCEIVER, "No CAN frame is pending (R_ERR_CNT:" + QString::number( this->operator[](OFFSET_RESP_RXERR)) 
                + ", T_ERR_CNT:" + QString::number( this->operator[](OFFSET_RESP_TXERR)) 
                + ", TXBIE:" + QString::number( this->operator[](OFFSET_RESP_TXBIE), 16) 
                + ", BIE0:" + QString::number( this->operator[](OFFSET_RESP_BIE0), 16) 
                + ", R_FIFO:" + QString::number( this->operator[](OFFSET_RESP_RFIFO_SIZE))
                + ", W_FIFO:" + QString::number( this->operator[](OFFSET_RESP_WFIFO_SIZE)) + ")");
    //logger->low_info(TRANSCEIVER, "Reading error counter: " + QString::number( this->operator[](OFFSET_RESP_RXERR)));
    //logger->low_info(TRANSCEIVER, "Transmit error counter: " + QString::number( this->operator[](OFFSET_RESP_TXERR)));
    //logger->low_info(TRANSCEIVER, "TXBIE register: " + QString::number( this->operator[](OFFSET_RESP_TXBIE), 16));
    //logger->low_info(TRANSCEIVER, "BIE0 register: " + QString::number( this->operator[](OFFSET_RESP_BIE0), 16));
    //logger->low_info(TRANSCEIVER, "Read FIFO messages pending: " + QString::number( this->operator[](OFFSET_RESP_RFIFO_SIZE)));
    //logger->low_info(TRANSCEIVER, "Write FIFO messages pending: " + QString::number( this->operator[](OFFSET_RESP_WFIFO_SIZE)));
}

void CSerialBuffer::printDataFrame() {
    logger->info(TRANSCEIVER, "CAN frame has been received");

    QString data = "";
    for (size_t i = OFFSET_UART2CAN_DATA; i < this->size() - 1; i++) {
        data +=  "0x" + QString::number(this->operator [](i), 16) + " ";
    }

    if (data.length() > 0){
        logger->low_info(TRANSCEIVER, "CAN ID: " + QString::number(this->getCanID(), 16) + " " + CCanBuffer::shortDescriptionForSourceID(this->getCanID()) + " , DATA: [" + data + "]");
    }else{
        logger->low_info(TRANSCEIVER, "CAN ID: " + QString::number(this->getCanID(), 16) + " " + CCanBuffer::shortDescriptionForSourceID(this->getCanID()) + " , NO DATA]");
    }
}

unsigned long CSerialBuffer::getCanID() {
    unsigned long id = (unsigned long) ((int)this->operator[](OFFSET_UART2CAN_ID) << 24);
    id |= (unsigned long) (this->operator[](OFFSET_UART2CAN_ID + 1) << 16);
    id |= (unsigned long) (this->operator[](OFFSET_UART2CAN_ID + 2) << 8);
    id |= (unsigned long) this->operator[](OFFSET_UART2CAN_ID + 3);
    return id;
}

bool CSerialBuffer::isACK() {
    return getResponsedBufferCommand() == CMD_RESP_ACK;
}
