#include "CCanDevice.h"

CCanDevice::CCanDevice(QObject *parent) : QObject(parent)
{
    logger = &CSingleton<CLogger>::Instance();
    deviceWatcher = &CSingleton<CDeviceWatcher>::Instance();
    //    logger->debug("create new device");
    config = &CSingleton<CConfig>::Instance();

}

CCanDevice::CCanDevice(DevCat category, unsigned char address, QObject *parent) : CCanDevice(parent){
    this->category = category;
    this->address = address;
    this->deviceId = generateDeviceId(category, address);
}

CCanDevice::CCanDevice(unsigned char category, unsigned char address, QObject *parent) : CCanDevice(static_cast<DevCat>(category), address, parent){

}

CCanDevice::CCanDevice(QString category, QString address, QObject *parent) : CCanDevice(category.toInt(), address.toInt(), parent){

}

void CCanDevice::setCategoryAndAddress(DevCat category, unsigned char address) {
    this->category = category;
    this->address = address;
    this->deviceId = generateDeviceId(category, address);
}

void CCanDevice::setCategory(DevCat category) {
    this->category = category;
    this->deviceId = generateDeviceId(category, this->address);
}

DevCat CCanDevice::getCategory() {
    return this->category;
}

unsigned char CCanDevice::getAddress() {
    return this->address;
}

QString CCanDevice::getDeviceId() {
    return this->deviceId;
}


EParamChanges CCanDevice::checkValueChanged(QString param, qint64 val){
    if (parameters.contains(param) &&
            (! parameters[param].undefined && (parameters[param].value != val))){
        if (val > parameters[param].value) {
            return EParamChanges::Rising;
        } else {
            return EParamChanges::Falling;
        }
    }else{
        return EParamChanges::None;
    }
}

CCanDevice::~CCanDevice()
{
    logger->debug(DEVICE_MANAGER, "Device destructor invoked");
}

void CCanDevice::setUID(const UID &uid){
    this->uid = uid;
    deviceWatcher->updateLastSeenTime(uid);
}

UID CCanDevice::getUID()const{
    return uid;
}

qint64 CCanDevice::getLastSeenTime(){
    return deviceWatcher->getLastSeenTime(uid);
}

void CCanDevice::updateLastSeenTime(){
    //    qDebug()<<">>>>>>>> Update last seen time for device " << this->objectName();
    deviceWatcher->updateLastSeenTime(uid);
}

void CCanDevice::assignTransceiver(CTransceiver *transceiver){
    this->transceiver = transceiver;
}


void CCanDevice::executeCommand(COMMAND cmd, CBuffer data){
    logger->info(DEVICE_MANAGER, "Executing command " + QString::number(cmd) + " for device " + this->objectName()
                 + " with data: " + data.getPrintBuffer(true));
    QString name = "commandMethodOf" + categoryToString(category);
    QString normalizedName = name + "(COMMAND,CBuffer&)";
    if (this->metaObject()->indexOfSlot(normalizedName.toStdString().c_str()) == -1){
        logger->warning(DEVICE_MANAGER, "No method " + normalizedName + " to invoke");
    }else{
        qRegisterMetaType<CBuffer>("CBuffer&");
        qRegisterMetaType<COMMAND>("COMMAND");
        QMetaObject::invokeMethod(this, name.toStdString().c_str(), Q_ARG(COMMAND, cmd), Q_ARG(CBuffer&, data));

    }

}

void CCanDevice::requestParameter(QString param){
    logger->info(DEVICE_MANAGER, "Requesting parameter " + param + " for device " + this->objectName());
    QString name = "requestParameterFor" + categoryToString(category);
    QString normalizedName = name + "(QString)";
    if (this->metaObject()->indexOfSlot(normalizedName.toStdString().c_str()) == -1){
        logger->warning(DEVICE_MANAGER, "No method " + normalizedName + " to invoke");
    }else{
        QMetaObject::invokeMethod(this, name.toStdString().c_str(), Q_ARG(QString, param));
    }

}

SCommandSet CCanDevice::executeAction(SCommandSet commSet){
    logger->info(DEVICE_MANAGER, "Executing action, setting parameter " + commSet.parameter
                 + " with value " + QString::number(commSet.value) + " for device " + this->objectName());
    SCommandSet ret;
    QString name = "executeCommandFor" + categoryToString(category);
    QString normalizedName = name + "(SCommandSet)";
    qRegisterMetaType<SCommandSet>("SCommandSet");
    if (this->metaObject()->indexOfSlot(normalizedName.toStdString().c_str()) == -1){
        logger->warning(DEVICE_MANAGER, "No method " + normalizedName + " to invoke");
    }else{
        QMetaObject::invokeMethod(this, name.toStdString().c_str(), Qt::DirectConnection, Q_RETURN_ARG(SCommandSet, ret), Q_ARG(SCommandSet, commSet));
    }
    return ret;

}

bool CCanDevice::operator <(const CCanDevice & device){

    if (static_cast<int>(this->category) > static_cast<int>(device.category)){
        return false;
    }else if (static_cast<int>(this->category) < static_cast<int>(device.category)){
        return true;
    }else{
        if (this->address < device.address){
            return true;
        }else if (this->address < device.address){
            return false;
        }else{
            if (this->uid < device.uid){
                return true;
            }else{
                return false;
            }

        }
    }  

}


QString CCanDevice::generateDeviceId(DevCat cat, unsigned char address){
    return categoryToString(cat) + QString::number(address);
}

QString CCanDevice::categoryToString(DevCat cat){
    QString category;
    switch(cat){
    case DevCat::BroadCast:
        category = "BroadCast";
        break;
    case DevCat::BistableSwitch:
        category = "BistableSwitch";
        break;
    case DevCat::BistableSwitchSensor:
        category = "BistableSwitchSensor";
        break;
    case DevCat::PWM:
        category = "PwmDriver";
        break;
    case DevCat::RGB:
        category = "RgbDriver";
        break;
    case DevCat::MotionDetector:
        category = "MotionDetector";
        break;
    default :
        category = "Unknown";
        break;
    }

    return category;
}


bool CCanDevice::operator ==(const CCanDevice &device) const{
    return this->deviceId == device.deviceId;
    // return (static_cast<int>(this->category) == static_cast<int>(device.category) &&
    // this->address == device.address);

}

bool CCanDevice::operator ==(const CCanDevice *device) const{
    return this->deviceId == device->deviceId;
    // return (static_cast<int>(this->category) == static_cast<int>(device->category) &&
    // this->address == device->address);

}


QString CCanDevice::objectName() const{
    QString str;
    str = categoryToString(category) +
            "(" + ((getDeviceType() == EDeviceType::Actuator) ? "A" : "S") + ")" +
            "[" + QString::number(uid, 16) + "][" +
            QString::number(static_cast<int>(category)) + ":" + QString::number(static_cast<int>(address)) + "]'" +
            ((name.length() > 0) ? name : "noName") + "'";

    return str;
}

QString CCanDevice::toStringFull() const{
    QString str;
    str = objectName();

    QMap<QString, Value>::const_iterator i;
    for (i = parameters.begin(); i != parameters.end(); ++i){
        str += "\r\n{" + i.key() + ": " + QString::number(i.value().value) + "} ";
    }
    return str;
}

bool CCanDevice::checkCategory(DevCat cat){
    if (cat == DevCat::BistableSwitch ||
            cat == DevCat::BistableSwitchSensor ||
            cat == DevCat::PWM ||
            cat == DevCat::RGB ||
            cat == DevCat::MotionDetector
    ){
        return true;
    }else{
        return false;
    }
}

EDeviceType CCanDevice::getDeviceType() const{
    unsigned char cat = static_cast<unsigned char>(this->category);
    if (cat & 1){
        return EDeviceType::Actuator;
    }else{
        return EDeviceType::Sensor;
    }
}

void CCanDevice::sendACKForSensor(const CCanBuffer &message){
    CCanBuffer response;
    response.setCommand(CMD_CAN_ACK);
    response << static_cast<unsigned char>(message.getCommand());
    response.setTransmitDeviceId(category, address);
    if (transceiver != NULL){
        transceiver->sendExCanFrame(response);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
}

void CCanDevice::sendErrorFrameACK(){
    CCanBuffer response;
    response.setTransmitGlobalId(CMD_CAN_GLOB_ERR_ACK, uid);
    if (transceiver != NULL){
        transceiver->sendExCanFrame(response);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
}


void CCanDevice::sendPing(){
    logger->debug(DEVICE_MANAGER, "Sending ping to device " + objectName());
    CCanBuffer response;
    response.setTransmitDeviceId(category, address);
    response.setCommand(CMD_CAN_PING);
    if (transceiver != NULL){
        transceiver->sendExCanFrame(response);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
}

void CCanDevice::sendBootCommand(){
    logger->info(DEVICE_MANAGER, "Sending boot command to device with UID: " + QString::number(uid, 16));
    CCanBuffer response;
    response.setTransmitGlobalId(CMD_CAN_GLOB_BOOT, uid);
    if (transceiver != NULL){
        transceiver->sendExCanFrame(response);
    }else{
        logger->error(DEVICE_MANAGER, "No transceiver assigned");
    }
}

void CCanDevice::sendInitCommand(){
    CCanBuffer response;
    response.setTransmitGlobalId(CMD_CAN_GLOB_INIT, 0);
    if (transceiver != NULL){
        transceiver->sendExCanFrame(response);
    }else{
        logger->error(TRANSCEIVER, "No transceiver assigned");
    }
}


void CCanDevice::clearStatuses(){
    logger->info(DEVICE_MANAGER, "Clearing statuses for device " + this->objectName());
    QMap<QString, Value>::iterator it;
    for (it = parameters.begin(); it != parameters.end(); ++it){
        it.value().undefined = true;
    }
}

void CCanDevice::clearChangingStatus(QString param) {
    logger->info(DEVICE_MANAGER, "Clearing changing status of parameter " + param + " for device " + this->objectName());
    if (parameters.contains(param)){
        parameters[param].changes = EParamChanges::None;
    }
}

void CCanDevice::printDeviceErrorInfo(const CCanBuffer &buffer){
    CLogger * logger_tmp = &CSingleton<CLogger>::Instance();
    logger_tmp->low_info(TRANSCEIVER, "RX ERROR COUNTER: " + QString::number(buffer[OFFSET_CAN_ERR_RXERRCNT]));
    logger_tmp->low_info(TRANSCEIVER, "TX ERROR COUNTER: " + QString::number(buffer[OFFSET_CAN_ERR_TXERRCNT]));
    logger_tmp->low_info(TRANSCEIVER, "COMSTAT:          " + QString::number(buffer[OFFSET_CAN_ERR_COMSTAT], 16));
    logger_tmp->low_info(TRANSCEIVER, "TXB0CON:          " + QString::number((buffer[OFFSET_CAN_ERR_TXB0B1CON] & 0x0f), 16));
    logger_tmp->low_info(TRANSCEIVER, "TXB1CON:          " + QString::number((buffer[OFFSET_CAN_ERR_TXB0B1CON] >> 4), 16));
    logger_tmp->low_info(TRANSCEIVER, "TXB2CON:          " + QString::number((buffer[OFFSET_CAN_ERR_TXB2BIE] >> 4), 16));
    logger_tmp->low_info(TRANSCEIVER, "TXBIE:            " + QString::number((buffer[OFFSET_CAN_ERR_TXB2BIE] & 0x0f << 2), 16));
    logger_tmp->low_info(TRANSCEIVER, "B4CON:            " + QString::number((buffer[OFFSET_CAN_ERR_B4CON] & 0x0f), 16));
    logger_tmp->low_info(TRANSCEIVER, "B5CON:            " + QString::number((buffer[OFFSET_CAN_ERR_B5CON] & 0x0f), 16));
    logger_tmp->low_info(TRANSCEIVER, "BIE0:             " + QString::number(buffer[OFFSET_CAN_ERR_BIE0], 16));
}

bool CCanDevice::isActive(){
    int activeTime = config->getConfigParam(DEVICE_ACTIVE_TIME).toInt();
    if (QDateTime::currentMSecsSinceEpoch() - getLastSeenTime() < 1000 * activeTime){
        return true;
    }else{
        logger->warning(DEVICE_MANAGER, "Device [" + this->objectName() + "] has not responsed for a " + QString::number(activeTime) + " seconds");
        return false;
    }
}

void CCanDevice::uploadFirmware(QString fileName) {
    //quiet other devices


    CFirmwareLoader loader;
    CFirmwareBuffer firmware;
    unsigned int crc;

    try {

        firmware = loader.readFile(fileName);
        clearFlash();
        initSelfCRC();
        logger->info(UPLOAD_FW, "Starting a firmware uploading...");
        crc = uploadFirmware(firmware);
        verifyFirmware(crc);
        uploadExFirmware(firmware);
        exitBootMode();
        logger->success(UPLOAD_FW, "Uploading firmware success");
    } catch (CException e) {
        logger->error(UPLOAD_FW, "Firmware uploading failed: " + e.what());
    }

}


void CCanDevice::clearFlash() {
    logger->info(UPLOAD_FW, "Reseting program memory...");
    CTimeOut tout;
    CCanBuffer buffer;
    buffer.setCanID(0x330 | BOOT_PUT_CMD);
    buffer << CCanBuffer::convertToFlashAddress(0);

    buffer << static_cast<unsigned char>(BOOT_WRITE_UNLOCK | BOOT_AUTO_ERASE | BOOT_AUTO_INC | BOOT_SEND_ACK);
    buffer << static_cast<unsigned char>(BOOT_CMD_CLR_PROG);
    buffer << (unsigned char) 0;
    buffer << (unsigned char) 0;
    buffer = transceiver->sendRegularFrameWithResponse(buffer);
    if (buffer[0] != BOOT_COMMAND_ACK) {
        throw CException(QString("Reseting program memory failed"), this);
    }


    tout.startTimeOut(5000);
    while (!tout.isTimeOut()) {
        if (checkReady()) {
            logger->info(UPLOAD_FW, "Reseting program memory complete");
            return;
        }
    }

    throw CException(QString("Reseting program memory failed"), this);
}


bool CCanDevice::checkReady() {
    logger->info(UPLOAD_FW, "Checking if device is ready...");
    CCanBuffer buffer;
    buffer.setCanID(0x330 | BOOT_GET_CMD);
    buffer << CCanBuffer::convertToFlashAddress(0);
    buffer << static_cast<unsigned char>(BOOT_WRITE_UNLOCK | BOOT_AUTO_ERASE | BOOT_AUTO_INC | BOOT_SEND_ACK);
    buffer << static_cast<unsigned char>(BOOT_CMD_GET_READY);
    buffer << (unsigned char) 0;
    buffer << (unsigned char) 0;
    try{
        buffer = transceiver->sendRegularFrameWithResponse(buffer);
    }catch(CException ex){
        buffer.clear();
        return false;
    }

    return (buffer[0] == BOOT_COMMAND_ACK);
}

void CCanDevice::getDummyFrames() {
    CCanBuffer buffer;
    CBuffer rec;
    do {
        try{
            rec = transceiver->sendRegularFrameWithResponse(buffer);
        }catch(CException ex){

        }
    } while (rec.size() > 0);
}

void CCanDevice::initSelfCRC() {
    CCanBuffer buffer;
    buffer.setCanID(0x330 | BOOT_PUT_CMD);
    buffer.insertFlashAddress(0);
    buffer << CCanBuffer::convertToFlashAddress(0);
    buffer << static_cast<unsigned char>(BOOT_SEND_ACK);
    buffer << static_cast<unsigned char>(BOOT_CMD_INIT_CHK);
    buffer << (unsigned char) 0;
    buffer << (unsigned char) 0;

    buffer = transceiver->sendRegularFrameWithResponse(buffer);

    if (buffer[0] != BOOT_COMMAND_ACK) {
        throw CException(QString("Self CRC init failed"), this);
    }
}

unsigned int CCanDevice::uploadFirmware(CFirmwareBuffer &firmware) {
    CFirmwareBuffer tempBuffer;
    unsigned int lastAddress = 0;
    unsigned char progress = 255;
    unsigned int crc = 0;

    tempBuffer = firmware.getNotNullDataBlock();
    while (tempBuffer.size()) {

        if (tempBuffer.getDataBlockAddress() != lastAddress + 8) {
            initBootWrite(tempBuffer.getDataBlockAddress());
        }
        lastAddress = tempBuffer.getDataBlockAddress();
        writeProgramData(tempBuffer);
        calcCRC(crc, tempBuffer);

        if (progress != firmware.getReadingProgress()) {
            progress = firmware.getReadingProgress();
            logger->success(UPLOAD_FW, "Uploading progress: " + QString::number((int) progress) + "%");
        }
        tempBuffer = firmware.getNotNullDataBlock();
    }
    return crc;
}

void CCanDevice::initBootWrite(unsigned int address) {
    //    cout << "INIT WRITE ADR: " << address << endl;
    //CCanBuffer recBuf;
    CCanBuffer buffer;
    buffer.setCanID(0x330 | BOOT_PUT_CMD);
    buffer << CCanBuffer::convertToFlashAddress(address);
    buffer << static_cast<unsigned char>(BOOT_WRITE_UNLOCK | BOOT_AUTO_ERASE | BOOT_AUTO_INC | BOOT_SEND_ACK);
    buffer << static_cast<unsigned char>(BOOT_CMD_INIT_WRITE);
    buffer << (unsigned char) 0;
    buffer << (unsigned char) 0;

    buffer = transceiver->sendRegularFrameWithResponse(buffer);
    if (buffer[0] != BOOT_COMMAND_ACK) {
        throw CException(QString("Writing boot init failed"), this);
    }

}

void CCanDevice::initBootRead(unsigned int address) {
    CCanBuffer buffer;
    buffer.setCanID(0x330 | BOOT_PUT_CMD);
    buffer << CCanBuffer::convertToFlashAddress(address);
    buffer << static_cast<unsigned char>(BOOT_AUTO_INC | BOOT_SEND_ACK);
    buffer << static_cast<unsigned char>(BOOT_CMD_INIT_READ);
    buffer << (unsigned char) 0;
    buffer << (unsigned char) 0;

    buffer = transceiver->sendRegularFrameWithResponse(buffer);

    if (buffer[0] != BOOT_COMMAND_ACK) {
        throw CException(QString("Reading boot init failed"), this);
    }
}

void CCanDevice::writeProgramData(CBuffer data) {

    CCanBuffer buffer;
    buffer.setCanID(0x330 | BOOT_PUT_DATA);
    buffer << data;
    buffer = transceiver->sendRegularFrameWithResponse(buffer);

    if (buffer[0] != BOOT_COMMAND_ACK) {
        throw CException(QString("Writing program data failed"), this);
    }
}

void CCanDevice::calcCRC(unsigned int &crc, CFirmwareBuffer buf) {
    for (size_t i = 0; i < buf.size(); i++) {
        crc += buf[i];
        crc &= 0xffff;
    }
}

void CCanDevice::verifyFirmware(unsigned int crc) {
    logger->info(UPLOAD_FW, "Verifying firmware...");

    unsigned int devCRC = getSelfCRC();
    logger->info(UPLOAD_FW, "CRC in system: " + QString::number(crc) + ", CRC from device: " + QString::number(devCRC));
    if (crc != devCRC) {
        throw CException(QString("Veryfication firmware failed"), this);
    }

}

unsigned int CCanDevice::getSelfCRC() {

    CCanBuffer buffer;
    buffer.setCanID(0x330 | BOOT_GET_CMD);
    buffer << CCanBuffer::convertToFlashAddress(0);
    buffer << static_cast<unsigned char>(0);
    buffer << static_cast<unsigned char>(BOOT_CMD_CHK_RUN);
    buffer << (unsigned char) 0;
    buffer << (unsigned char) 0;

    buffer = transceiver->sendRegularFrameWithResponse(buffer);

    return buffer.getBootCRC();

}

void CCanDevice::uploadExFirmware(CFirmwareBuffer &buffer) {
    unsigned char exQnty = buffer.getExBufferQnty();
    CFirmwareBuffer exBuffer;
    for (unsigned char index = 0; index < exQnty; index++) {
        logger->info(UPLOAD_FW, "Extended firmware data uploading (" + QString::number((int) index + 1) + "/" + QString::number((int) exQnty) + ")...");
        exBuffer = buffer.getExBuffer(index);
        uploadExFirmwarePart(exBuffer);
    }
}


unsigned int CCanDevice::uploadExFirmwarePart(CFirmwareBuffer &firmware) {
    CFirmwareBuffer tempBuffer;
    //    unsigned int lastAddress = 0;
    unsigned int currAddress = 0;
    unsigned char progress = 255;
    unsigned int crc = 0;

    tempBuffer = firmware.getNotNullDataBlock();
    while (tempBuffer.size() > 0) {

        currAddress = tempBuffer.getDataBlockAddress();
        for (size_t i = 0; i < tempBuffer.size(); i++) {
            if (tempBuffer[i] != 0xff) {
                initBootWrite(currAddress++);
                writeProgramData(tempBuffer.subbuffer(i, 1));
            } else {
                currAddress++;
            }
        }

        calcCRC(crc, tempBuffer);
        if (progress != firmware.getReadingProgress()) {
            progress = firmware.getReadingProgress();
            logger->success(UPLOAD_FW, "Uploading progress: " + QString::number((int) progress) + "%");
        }
        tempBuffer = firmware.getNotNullDataBlock();


    }
    return crc;
}


void CCanDevice::exitBootMode() {
    logger->info(UPLOAD_FW, "Exiting BOOT mode...");
    CCanBuffer buffer;
    buffer.setCanID(0x330 | BOOT_PUT_CMD);
    buffer << CCanBuffer::convertToFlashAddress(0);
    buffer << static_cast<unsigned char>(BOOT_WRITE_UNLOCK | BOOT_AUTO_ERASE | BOOT_AUTO_INC | BOOT_SEND_ACK);
    buffer << static_cast<unsigned char>(BOOT_CMD_EXIT_BOOT);
    buffer << (unsigned char) 0;
    buffer << (unsigned char) 0;

    buffer = transceiver->sendRegularFrameWithResponse(buffer);

    if (buffer[0] != BOOT_COMMAND_ACK) {
        throw CException(QString("Exiting boot mode failed"), this);
    }
    resetDevice();

}

void CCanDevice::resetDevice() {
    logger->info(UPLOAD_FW, "Reseting device...");
    CCanBuffer buffer;
    buffer.setCanID(0x330 | BOOT_PUT_CMD);
    buffer << CCanBuffer::convertToFlashAddress(0);
    buffer << static_cast<unsigned char>(BOOT_WRITE_UNLOCK | BOOT_AUTO_ERASE | BOOT_AUTO_INC | BOOT_SEND_ACK);
    buffer << static_cast<unsigned char>(BOOT_CMD_RESET);
    buffer << (unsigned char) 0;
    buffer << (unsigned char) 0;

    transceiver->sendCanRegularFrame(buffer);
}





