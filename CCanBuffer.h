#ifndef CCANBUFFER_H
#define CCANBUFFER_H

#include <CBuffer.h>
#include <HCommConstans.h>
#include <HTypes.h>

class CCanBuffer : public CBuffer
{
    unsigned int canID;

    unsigned int flashAddress = 0;
    unsigned char bootCommand = 0;
    unsigned char bootControlBits = 0;

    unsigned int getDestinationID() const;
    unsigned char getDestinationAddress() const;
    unsigned char getDestinationCategoryID() const;

    static unsigned int getDstId(const unsigned int& canId);
    static unsigned int getDstAddress(const unsigned int& canId);
    static unsigned int getDstCat(const unsigned int& canId);
    static unsigned int getSrcId(const unsigned int& canId);
    static unsigned int getSrcAddress(const unsigned int& canId);
    static unsigned int getSrcCat(const unsigned int& canId);




public:
    CCanBuffer();
    CCanBuffer (const CCanBuffer & buf);
    ~CCanBuffer();

    enum class EFrameType{
        REGULAR,
        ADDRESS_REQUEST,
        ADDRESS_RESPONSE,
        ERROR_INFO,
        GLOBAL_COMMAND,
        EMPTY,
        DEV_STARTING_UP
    };

    void setCanID(const unsigned int);
    void setTransmitGlobalId(const COMMAND globalCommand, const UID uid);
    void setTransmitDeviceId(DevCat category, unsigned char address);
    unsigned int getCanID() const;
    void clear();

    void printCanBuffer(QString msg = "") const;

    unsigned int getSourceID() const;
    unsigned char getSourceAddress() const;
    unsigned char getSourceCategoryID() const;
    DevCat getSourceCategory() const;
    unsigned char getSourceCategoryTypeId() const;
    EDeviceType getSourceCategoryType() const;
    unsigned int getSourceUID() const;
    COMMAND getCommand() const;
    //    CBuffer getData() const;
    EFrameType getFrameType() const;
    int getResetCounter() const;
    int getWdtResetCounter() const;
    unsigned char getCANRxErrorCounter() const;
    unsigned char getCANTxErrorCounter() const;
    unsigned char getStartupInfo() const;
    unsigned char getRCONRegisterValue() const;

    void addPwmValuesToBuffer(const unsigned long long & values);
    void addRgbValuesToBuffer(const unsigned long long & values);

    unsigned char getAddressingModeFrameId() const;

    void insertFlashAddress(unsigned int address);
    void insertBootCommand(unsigned char command);
    void insertBootControlBits(unsigned char bits);
    unsigned int getBootCRC();

    static unsigned int convertToFlashAddress(unsigned int address);

    static QString shortDescriptionForDestinationID(const unsigned int &canID);
    static QString shortDescriptionForSourceID(const unsigned int &canID);

};

#endif // CCANBUFFER_H
