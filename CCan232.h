#ifndef CCAN232_H
#define CCAN232_H

#include <CTransceiver.h>
#include <CLogger.h>
#include <QList>
#include <CSerialBuffer.h>
#include <QThread>
#include <CConfig.h>

#include <HCommConstans.h>
#include <QMutex>

#define REQUEST_TOUT    1000

/**
 * @brief The CCan232 class
 * A driver class for CAN232 device.
 */
class CCan232  : public CTransceiver
{
    Q_OBJECT

    bool handshake();
    mutable QMutex serialPortMutex;

    CSerialBuffer lastNoDataBuffer;

    CSerialBuffer getFrame();
    void sendFrame(const CBuffer &buffer);

    void setRegularFilter(unsigned char id, unsigned int filter);
    void setExFilter(unsigned char id, unsigned int filter);
    void setRegularMask(unsigned char id, unsigned int mask) ;
    void setExMask(unsigned char id, unsigned int mask) ;
    void clearRXbuffer();
    QString findPort();
    void openPort(QString portName);
    void setRestartCommand();
    void setCanMode(const EEcanMode mode);
public:

    explicit CCan232();
    void configForCanA();
    void configForCanB();

    CCanBuffer getExCanMessage();

    void sendExCanFrame(const CBuffer &can) ;

    void sendCanRegularFrame(const CBuffer &can);
    CCanBuffer getRegularCanFrame();
    CCanBuffer sendRegularFrameWithResponse(const CBuffer &can);
    CConfig *config;

};

#endif // CCAN232_H
